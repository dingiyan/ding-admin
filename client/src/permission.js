/*
 * @copyright: Huang Ding
 * @Author: ding-cx
 * @Date: 2020-11-26 18:10:22
 * @LastEditors: ding-cx
 * @LastEditTime: 2021-04-23 15:15:46
 * @Description: file content
 */
import router from './router'
import store from './store'
import {
  Message
} from 'element-ui'
import NProgress from 'nprogress' // progress bar
import 'nprogress/nprogress.css' // progress bar style
import {
  getToken,
  removeToken
} from '@/utils/auth' // get token from cookie
import getPageTitle from '@/utils/get-page-title'

NProgress.configure({
  showSpinner: false
}) // NProgress Configuration

const whiteList = ['/login', '/auth-redirect','/system-installer'] // no redirect whitelist

router.beforeEach(async (to, from, next) => {
  // start progress bar
  NProgress.start()
  console.log(to);
  // set page title
  document.title = getPageTitle(to.meta.title)

  // determine whether the user has logged in
  const hasToken = getToken()
  if (hasToken) {
    if (to.path === '/login') {
      // if is logged in, redirect to the home page
      next({
        path: '/'
      })
      NProgress.done() // hack: https://github.com/PanJiaChen/vue-element-admin/pull/2939
    } else {
      // 判断用户是否有用户信息存在代码缓存，并拿到了路由表等。若有，则直接进入next。 
      // const hasRoles = store.getters.roles && store.getters.roles.length > 0
      const isLogin = store.getters.user_name;
      // console.log(store.getters.roles);
      if (isLogin) {
        next()
      } else {
        try {
          // get user info
          // note: 获取用户信息，含权限表
          await store.dispatch('user/getInfo')

          // 获取系统配置。
          if (!store.getters.appConfig) {
            await store.dispatch('user/getAppConfig');
          }

          // note:获取路由表(后端加权限后的。)
          const accessRoutes = await store.dispatch('permission/generateRoutes')

          // dynamically add accessible routes
          router.addRoutes(accessRoutes)

          // hack method to ensure that addRoutes is complete
          // set the replace: true, so the navigation will not leave a history record
          next({
            ...to,
            replace: true
          })
        } catch (error) {
          // remove token and go to login page to re-login
          await store.dispatch('user/resetToken')
          Message.error(error || 'Has Error')
          next(`/?redirect=${to.path}`)
          NProgress.done()
        }
      }
    }
  } else {
    /* has no token*/

    if (whiteList.indexOf(to.path) !== -1) {
      // in the free login whitelist, go directly
      next()
    } else {
      // other pages that do not have permission to access are redirected to the login page.
      next(`/login?redirect=${to.path}`)
      NProgress.done()
    }
  }
})

router.afterEach(() => {
  // finish progress bar
  NProgress.done()
})
