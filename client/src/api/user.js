/*
 * @copyright: Huang Ding
 * @Author: ding-cx
 * @Date: 2020-11-26 18:10:22
 * @LastEditors: ding-cx
 * @LastEditTime: 2021-03-24 16:51:44
 * @Description: file content
 */
import request from '@/utils/request'


import {
  api_prefix
} from "@/service/routeConst"


/* 登录 */
export function login(data) {
  return request({
    isCustomMsg: false,
    url: '/sys/user/login',
    method: 'post',
    // headers:{
    //   'Content-type': 'application/x-www-form-urlencoded'
    // }
    data
  })
}

/* 登录 */
export function loginTest(data) {
  return request({
    url: '/sys/User/test',
    method: 'get',
    // headers:{
    //   'Content-type': 'application/x-www-form-urlencoded'
    // }
    data
  })
}

/* 根据token获取用户详细信息 api */
export function getInfo(token) {
  return request({
    url: '/sys/user/info',
    method: 'get',
    params: {
      token
    }
  })
}

export function logout() {
  return request({
    url: '/sys/user/logout',
    method: 'post'
  })
}

/* 验证码 */
export function getCaptcha(uuid) {

  // console.log(uuid);
  return request({
    url: '/sys/captcha/generate/' + uuid + "?db=ding_admin_test",
    method: 'get',
    responseType: 'arraybuffer'
  }).then(function (response) {
    // console.log(response);
    // 将从后台获取的图片流进行转换
    return 'data:image/png;base64,' + btoa(
      new Uint8Array(response).reduce((data, byte) => data + String.fromCharCode(byte), '')
    )
  })
}

/** 获取登录用户的权限表 */
export function svs_permission() {
  // sys/permission/getAllPermByUser
  return request({
    method: "get",
    url: api_prefix + "/sys_user/permission/getAllPermByUser"
  })
}

/** 获取系统配置对象 */
export function svs_get_config() {
  // console.log("get config.db.");
  return request({
    method: "get",
    url: api_prefix + '/sys/config/get/object'
  })
}
