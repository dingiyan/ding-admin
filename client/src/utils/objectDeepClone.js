/*
 * @Author: dingding
 * @Date: 2020-03-22 20:25:23
 * @LastEditors: dingding
 * @LastEditTime: 2020-03-22 20:32:55
 * @Description: 实现对象和数组的深拷贝；基本类型和引用类型和函数，一些很高级的如symbol不支持
 */

 export default function deepClone(initalObj){
    let obj = {};
    if(typeof initalObj !== 'object'){
      return initalObj;
    }
    if(initalObj === null){
      return initalObj;
    }
    if(Array.isArray(initalObj)){
      obj = [];
    }
    for (const key in initalObj) {
      if (typeof initalObj[key] === 'object') {
        //对数组特殊处理
        if (Array.isArray(initalObj[key])) {
          //用map方法返回新数组，将数组中的元素递归
          obj[key] = initalObj[key].map(item => deepClone(item))
        } else {
          //递归返回新的对象
          obj[key] = deepClone(initalObj[key]);
        }
      } else if (typeof initalObj[key] === 'function') {
        //返回新函数
        obj[key] = initalObj[key].bind(obj);
      } else {
        //基本类型直接返回
        obj[key] = initalObj[key];
      }
    }
    return obj;
 }
