/*
 * @copyright: Huang Ding
 * @Author: ding-cx
 * @Date: 2020-11-10 12:10:01
 * @LastEditors: ding-cx
 * @LastEditTime: 2021-03-20 13:39:05
 * @Description: 权限判断函数。
 */
import store from '@/store'

/**
 * @param {Array} value
 * @returns {Boolean}
 * @example see @/views/permission/directive.vue
 */
export default function checkPermission(value) {
  // console.log(store.getters.user_name);
  if(store.getters.user_name === "admin"){
    return true;
  }
  if (value && (typeof value === 'string' || (value instanceof Array && value.length > 0))) {
    // console.log(value);
    const custom_perm_list = store.getters && store.getters.custom_perm_list
    // console.log(custom_perm_list);
    const needPermission = typeof value === 'string' ? [value] : value;

    const hasPermission = custom_perm_list.some(role => {
      return needPermission.includes(role)
    })
    return hasPermission
  } else {
    console.error(`need permission tag. string or array type.`)
    return false
  }
}
