/*
 * @copyright: Huang Ding
 * @Author: ding-cx
 * @Date: 2020-11-10 12:10:01
 * @LastEditors: ding-cx
 * @LastEditTime: 2021-03-11 11:41:57
 * @Description: file content
 */
// translate router.meta.title, be used in breadcrumb sidebar tagsview
export function generateTitle(title) {

  return title;//TODO 临时这样做，避免控制台弹出多语言的警告。
  const hasKey = this.$te('route.' + title)

  if (hasKey) {
    // $t :this method from vue-i18n, inject in @/lang/index.js
    const translatedTitle = this.$t('route.' + title)

    return translatedTitle
  }
  return title
}
