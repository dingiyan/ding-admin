/*
 * @copyright: Huang Ding
 * @Author: ding-cx
 * @Date: 2021-03-10 16:50:45
 * @LastEditors: ding-cx
 * @LastEditTime: 2021-04-09 11:26:27
 * @Description: file content
 */


import mitt from "mitt"


const handleData = (data) => {
  let ret;
  if (Array.isArray(data) || typeof data === "object") {
    try {
      ret = JSON.stringify(data);
    } catch (error) {
      ret = data.toString();
    }
  } else if (["string", "number", "boolean"].includes(typeof data)) {
    ret = data;
  } else {
    ret = data.toString ? data.toString() : data;
  }
  return ret;
}

const createWsuid = () => {
  const a = Math.random().toString(36).replace(".", "");
  const b = Math.random().toString(36).replace(".", "");
  return a + b;
}

export default class MyWebSocket {
  ws = null;
  emitter = null;
  wsArgs = {
    host: "",
    protocols: ""
  };
  /**
   * Creates an instance of MyWebSocket.
   * @param {*} host 连接的主机，host port和路径。不需要search查询参数。
   * @param {*} [args={}] 此处的参数将被做成search参数赋给host后面。
   * @param {*} protocols
   * @memberof MyWebSocket
   */
  constructor(host, args = {}, protocols) {
    if (!args.wsuid) {
      args.wsuid = createWsuid();
    }
    const argss = Object.keys(args).map(item => {
      return item + '=' + args[item]
    })
    host += argss.length > 0 ? "?" + argss.join("&") : "";
    this.wsArgs = {
      host,
      protocols
    }
    this.ws = new WebSocket(host, protocols);
    this.wsuid = args.wsuid;
    this.emitter = mitt()
    this._init()

  }

  reConnection() {
    // 重连ws
    this.ws = new WebSocket(this.wsArgs.host, this.wsArgs.protocols)
    this._init();
  }
  _init() {

    this.ws.onmessage = (msg) => {
      // console.log(msg);
      const data = msg.data;
      // 如果是传文件
      if (data instanceof Blob) {
        this.emitter.emit('get-file', data);
        return;
      }
      let ret = null;
      try {
        ret = JSON.parse(data)
      } catch (error) {
        ret = data;
      }
      // console.log(ret);
      if (ret && ret.event) {
        if(ret.randId){//处理携带randId的情况。将触发event加上这个随机id的事件。
          this.emitter.emit(ret.event + ret.randId,ret)
        }else{
          this.emitter.emit(ret.event, ret)
        }
      } else {
        this.emitter.emit("message", msg)
      }
    }

    this.ws.onopen = (msg) => {
      this.emitter.emit('open', msg)
    }
    // 内置心跳检测，断线重连。
    this.on('open', () => {
      // this._pingPong(20000)
    })

    this.ws.onerror = (err) => {
      this.emitter.emit('error', err)
    }

    this.ws.onclose = (data) => {
      this.emitter.emit('close', data)
    }

  }

  /** 发送消息 类似看做触发事件。但会send给服务器再接收同名事件响应。若需本地发送事件，需instance.emitter.emit */
  emit(event, data,randId) {
    this.ws.send(
      handleData({
        event: event,
        wsuid: this.wsuid,
        randId,
        data,
      })
    )
    // this.emitter.emit(event, data)
  }

  /** 请求响应模型，使用promsie发送和接收，模拟传统ajax */
  async send(event, data) {
    const randId = createWsuid();
    return new Promise((resolve, reject) => {
      try {
        this.emit(event, data,randId);
        this.once(event + randId, (msg) => {
          resolve(msg)
        })
      } catch (error) {
        reject(error)
      }
    })
  }

  /**
   *ws发送文件
   *
   * @memberof MyWebSocket
   */
  sendFile(file) {
    // console.log(file);
    const fileName = file.name;
    // return;
    const reader = new FileReader();
    reader.onload = (e) => {
      const binStr = e.target.result
      // console.log(binStr);
      this.send(binStr)
    }
    reader.readAsArrayBuffer(file)

    // 内部监听文件发送的结果。把上传后的无后缀文件添加上后缀等操作。
    this.once('send-file', (msg) => {
      if (msg.state === 'success') {
        this.emit('send-file-update-name', {
          originName: msg.originName,
          fileName,
        })
      }
    })

  }

  /**
   *p2p双向聊天接口
   *
   * @param {*} yourWsuid
   * @param {*} data
   * @memberof MyWebSocket
   */
  p2p(yourWsuid, data) {
    this.emit('p2p', {
      yourWsuid,
      data
    })
  }

  /** 监听自定义事件 */
  on(event, callback) {
    this.emitter.on(event, callback)
  }

  /** 监听一次事件，只要执行一次，就取消监听 */
  once(event, callback) {
    const cb = (...args) => {
      callback(...args)
      this.emitter.off(event, cb)
    }
    this.emitter.on(event, cb)
  }

  /** 移除某事件上的某监听回调 */
  off(event, cb) {
    this.emitter.off(event, cb)
  }

  /** 内置定时心跳在线检测器 */
  _pingPong(ms) {

    let isPonged;
    this.once('pong', (msg) => {
      // console.log(msg);
      if (msg.data === 'ping') {
        isPonged = true;
      }
    })

    this.emit('ping', 'ping');
    setTimeout(() => {
      if (!isPonged) {
        // console.log('new ws');
        this.emitter.emit('offline', true)
        this.reConnection();
      } else {
        this._pingPong(ms)
      }
    }, ms);

  }
}
