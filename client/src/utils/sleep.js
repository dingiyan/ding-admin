/*
 * @copyright: Huang Ding
 * @Author: ding-cx
 * @Date: 2021-03-17 10:46:11
 * @LastEditors: ding-cx
 * @LastEditTime: 2021-03-17 10:55:36
 * @Description: file content
 */

/** 睡眠时间，延迟之后resolve */
export default async (times) => {
  return new Promise((resolve) => {
    setTimeout(() => {
      resolve()
    }, times);
  })
}
