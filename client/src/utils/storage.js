/*
 * @copyright: Huang Ding
 * @Author: ding-cx
 * @Date: 2021-03-19 11:01:15
 * @LastEditors: ding-cx
 * @LastEditTime: 2021-03-22 12:20:34
 * @Description: file content
 */
// import objectDeepClone from './objectDeepClone'

const sessionStore = window.sessionStorage;
const localStore = window.localStorage;

function handleValue(value, isdo = "set") {
  // debugger;
  if (isdo === 'set') {
    try {
      if (typeof value === 'string') {
        return value;
      } else if (Array.isArray(value) || Object.prototype.toString.call(value) === "[object Object]") {
        return JSON.stringify(value)
      }
      return "";
    } catch (error) {
      console.log(error);
      return "";
    }
  } else {
    if (value) {
      try {
        return JSON.parse(value)
      } catch (error) {
        return value;
      }
    } else {
      return null;
    }
  }
}

export function setStore(name, value) {
  sessionStore.setItem(name, handleValue(value, 'set'))
}

export function getStore(name) {
  const val = sessionStore.getItem(name)
  return handleValue(val, 'get')
}

export function removeStore(name) {
  sessionStore.removeItem(name)
}

export function setLocalStore(name, value) {
  localStore.setItem(name, handleValue(value, 'set'))
}

export function getLocalStore(name) {
  const val = localStore.getItem(name)
  return handleValue(val, 'get')
}

export function removeLocalStore(name) {
  localStore.removeItem(name)
}
