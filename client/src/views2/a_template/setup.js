/*
 * @copyright: Huang Ding
 * @Author: ding-cx
 * @Date: 2021-01-08 11:05:22
 * @LastEditors: ding-cx
 * @LastEditTime: 2021-04-12 16:03:17
 * @Description: file content
 */

import {
  getCurrentInstance,
  reactive,
  ref
} from "@vue/composition-api"

import useCrud from "@/hooks/crud"

let root;


export default {
  name: "",
  setup(props, ctx) {
    return useEnter(props, ctx)
  }
}

/**
 *组合api功能入口。组件中调用。
 *
 * @export
 * @param {*} props
 * @param {*} ctx
 * @return {*} 
 */
function useEnter(props, ctx) {

  const setups = {
    props: props,
    ctx: ctx,
    root: ctx.root,
    that: getCurrentInstance(),
  }
  root = ctx.root;

  const options = {
    getConfig(page, condition) {
      return {
        method: "post",
        url: "",
        data: {}
      }
    },
    getMethod(page, condition) {
      //return axios service.
      return 
      // svs.XXX(config)
    }
  }

  setups.that.$on("get-list", (msg) => {
    console.log(msg);
  })



  const cruds = useCrud(options)
  useCrudOption(cruds.option)
  const addSubmit = (data, loading, done) => {

  }
  const rowEditSubmit = (data, loading, done) => {

  }

  const rowDel = (scope) => {

  }

  // 初始化时get data
  cruds.refresh();
  return {
    ...cruds,
    addSubmit,
    rowEditSubmit,
    rowDel,
  }
}




/**
 *专门处理crud 组件的option配置。
 *
 * @param {*} option
 */
function useCrudOption(option) {
  option.table.column = [{
      label: "lie",
      prop: "lie1"
    },
    {
      label: "lie",
      prop: "lie1"
    },
    {
      label: "lie",
      prop: "lie1"
    },
  ]

  /** 搜索查询配置 */
  option.search.items = [{
    label: "查询",
    prop: "search",
    component: "el-input",
    componentProps: {
      placeholder: "请输入内容"
    },
  }]
}
