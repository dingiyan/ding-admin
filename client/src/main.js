/*
 * @copyright: Huang Ding
 * @Author: ding-cx
 * @Date: 2020-11-17 11:18:21
 * @LastEditors: ding-cx
 * @LastEditTime: 2021-04-02 17:48:08
 * @Description: file content
 */
import Vue from 'vue'

import Cookies from 'js-cookie'

import 'normalize.css/normalize.css' // a modern alternative to CSS resets

import Element from 'element-ui'
import './styles/element-variables.scss'

import '@/styles/index.scss' // global css

import App from './App'
import store from './store'
import router from './router'

import i18n from './lang' // internationalization
import './icons' // icon
import './permission' // permission control
import './utils/error-log' // error log

import * as filters from './filters' // global filters

import vueCompositionApi from '@vue/composition-api'
Vue.use(vueCompositionApi)

import plugin from "./plugins"
Vue.use(plugin)


/**
 * If you don't want to use mock-server
 * you want to use MockJs for mock api
 * you can execute: mockXHR()
 *
 * Currently MockJs will be used in the production environment,
 * please remove it before going online ! ! !
 */
// if (process.env.NODE_ENV === 'production') {
//   const { mockXHR } = require('../mock')
//   mockXHR()
// }

Vue.use(Element, {
  size: Cookies.get('size') || 'small', // set element-ui default size
  i18n: (key, value) => i18n.t(key, value),
  zIndex: 60000
})

// register global utility filters
Object.keys(filters).forEach(key => {
  Vue.filter(key, filters[key])
})

Vue.config.productionTip = false

import {
  ref
} from "@vue/composition-api"
// 随窗口改变，自动设置一个属性为窗口高度和宽度，在组件中能用到。
window.hei = ref(window.innerHeight)
window.wid = ref(0)
Vue.prototype.$windowHeight = {
  height: window.innerHeight + "px",
  number: window.innerHeight
};
Vue.prototype.$windowWidth = {
  width: window.innerWidth + "px",
  number: window.innerWidth
};
window.onresize = () => {
  return (() => {
    window.hei.value = window.innerHeight
    window.wid.value = window.innerWidth
    Vue.prototype.$windowHeight.height = window.innerHeight + "px";
    Vue.prototype.$windowHeight.number = window.innerHeight;
    Vue.prototype.$windowWidth.width = window.innerWidth + "px";
    Vue.prototype.$windowWidth.number = window.innerWidth;
  })()
}

new Vue({
  el: '#app',
  router,
  store,
  i18n,
  render: h => h(App)
})
