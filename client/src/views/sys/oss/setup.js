/*
 * @copyright: Huang Ding
 * @Author: ding-cx
 * @Date: 2021-01-08 11:05:22
 * @LastEditors: ding-cx
 * @LastEditTime: 2021-08-13 13:45:05
 * @Description: file content
 */

import {
  getCurrentInstance,
  reactive,
  ref,
  computed
} from "@vue/composition-api"

import useCrud from "@/hooks/crud"

import svs_sys_oss from "@/service/sys/oss"
import {
  Form
} from "element-ui";

let root;


export default {
  name: "",
  setup(props, ctx) {
    return useEnter(props, ctx)
  },
  data() {
    return {
      apiName: svs_sys_oss.api + "upload",

      fileList: [
        //   {
        //   name: 'food.jpeg',
        //   url: 'https://fuss10.elemecdn.com/3/63/4e7f3a15429bfda99bce42a18cdd1jpeg.jpeg?imageMogr2/thumbnail/360x360/format/webp/quality/100'
        // }, {
        //   name: 'food2.jpeg',
        //   url: 'https://fuss10.elemecdn.com/3/63/4e7f3a15429bfda99bce42a18cdd1jpeg.jpeg?imageMogr2/thumbnail/360x360/format/webp/quality/100'
        // }
      ]
    };
  },
  methods: {
    handleRemove(file, fileList) {
      console.log(file, fileList);
    },
    handlePreview(file) {
      console.log(file);
    },
    handleExceed(files, fileList) {
      this.$message.warning(`当前限制选择 3 个文件，本次选择了 ${files.length} 个文件，共选择了 ${files.length + fileList.length} 个文件`);
    },
    beforeRemove(file, fileList) {
      return this.$confirm(`确定移除 ${ file.name }？`);
    }
  }
}

/**
 *组合api功能入口。组件中调用。
 *
 * @export
 * @param {*} props_
 * @param {*} ctx_
 * @return {*} 
 */
function useEnter(props_, ctx_) {

  const setups = {
    props: props_,
    ctx: ctx_,
    root: ctx_.root,
    that: getCurrentInstance(),
  }
  root = ctx_.root;

  const options = {
    getMethod(page, condition) {
      return svs_sys_oss.getSearchData({
        page: [page.size, page.offset],
      })
    }
  }

  setups.that.proxy.$on("get-list", (msg) => {
    console.log(msg);
  })



  const cruds = useCrud(options)
  useCrudOption(cruds.option)
  const addSubmit = (data, loading, done) => {

  }
  const rowEditSubmit = (data, loading, done) => {

  }

  const rowDel = (scope) => {

  }

  // 初始化时get data
  cruds.refresh();

  // 设置上传组件的headers,用于jwt鉴权。
  const uploadHeaders = computed(() => {
    return {
      'x-authorization': root.$store.getters.token
    }
  })

  const uploadFunc = (upObj) => {
    const file = upObj.file;
    const formData = new FormData();
    formData.append('file', file)
    svs_sys_oss.upload(formData).then(res => {
      console.log(res);
      if (res.id > 0) {
        // 上传成功返回true
        root.$message.success("上传成功！")
      }
    })

  }

  // console.log(uploadHeaders.value);
  return {
    ...cruds,
    addSubmit,
    rowEditSubmit,
    rowDel,
    uploadHeaders,
    uploadFunc
  }
}




/**
 *专门处理crud 组件的option配置。
 *
 * @param {*} option
 */
function useCrudOption(option) {
  option.table.column = [{
      label: "URL",
      prop: "url",
      slot: true,
      overflow: true,
    },
    {
      label: "用户",
      prop: "create_user_name",
    }

  ]

  option.table.rowMenuColumn.display = false;

  /** 搜索查询配置 */
  option.search.items = [{
    label: "查询",
    prop: "search",
    component: "el-input",
    componentProps: {
      placeholder: "请输入内容"
    },
  }]
}
