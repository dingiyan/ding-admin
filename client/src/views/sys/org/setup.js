/*
 * @copyright: Huang Ding
 * @Author: ding-cx
 * @Date: 2020-12-29 08:44:29
 * @LastEditors: ding-cx
 * @LastEditTime: 2021-04-02 18:00:48
 * @Description: file content
 */
import {
  ref,
  reactive,
  inject,
  watch,
  computed
} from "@vue/composition-api"

import useCrudHook from "@/hooks/crud"

import svs_sys_menus from "@/service/sys/menus"

import svs_sys_org from "@/service/sys/org"


import objectDeepClone from '@/utils/objectDeepClone'

export default {
  components: {

  },
  setup(props, ctx) {
    const setups = {
      props,
      ctx,
      root: ctx.root
    }
    const message = inject("globalMessage");

    const cruds = useTable();

    const leftTrees = useLeftTree(setups, cruds);

    const exports = {};

    const getTreeList = () => {
      svs_sys_org.getTreeList().then(data => {
        console.log(data);
        leftTrees.tree.data = data;
        // 将获取到的数据赋值给级联菜单。
        const col = cruds.option.table.column.find(item => item.prop === "parent_org_id")
        if (col) {
          const opt = col.form.componentProps.options;
          opt.splice(1, opt.length - 1, ...data)
        }
      })

    }
    getTreeList();


    /** 增、改 、表单数据处理转化。 */
    function addEditTrans(formData) {
      const sendData = objectDeepClone(formData)
      if (Array.isArray(sendData.parent_org_id)) {
        sendData.parent_org_id = sendData.parent_org_id[sendData.parent_org_id.length - 1] || 0;
      } else {
        // 非数组情况，可能是null，数值。则数值不变，null转为0.
        sendData.parent_org_id = sendData.parent_org_id > 0 ? sendData.parent_org_id : 0;
      }
      sendData.state = !!sendData.state;
      console.log(sendData);
      return sendData;
    }
    exports.addSubmit = (formData, loading, done) => {
      console.log(formData);
      loading();
      const sendData = addEditTrans(formData)
      // return
      // sendData.
      svs_sys_org.post(sendData).then(res => {
        console.log(res);
        loading();
        message.saveSuccess();
        getTreeList();
      })
    }

    exports.editSubmit = (formData, loading, done) => {
      console.log(formData);
      // loading();
      const sendData = addEditTrans(formData)
      // return
      // sendData.
      svs_sys_org.put(sendData.id, sendData).then(res => {
        console.log(res);
        loading();
        message.saveSuccess();
        cruds.refresh();
        getTreeList();
        done();
      })
    }

    // 行删除
    exports.rowDel = (scope) => {
      console.log(scope.row.id);
      setups.root.$confirm('确定要删除此内容吗？', '请确认', {
        confirmButtonText: '确定',
        cancelButtonText: '取消',
        type: 'warning'
      }).then(() => {
        svs_sys_org.delete(scope.row.id).then(res => {
          console.log(res);
          getTreeList();
          cruds.refresh();
          if (res.affectedRows >= 0) {
            setups.root.$message.success("删除成功！")
          } else {
            setups.root.$message.error("删除失败！")
          }
        })
      }).catch(() => {
        // setups.root.$message.error("操作失败！")

      });
    }





    return {
      ...cruds,
      ...exports,
      ...leftTrees,
    }

  }
}

function useTable() {
  const crudObj = ref({
    parent_menu_id: []
  })
  const cruds = useCrudHook({
    crudOption: {},
    getConfig(page, condition) {
      const config = svs_sys_org.getListByIdListConfig();
      // console.log(condition);
      config.data.idList = condition.idList;
      config.data.page = [page.size, page.offset]
      return config;
    },
    searchFormToCondition(form) {
      return {}
    },
    reduceHeight: 250,
  });

  cruds.option.table.column = [{
      label: "编号",
      prop: "org_code",
      form: {
        rules: [{
            required: true,
            message: '请输入',
            trigger: 'blur'
          },
          {
            trigger: "blur",
            validator(rule, value, callback) {
              const isTest = value => /^[a-zA-Z0-9]{0,20}$/g.test(value);
              if (isTest(value)) {
                callback();
              } else {
                callback("只能输入字母、数字,1-20个字符")
              }
            }
          }
        ]
      }
    }, {
      label: "组织名称",
      prop: "org_name",
      overflow: true,
      form: {
        component: "el-input",
        componentProps: {
          placeholder: "组织名称"
        },
        rules: [{
          required: true,
          message: '请输入',
          trigger: 'blur'
        }]

      }
    },

    {
      label: "上级组织",
      prop: "parent_org_id",
      hiden: true,
      form: {
        newRow: true,
        // slot: true,
        component: "el-cascader",
        rules: [],
        componentProps: {
          "show-all-levels": false,
          clearable: true,
          options: [{
            id: 0,
            org_name: "无",
          }],
          placeholder: "选择上级组织机构",
          props: {
            value: 'id',
            label: 'org_name',
            children: 'children',
            checkStrictly: true,
            expandTrigger: 'hover',
          }
        },
        options: {
          list: [],
          // valueKey:"route_id",
          valueField: "id",
          filter(val, list) {
            // return val;
            // console.log(val);
            if (val == '0') {
              return "无"
            } else {
              // console.log(list);
              const a = list[val]
              return a ? a['org_name'] : "未知"
            }
          }
        }

      }
    },
    {
      label: "上级机构",
      prop: "parent_org_name",
      overflow: true,

      form: {
        addDisplay: false,
        editDisplay: false
      }
    },

    {
      label: "排序号",
      prop: "orders",
      form: {
        component: "el-input-number",
      }
    },
    {
      label: "状态",
      prop: "state",
      slot: true,
      form: {
        span:24,
        component: "el-switch",
        componentProps: {

        }
      }
    },

  ]
  return {
    ...cruds,
    crudObj,
  }
}


function useLeftTree(setups, cruds) {
  const treeRef = ref(null)
  const tree = reactive({
    data: [],
    props: {
      label: "org_name",
      children: "children"
    },
    filterMsg: "",
  })

  // 实现过滤功能。
  watch(() => tree.filterMsg, (nv, ov) => {
    // console.log(nv);
    treeRef.value.filter(nv);
  })

  /** 树节点过滤 */
  const filterNode = (value, data) => {
    if (!value) return true;
    return data.org_name.indexOf(value) !== -1;
  }

  // 递归获取一个树的所有route_id
  function getNodeRouteId(node, getList = []) {
    getList.push(node.id);
    if (node.children && node.children.length > 0) {
      node.children.forEach(item => {
        getNodeRouteId(item, getList)
      })
    }
  }

  /** 树节点点击 */
  const nodeClick = (node) => {
    // console.log(node);
    // cruds.loading.value = true;
    const idList = [];
    getNodeRouteId(node, idList);

    // 在条件数组上挂载一个数据。
    cruds.condition.value.idList = idList;
    cruds.refresh();

  }


  const treeHeight = computed(() => {
    return window.hei.value - 198 + 'px';
  })

  tree.nodeClick = nodeClick;
  tree.filterNode = filterNode;
  return {
    tree,
    treeRef,
    treeHeight,
  }
}
