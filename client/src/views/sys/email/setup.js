/*
 * @copyright: Huang Ding
 * @Author: ding-cx
 * @Date: 2021-01-08 11:05:22
 * @LastEditors: ding-cx
 * @LastEditTime: 2021-08-13 13:40:57
 * @Description: file content
 */

import {
  getCurrentInstance,
  reactive,
  ref
} from "@vue/composition-api"

import useCrud from "@/hooks/crud"

import svs_sys_email from "@/service/sys/email"
import sys_permission1 from "@/service/sys/permission";


let root;


export default {
  name: "sys-email",
  setup(props, ctx) {
    return useEnter(props, ctx)
  }
}

/**
 *组合api功能入口。组件中调用。
 *
 * @export
 * @param {*} props_
 * @param {*} ctx_
 * @return {*} 
 */
function useEnter(props, ctx) {

  const setups = {
    props: props,
    ctx: ctx,
    root: ctx.root,
    that: getCurrentInstance(),
  }
  root = ctx.root;

  const options = {

    getMethod(page, condition) {
      return svs_sys_email.getSearchData({
        page: [page.size, page.offset],
        searchs: {}
      })
    }
  }

  setups.that.proxy.$on("get-list", (msg) => {
    console.log(msg);
  })


  const cruds = useCrud(options)
  useCrudOption(cruds.option)
  const addSubmit = (data, loading, done) => {

  }
  const rowEditSubmit = (data, loading, done) => {

  }

  const rowDel = (scope) => {

  }

  const sendEmail = () => {
    svs_sys_email.sendEmail({
      to: "981708521@qq.com"
    }).then(res => {
      console.log(res);
      root.$message.success("已发送！")
    })
  }

  // 初始化时get data
  cruds.refresh();
  return {
    ...cruds,
    addSubmit,
    rowEditSubmit,
    rowDel,
    sendEmail,
  }
}




/**
 *专门处理crud 组件的option配置。
 *
 * @param {*} option
 */
function useCrudOption(option) {
  option.table.column = [{
      label: "发送者",
      prop: "sender"
    },
    {
      label: "接收者",
      prop: "receiver"
    },
    {
      label: "发送时间",
      prop: "send_time"
    },
    {
      label: "主题",
      prop: "subject"
    },
    {
      label: "状态",
      prop: "state"
    },
  ]

  /** 搜索查询配置 */
  option.search.items = [{
    label: "查询",
    prop: "search",
    component: "el-input",
    componentProps: {
      placeholder: "请输入内容"
    },
  }]
}
