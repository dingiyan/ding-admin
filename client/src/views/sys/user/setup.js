/*
 * @copyright: Huang Ding
 * @Author: ding-cx
 * @Date: 2021-01-08 11:05:22
 * @LastEditors: ding-cx
 * @LastEditTime: 2021-08-13 13:45:49
 * @Description: file content
 */

import {
  getCurrentInstance,
  inject,
  reactive,
  ref
} from "@vue/composition-api"

import objectDeepClone from '@/utils/objectDeepClone'

import useCrud from "@/hooks/crud"

import svs_sys_user from "@/service/sys/user"
import svs_sys_role from "@/service/sys/role"
import data from "@/views/pdf/content"

let root;


export default {
  name: "sys-user",
  setup(props, ctx) {
    return useEnter(props, ctx)
  }
}

/**
 *组合api功能入口。组件中调用。
 *
 * @export
 * @param {*} props_
 * @param {*} ctx_
 * @return {*} 
 */
function useEnter(props_, ctx_) {

  const setups = {
    props: props_,
    ctx: ctx_,
    root: ctx_.root,
    that: getCurrentInstance(),
  }
  root = ctx_.root;

  const msg = inject('globalMessage')

  const options = {
    getConfig(page, condition) {
      const config = svs_sys_user.getUserListAxiosConfig();
      config.data = {
        page: [page.size, page.offset],
        searchs: condition,
      }

      return config;
    },
    reduceHeight: 290,
  }

  // setups.that.proxy.$on("get-list", (msg) => {
  //   console.log(msg);
  // })



  const cruds = useCrud(options)
  useCrudOption(cruds.option)
  const addSubmit = (data_, loading, done) => {
    const data = objectDeepClone(data_)
    data.role_ids = data.role_ids.toString();
    // console.log(data);
    // loading()
    // return;
    data.password = "96e79218965eb72c92a549dd5a330112"
    svs_sys_user.post(data).then(data => {
      console.log(data);
      if (data.id > 0) {
        loading();
        msg.saveSuccess();
        cruds.refresh();
      }
    })

  }
  const rowEditSubmit = (data_, loading, done) => {
    const data = objectDeepClone(data_)
    data.role_ids = data.role_ids.toString();
    svs_sys_user.put(data.id, data).then(data => {
      if (data.affectedRows >= 0) {
        loading();
        done();
        msg.editSuccess();
        cruds.refresh();
      }
    })
  }

  const rowDel = (scope) => {
    const {
      row
    } = scope;
    msg.confirm("确定要删除该用户吗？删除不可找回！").then(res => {
      svs_sys_user.delete(row.id).then(res => {
        if (res.affectedRows >= 0) {
          msg.deleteSuccess();
          cruds.refresh();
        }
      })
    }).catch(() => {})
  }

  const beforeOpenDialog = (type, done, rowScope) => {
    // 修改时
    if (type == 1) {
      // console.log(row);
      const row = rowScope.row;
      typeof row.role_ids === 'string' && (row.role_ids = row.role_ids.split(',').map(item => Number(item)));
      row.role_ids === null && (row.role_ids = []);
      // console.log(row);
      done();
    } else {
      done();
    }
  }

  // init get table data list
  cruds.refresh();
  return {
    ...cruds,
    addSubmit,
    rowEditSubmit,
    rowDel,
    beforeOpenDialog,
  }
}




/**
 *专门处理crud 组件的option配置。
 *
 * @param {*} option
 */
function useCrudOption(option) {
  option.table.border = false;
  option.table.column = [{
      label: "用户名",
      prop: "user_name",
      form: {
        componentProps: {
          placeholder: "用户名"
        },
        rules: [{
          required: true,
          message: '请输入',
          trigger: 'blur'
        }]
      }
    },
    {
      label: "真实姓名",
      prop: "real_name",
      form: {
        componentProps: {
          placeholder: "真实姓名"
        },
        rules: [{
          required: true,
          message: '请输入',
          trigger: 'blur'
        }]
      }
    },
    {
      label: "手机号",
      prop: "phone_num",
      form: {
        componentProps: {
          placeholder: "手机号"
        },
        rules: [{
          required: false,
          message: '请输入',
          trigger: 'blur'
        }]
      }
    },
    {
      label: "邮箱",
      prop: "email",
      form: {
        componentProps: {
          placeholder: "邮箱"
        }
      }
    },
    {
      label: "角色",
      prop: "role_ids",
      hiden: true,
      form: {
        span: 24,
        component: "el-select",
        componentProps: {
          placeholder: "选择角色",
          multiple: true,
        },
        rules: [{
          required: true,
          message: '请输入',
          trigger: 'blur'
        }],
        options: {
          list: [],
          label: "role_name",
          valueField: "id",
          valueKey: 'id',
          asyncData(cb) {
            svs_sys_role.search({
              page: [500, 0]
            }).then(res => {
              // console.log(res);
              cb(res.rows)
            })
          }
        }
      }
    },
    {
      label: "启用",
      prop: "is_use",
      slot: true,
      form: {
        component: "el-switch",
        componentProps: {
          activeValue: true,
          inactiveValue: false
        }
      }
    },
    {
      label: "创建时间",
      prop: "createdAt",
      overflow: true,
      form: {
        addDisplay: false,
        editDisplay: false
      }
    },
  ]

  /** 搜索查询配置 */
  option.search.items = [{
    label: "用户名",
    labelWidth: "70px",
    prop: "user_name",
    component: "el-input",
    componentProps: {
      placeholder: "请输入内容",
      clearable: true,
    },
  }]
}
