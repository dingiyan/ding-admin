/*
 * @copyright: Huang Ding
 * @Author: ding-cx
 * @Date: 2021-01-08 11:05:22
 * @LastEditors: ding-cx
 * @LastEditTime: 2021-08-13 13:44:29
 * @Description: file content
 */

import {
  getCurrentInstance,
  reactive,
  ref
} from "@vue/composition-api"

import useCrud from "@/hooks/crud"

import svs_sys_log from "@/service/sys/log"

let root;


export default {
  name: "sys-log",
  setup(props, ctx) {
    return useEnter(props, ctx)
  }
}

/**
 *组合api功能入口。组件中调用。
 *
 * @export
 * @param {*} props_
 * @param {*} ctx_
 * @return {*} 
 */
function useEnter(props_, ctx_) {

  const setups = {
    props: props_,
    ctx: ctx_,
    root: ctx_.root,
    that: getCurrentInstance(),
  }
  root = ctx_.root;

  const options = {

    getMethod(page, condition) {
      console.log(condition);
      const data = {
        page: [page.size, page.offset],
        searchs: condition,
      }
      return svs_sys_log.getSearchData(data)
    },
    // 搜索表单数据转化,结果将在getMethod的第二个参数里拿到。
    searchFormToCondition(formData) {
      console.log(formData);
      const ret = {};
      if (formData.method) {
        ret.method = formData.method;
      }
      return ret;
    }
  }

  setups.that.proxy.$on("get-list", (msg) => {
    // console.log(msg);
  })



  const cruds = useCrud(options)
  cruds.refresh();
  useCrudOption(cruds.option)
  const addSubmit = (data, loading, done) => {

  }
  const rowEditSubmit = (data, loading, done) => {

  }

  const rowDel = (scope) => {

  }

  const onRowDblclick = () => {
    cruds.crud.value.openViewRow()
  }
  return {
    ...cruds,
    addSubmit,
    rowEditSubmit,
    rowDel,
    onRowDblclick,
  }
}




/**
 *专门处理crud 组件的option配置。
 *
 * @param {*} option
 */
function useCrudOption(option) {
  option.table.column = [{
      label: "id",
      prop: "id",
      hiden: true
    },
    {
      label: "操作时间",
      prop: "createdAt",
      width: "140px",
      overflow: true
    },
    {
      label: "耗时(ms)",
      prop: "time",
      overflow: true,
    },
    {
      label: "type",
      prop: "type",
      hiden: true
    },
    {
      label: "日志类型",
      prop: "type_name"
    },
    {
      label: "用户",
      prop: "user_name"
    },
    {
      label: "请求路径",
      prop: "request_url",
      width: "140px",
      overflow: true,
    },
    {
      label: "请求方法",
      prop: "method"
    },
    {
      label: "请求参数",
      prop: "params",
      width: "120px",
      overflow: true,
    },
    {
      label: "请求IP",
      prop: "request_ip"
    },
    {
      label: "http状态",
      prop: "response_status"
    },
    {
      label: "应用状态",
      prop: "app_status"
    },
    {
      label: "结果",
      prop: "result",
      width: "100px",
      overflow: true,
    },
    {
      label: "客户端",
      prop: "user_agent",
      width: "120px",
      overflow: true,
    },
  ]

  option.table.rowMenuColumn = {
    display: false
  }

  option.headerMenu.isAddBtn = false;

  /** 搜索查询配置 */
  option.search.items = [{
    label: "方法",
    prop: "method",
    span: 4,
    component: "el-select",
    componentProps: {
      placeholder: "请选择请求方法",
      clearable: true,
    },
    options: {
      list: [{
          label: "GET",
          value: "GET"
        },
        {
          label: "POST",
          value: "POST"
        },
        {
          label: "PUT",
          value: "PUT"
        },
        {
          label: "DELETE",
          value: "DELETE"
        },
      ],
      valueField: "value",
      valueKey: "value"
    }
  }]
}
