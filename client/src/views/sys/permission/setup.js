/*
 * @copyright: Huang Ding
 * @Author: ding-cx
 * @Date: 2021-01-08 11:05:22
 * @LastEditors: ding-cx
 * @LastEditTime: 2021-08-13 14:00:35
 * @Description: file content
 */

import {
  getCurrentInstance,
  inject,
  reactive,
  ref
} from "@vue/composition-api"

import useCrud from "@/hooks/crud"

import svs_sys_perm from "@/service/sys/permission"
import svs_sys_menus from "@/service/sys/menus"

let root;


export default {
  name: "sys-permission",
  setup(props, ctx) {
    return useEnter(props, ctx)
  }
}

/**
 *组合api功能入口。组件中调用。
 *
 * @export
 * @param {*} props_
 * @param {*} ctx_
 * @return {*} 
 */
function useEnter(props_, ctx_) {

  const setups = {
    props: props_,
    ctx: ctx_,
    root: ctx_.root,
    that: getCurrentInstance(),
  }
  root = ctx_.root;

  const msg = inject('globalMessage')

  const options = {
    getConfig(page, condition) {
      console.log(condition);
      const config = {
        url: svs_sys_perm.api + "getSearchData",
        method: "post",
        data: {
          page: [page.size, page.offset],
          searchs: condition
        }
      }

      // console.log(config);

      return config;
    },
    // searchFormToCondition(data) {
    //   // console.log(data);
    //   return data
    // },
    reduceHeight: 290,
  }

  // setups.that.proxy.$on("get-list", (msg) => {
  //   console.log(msg);
  // })



  const cruds = useCrud(options)
  useCrudOption(cruds.option)
  const addSubmit = (data, loading, done) => {
    console.log(data);
    if (Array.isArray(data.menu_id) && data.menu_id.length > 0) {
      data.menu_id = data.menu_id[data.menu_id.length - 1]
    }
    svs_sys_perm.post(data).then(data => {
      console.log(data);
      if (data.id > 0) {
        loading();
        msg.saveSuccess();
        cruds.refresh();
      }
    })

  }
  const rowEditSubmit = (data, loading, done) => {
    svs_sys_perm.put(data.id, data).then(data => {
      if (data.affectedRows >= 0) {
        loading();
        done();
        msg.editSuccess();
        cruds.refresh();
      }
    })
  }

  const rowDel = (scope) => {
    const {
      row
    } = scope;
    msg.confirm("确定要删除吗？删除不可找回！").then(res => {
      svs_sys_perm.delete(row.id).then(res => {
        if (res.affectedRows >= 0) {
          msg.deleteSuccess();
          cruds.refresh();
        }
      })
    }).catch(() => {})
  }
  // init get table data list
  cruds.refresh();
  return {
    ...cruds,
    addSubmit,
    rowEditSubmit,
    rowDel,
  }
}




/**
 *专门处理crud 组件的option配置。
 *
 * @param {*} option
 */
function useCrudOption(option) {
  // 菜单树获取列表
  const getRouteMenuList = () => {
    svs_sys_menus.getRouteMenuTree().then(data => {
      console.log(data);

      // 将获取到的数据赋值给级联菜单。
      const col = option.table.column.find(item => item.prop === "menu_id")
      if (col) {
        col.form.componentProps.options = data;
        // opt.splice(1, opt.length - 1, ...data)
      }
    })

  }
  option.table.column = [{
      label: "所属菜单",
      prop: "menu_title",
      form: {
        addDisplay: false,
        editDisplay: false,
      }
    },

    {
      label: "所属菜单",
      prop: "menu_id",
      hiden: true,

      form: {
        newRow: false,
        // slot: true,
        component: "el-cascader",
        rules: [{
          required: true,
          message: '必填项',
          trigger: 'change'
        }],
        componentProps: {
          "show-all-levels": false,
          clearable: true,
          options: [{
            route_id: 0,
            title: "无",
          }],
          placeholder: "选择上级菜单",
          props: {
            value: 'route_id',
            label: 'title',
            children: 'children',
            checkStrictly: false,
            expandTrigger: 'hover',
          }
        },
        options: {
          list: [],
          // valueKey:"route_id",
          valueField: "route_id",
          filter(val, list) {
            // console.log(val);
            if (val == '0') {
              return "无"
            } else {
              // console.log(list);
              const a = list[val]
              return a ? a['title'] : "未知"
            }
          }
        }

      }
    },


    {
      label: "权限名称",
      prop: "cname",
      form: {
        componentProps: {
          placeholder: "权限名称"
        },
        rules: [{
            required: true,
            message: '必填项',
            trigger: 'blur'
          }

        ]
      }
    },
    {
      label: "授权标识",
      prop: "ename",
      copy:true,
      form: {
        componentProps:{
          placeholder:"授权标识"
        },
        rules: [{
            required: true,
            message: '必填项',
            trigger: 'blur'
          }

        ]
      }
    },

    // {
    //   label: "创建人",
    //   prop: "create_user_real_name",
    //   form: {
    //     addDisplay: false,
    //     editDisplay: false,
    //   }
    // },
    // {
    //   label: "修改人",
    //   prop: "edit_user_real_name",
    //   form: {
    //     addDisplay: false,
    //     editDisplay: false,
    //   }
    // },
    {
      label: "创建时间",
      prop: "createdAt",
      overflow: true,
      form: {
        addDisplay: false,
        editDisplay: false
      }
    },
  ]

  option.table.border = true;

  getRouteMenuList();

  /** 搜索查询配置 */
  option.search.items = [{
    label: "权限名称",
    prop: "cname",
    component: "el-input",
    componentProps: {
      placeholder: "请输入内容",
      clearable: true,
    },
  }]
}
