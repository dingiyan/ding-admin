/*
 * @copyright: Huang Ding
 * @Author: ding-cx
 * @Date: 2021-02-27 16:19:52
 * @LastEditors: ding-cx
 * @LastEditTime: 2021-03-19 09:44:23
 * @Description: 权限管理相关api service
 */

import Service from '@/service/Service'

import RouteConst from "../routeConst"
export class SysPermissionService extends Service {

  /** 批量设置角色的权限。 
   * data = {
   *           roleIdList:[12],
   *           menusList:[23],
   *           commonList:{12:[23]},
   *           customList:{12:[3]},
   * }
   */
  async batchSetRolePermission(data) {
    const config = {
      method: "post",
      url: this.api + "/batchSetRolePermission",
      data,
    }
    return this.axios(config)
  }

  /** 获取自定义权限列表，根据菜单id */
  async getListByMenuId(menu_id) {
    return this.axios({
      method: "get",
      url: this.api + '/getListByMenuId/' + menu_id,
    })
  }

  /** 根据角色id获取其所有权限list */
  async getAllPermByRoleId(role_id = 0) {
    return this.axios({
      method: "get",
      url: this.api + "/getAllPermByRoleId/" + role_id
    })
  }

  
}

const sys_permission1 = new SysPermissionService(RouteConst.sys.perm);
export default sys_permission1;