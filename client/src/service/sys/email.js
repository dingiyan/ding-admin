/*
 * @copyright: Huang Ding
 * @Author: ding-cx
 * @Date: 2021-02-27 16:19:52
 * @LastEditors: ding-cx
 * @LastEditTime: 2021-04-12 15:50:06
 * @Description: file content
 */

import Service from '@/service/Service'
import RouteConst from "../routeConst"
export class SysEmailService extends Service {
  /** 发送邮件 */
  async sendEmail(data) {
    return await this.axios({
      method: "post",
      url: this.api + "sendEmail",
      data,
    })
  }
}

export default new SysEmailService(RouteConst.sys.email);
