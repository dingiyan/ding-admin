/*
 * @copyright: Huang Ding
 * @Author: ding-cx
 * @Date: 2021-02-27 16:19:52
 * @LastEditors: ding-cx
 * @LastEditTime: 2021-03-26 16:28:17
 * @Description: file content
 */

import Service from '@/service/Service'
import RouteConst from "../routeConst"
export class SysOssService extends Service {

  /** 上传文件接口 */
  async upload(fileObj) {
    const config = {
      url: this.api + "upload",
      method:"post",
      data: fileObj
    }
    return this.axios(config)
  }
}

export default new SysOssService(RouteConst.sys.oss);
