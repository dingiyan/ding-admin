/*
 * @copyright: Huang Ding
 * @Author: ding-cx
 * @Date: 2021-02-27 16:19:52
 * @LastEditors: ding-cx
 * @LastEditTime: 2021-03-12 18:13:54
 * @Description: file content
 */

import Service from '@/service/Service'
import RouteConst from "../routeConst"
export class SysMenusService extends Service {
  /** 获取路由表树结构 */
  async getRouteMenuTree() {
    const url = this.api + 'getRouteTree';
    const menu = await this.axios({
      url,
      method: "get",
    })
    // console.log(menu);
    return menu;
  }
  /** 获取路由表树结构 权限限制 */
  async getRouteMenu() {
    const url = this.api + 'getRouteMenu';
    const menu = await this.axios({
      url,
      method: "get",
    })
    // console.log(menu);
    return menu;
  }

  /** 获取路由列表，根据上级菜单id */
  async getRouteListByParentId(parentId) {
    const url = this.api + "getRouteByParentId/" + parentId;
    return await this.axios({
      url,
      method: 'get'
    })
  }
  /** 获取路由列表，根据树节点 */
  async getRouteListByTreeNode(idList = []) {
    const url = this.api + "getRouteListByTreeNode/";
    return await this.axios({
      url,
      method: 'post',
      data: {
        idList
      }
    })
  }

  /** 提供获取路由列表，根据树节点的请求配置 */
  getRouteListConfigByTreeNode() {
    const url = this.api + "getRouteListByTreeNode/";
    return {
      url,
      method: 'post',
      data: {}
    }
  }
}

export default new SysMenusService(RouteConst.sys.menus);
