/*
 * @copyright: Huang Ding
 * @Author: ding-cx
 * @Date: 2021-02-27 16:19:52
 * @LastEditors: ding-cx
 * @LastEditTime: 2021-03-24 16:13:52
 * @Description: file content
 */

import Service from '@/service/Service'
import RouteConst from "../routeConst"
export class SysConfigService extends Service {

  /** 获取配置对象 */
  async getConfigObject() {
    return this.axios({
      url: this.api + "get/object",
      method: "get"
    })
  }
}

export default new SysConfigService(RouteConst.sys.config);
