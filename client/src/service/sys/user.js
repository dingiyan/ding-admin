/*
 * @copyright: Huang Ding
 * @Author: ding-cx
 * @Date: 2021-02-27 16:19:52
 * @LastEditors: ding-cx
 * @LastEditTime: 2021-03-02 11:55:43
 * @Description: file content
 */

import Service from '@/service/Service'
import RouteConst from "../routeConst"
export class SysUserService extends Service {
  /** 获取用户列表的axios config */
  getUserListAxiosConfig() {
    const config = {
      url: this.api + 'getSearchData',
      method: 'post',
      data: {}
    }
    return config;
  }
}

export default new SysUserService(RouteConst.sys.user);
