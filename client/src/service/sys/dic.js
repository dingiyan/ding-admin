/*
 * @copyright: Huang Ding
 * @Author: ding-cx
 * @Date: 2021-02-27 16:19:52
 * @LastEditors: ding-cx
 * @LastEditTime: 2021-03-20 14:51:30
 * @Description: file content
 */

import Service from '@/service/Service'
import RouteConst from "../routeConst"
export class SysdicService extends Service {

  /** 获取菜单字典列表 整个的。 */
  async getDicList() {
    return this.axios({
      url: RouteConst.sys.dic + "-list/getDicList",
      method: "get"
    })
  }

  /** 新增一个菜单 */
  async AddOneDic(data) {
    return this.axios({
      method: "post",
      url: RouteConst.sys.dic + "-list",
      data,
    })
  }
}

export default new SysdicService(RouteConst.sys.dic);
