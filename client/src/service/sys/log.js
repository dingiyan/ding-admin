/*
 * @copyright: Huang Ding
 * @Author: ding-cx
 * @Date: 2021-02-27 16:19:52
 * @LastEditors: ding-cx
 * @LastEditTime: 2021-03-26 11:47:30
 * @Description: file content
 */

import Service from '@/service/Service'
import RouteConst from "../routeConst"
export class SysLogService extends Service {
    
}

export default new SysLogService(RouteConst.sys.log);
