/*
 * @copyright: Huang Ding
 * @Author: ding-cx
 * @Date: 2021-02-27 16:19:52
 * @LastEditors: ding-cx
 * @LastEditTime: 2021-03-22 16:04:25
 * @Description: file content
 */

import Service from '@/service/Service'
import RouteConst from "../routeConst"
export class SysOrgService extends Service {

  /** 获取组织机构树结构 */
  async getTreeList() {
    const url = this.api + 'tree/getTreeList';
    const menu = await this.axios({
      url,
      method: "get",
    })
    // console.log(menu);
    return menu;
  }

  /** 根据左侧树获取idlist的列表 的api配置。*/
  getListByIdListConfig() {
    const url = this.api + "tree/getOrgListByIdList";
    const ret = {
      url,
      method: "post",
      data: {},
    }
    return ret;
  }
}

export default new SysOrgService(RouteConst.sys.org);
