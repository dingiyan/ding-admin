/*
 * @copyright: Huang Ding
 * @Author: ding-cx
 * @Date: 2021-03-02 11:22:31
 * @LastEditors: ding-cx
 * @LastEditTime: 2021-04-12 14:42:33
 * @Description: file content
 */

// 基本api前缀
export const api_prefix = "/api";

export default {
  /** 系统管理 */
  sys: {
    /** 菜单管理 */
    menus: api_prefix + '/sys/menus',
    /** 用户管理 */
    user: api_prefix + '/sys/user',
    /** 角色管理 */
    role: api_prefix + "/sys/user/role",
    /** 权限管理 */
    perm: api_prefix + "/sys/permission",
    /** 日志管理 */
    log: api_prefix + "/sys/log",
    /** 字典管理 */
    dic: api_prefix + "/sys/dic",
    /** 组织机构管理 */
    org: api_prefix + "/sys/org",
    /** 系统配置管理 */
    config: api_prefix + "/sys/config",
    /** 云存储，上传文件管理 */
    oss: api_prefix + "/sys/oss",
    /** 系统 邮件发送 */
    email: api_prefix + "/sys/email"
  }
}
