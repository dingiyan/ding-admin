/*
 * @copyright: Huang Ding
 * @Author: ding-cx
 * @Date: 2020-12-27 15:11:38
 * @LastEditors: ding-cx
 * @LastEditTime: 2021-04-12 16:10:12
 * @Description: api service 封装基类
 */


import axios from '@/utils/request'






/**
 * 封装的api service 基类。实现api调用封装。
 *
 * @export
 * @class Service
 */
export default class Service {

  /**
   *基本api路径 + 实例化类时传入的api名称组合成的一个api路径（相当基本完整，但还可继续添加后缀）。
   *
   * @memberof Service
   */
  api = null;

  /**
   *将axios放在service对象上。方便继承本基类时调用请求。
   *
   * @memberof Service
   */
  axios = axios;
  /**
   * 创建service基类的实例对象.
   * @param {string} apiName api基本路径（名称）。（不含通用前缀和后缀）必须在前面加斜杠：/
   * @memberof Service
   */
  constructor(apiName) {
    if (apiName) {
      this.api = apiName;
      if (this.api.substr(this.api.length - 1) !== "/") {
        this.api += '/';
      }
    } else {
      throw new Error("实例化service基类必须传入api 基本路径名称。")
    }
  }

  /**
   *获取列表。
   *
   * @return {array} 
   * @memberof Service
   */
  async get() {
    const config = {
      method: "get",
      url: this.api,
    }
    return await axios(config)
  }


  /**
   *根据主键id获取一条数据
   *
   * @param {number} [id=0]
   * @memberof Service
   */
  async show(id = 0) {
    const config = {
      method: "get",
      url: this.api + id,
    }
    return await axios(config)
  }

  
  /**
   *搜索获取列表api
   *
   * @param {object} [args={}] 搜索接收的参数 
   * {  
   * page:[page.size,page.offset],
   * searchs:{}
   * }
   * @return {Promise} 
   * @memberof Service
   */
  async getSearchData(args = {}) {

    const data = args;

    const config = {
      method: "post",
      url: this.api + "getSearchData",
      data,
    }
    return await axios(config)
  }

  /**
   *搜索获取列表api 。getSearchData方法的别名
   *
   * @param {object} [args={}] 搜索接收的参数 {  userid: this.userInfo.userId,
          funid: this.funid,
          pageindex: this.page.currentPage,
          pageSize: this.page.pageSize,
          searchData: this.searchData,}
   * @return {Promise} 
   * @memberof Service
   */
  async search(args = {}) {
    return await this.getSearchData(args)
  }

  /**
   *新增api
   *
   * @param {object} formData 新增的表单数据
   * @return {Promise} 
   * @memberof Service
   */
  async post(formData) {
    const config = {
      method: "post",
      url: this.api,
      data: formData,
    }
    return await axios(config)
  }

  /**
   *删除api
   *
   * @param {number} autoId 要删除的数据的autoId
   * @return {Promise} 
   * @memberof Service
   */
  async delete(autoId) {
    const config = {
      method: "delete",
      url: this.api + autoId,
    }
    return await axios(config)
  }


  /**
   *修改api
   *
   * @param {number} autoId 要修改的数据的autoId
   * @param {object} formData 表单修改后的数据。
   * @return {Promise} 
   * @memberof Service
   */
  async put(autoId, formData) {
    const config = {
      method: "put",
      url: this.api + autoId,
      data: formData
    }
    return await axios(config)
  }

  /**
   *批量删除api
   *
   * @param {Object} [data={id: []}] data，一般是id字段携带idArrayList
   * @return {Promise} 
   * @memberof Service
   */
  async batchDelete(data = {
    id: []
  }) {
    const config = {
      method: "post",
      url: this.api + "batchDelete",
      data,
    }
    return await axios(config)
  }

}
