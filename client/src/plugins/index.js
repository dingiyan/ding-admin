/*
 * @copyright: Huang Ding
 * @Author: ding-cx
 * @Date: 2021-03-01 08:51:45
 * @LastEditors: ding-cx
 * @LastEditTime: 2021-04-23 15:02:33
 * @Description: file content
 */

import xlsxPlugin from "./xlsx"


import BaseForm from "@/components/base/BaseForm"
import BaseCrudTable from "@/components/base/BaseCrudTable"
import JsonEditor from "@/components/JsonEditor"
import PermissionSelectPannel from '@/views/sys/permission/component/PermissionSelectPannel'
import BaseIconSelect from '@/components/base/BaseIconSelect'
import Vue from "vue"
import checkPermission from "@/utils/permission"
// 指令
import permission from "@/directive/permission/index"
import clipboard2 from '@/directive/clipboard/clipboard2' // use clipboard by v-directive
import axios from "@/utils/request"

export default (Vue) => {
  Vue.use(xlsxPlugin)
  Vue.component('BaseForm', BaseForm)
  Vue.component('BaseCrudTable', BaseCrudTable)
  Vue.component('PermissionSelectPannel', PermissionSelectPannel)
  Vue.component(JsonEditor.name, JsonEditor)
  Vue.component(BaseIconSelect.name, BaseIconSelect)
  Vue.prototype.dingAdminConfig = window.dingAdmin.config;
  Vue.prototype.$permission = checkPermission;
  Vue.prototype.$axios = axios;
  // 指令
  Vue.use(permission)
  Vue.directive('clipboard', clipboard2)
}

// create a event bus 
export const vueBus = new Vue();
