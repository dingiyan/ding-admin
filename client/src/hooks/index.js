/*
 * @copyright: Huang Ding
 * @Author: ding-cx
 * @Date: 2021-03-01 09:21:34
 * @LastEditors: ding-cx
 * @LastEditTime: 2021-03-24 09:55:02
 * @Description: file content
 */


/** 封装消息提示函数。 */
export const useMessage = (root) => {
  const saveSuccess = () => {
    root.$message.success("保存成功！")
  }
  const editSuccess = () => {
    root.$message.success("修改成功！")
  }

  const deleteSuccess = () => {
    root.$message.success("删除成功！")
  }

  const emptyWarning = () => {
    root.$message.warning("请选择至少一项再进行操作！")
  }

  const confirm = (msg) => {
    return root.$confirm(msg || '确定要删除该内容吗？', '请确认', {
      confirmButtonText: '确定',
      cancelButtonText: '取消',
      type: 'warning'
    })
  }


  return {
    saveSuccess,
    editSuccess,
    deleteSuccess,
    confirm,
    emptyWarning,
  }
}
