/*
 * @copyright: Huang Ding
 * @Author: ding-cx
 * @Date: 2020-11-10 12:10:01
 * @LastEditors: ding-cx
 * @LastEditTime: 2021-03-19 10:17:15
 * @Description: file content
 */
// import store from '@/store'
import checkPermission2 from "@/utils/permission"

function checkPermission(el, binding) {
  const { value } = binding
  // const perm_name_list = store.getters && store.getters.custom_perm_list

  if(!checkPermission2(value)){
    el.parentNode && el.parentNode.removeChild(el)
  }

  // if (value && value instanceof Array) {
  //   if (value.length > 0) {
  //     const permissionRoles = value

  //     const hasPermission = perm_name_list.some(role => {
  //       return permissionRoles.includes(role)
  //     })

  //     if (!hasPermission) {
  //     }
  //   }
  // } else {
  //   throw new Error(`need roles! Like v-permission="['admin','editor']"`)
  // }
}

export default {
  inserted(el, binding) {
    checkPermission(el, binding)
  },
  update(el, binding) {
    checkPermission(el, binding)
  }
}
