/*
 * @Author: dingding
 * @Date: 2020-01-04 11:20:25
 * @LastEditors: ding-cx
 * @LastEditTime: 2021-04-02 10:45:50
 * @Description: clipboard 剪切板指令
 */

import Vue from 'vue'
import clipboard from 'clipboard'

/* value 要复制的值
modifiers self 被绑定者自己身上添加点击事件，点击可复制，没有self时则会在元素内创建btn，点击btn才能复制 
 */
export default {
  /* 在bind中绑定的，虽然可实现只绑定一次剪切板实例，但value却是不响应的 */
  bind(el, binding) {
    if (binding.modifiers.self) {
      el.style.cursor = 'pointer';
      el.setAttribute('title', '点击可复制');
      const cp = new clipboard(el, {
        text() {
          return binding.value;
        }
      });
      cp.on('success', (arg) => {
        // console.log(arg);
        const str = arg.text.length > 30 ? arg.text.substr(0, 30) + "..." : arg.text;
        Vue.prototype.$message.success('【' + str + '】 已复制');
      })
      cp.on('error', () => {
        Vue.prototype.$message.warning('复制失败');
      })
      el._dataset_clipboard = cp; //钩子间传递了clipboard对象，方便数据更新时再调用，而不是销毁重新实例化
    }
  },
  /* 组件更新后调用，不在insert等钩子调用，
  这样当value更新时，才能copy最新的binding.value 
  但是相当于每次组件，或值，更新都将调用此钩子，造成事件不断绑定
  只适合直接绑定在元素上，绑在封装组件上有问题
  */
  componentUpdated(el, binding) {
    if (binding.modifiers.self) {
      if (el._dataset_clipboard) {
        el._dataset_clipboard.text = function () {
          return binding.value;
        };
      }
      return;
    }
    let cp; //clipboard object

    const btn = document.createElement('button');
    const text = document.createTextNode('复制');
    btn.setAttribute('class', 'copyBtn el-button el-button--primary el-button--small');
    btn.setAttribute('type', 'button');

    btn.style.position = 'absolute';
    btn.style.bottom = "1px";
    btn.style.right = "1px";
    btn.appendChild(text);
    // btn.onclick = ()=>{

    // }
    el.onmouseenter = () => {
      btn.setAttribute('data-clipboard-text', binding.value);
      el.appendChild(btn);
      cp = new clipboard('.copyBtn')
      cp.on('success', (arg) => {
        // console.log(arg);
        Vue.prototype.$message.success(arg.text + ' 复制成功');
      })
      cp.on('error', () => {
        Vue.prototype.$message.warning('复制失败');
      })
    }
    el.onmouseleave = () => {
      cp.destroy();
      el.removeChild(btn);
    }
    el.style.position = "relative";
    el.style.zIndex = 30000;
    // console.log(el);
    // console.log(el.innerText);
  }
}
