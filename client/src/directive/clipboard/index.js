/*
 * @copyright: Huang Ding
 * @Author: ding-cx
 * @Date: 2020-11-10 12:10:01
 * @LastEditors: ding-cx
 * @LastEditTime: 2021-04-02 10:36:30
 * @Description: file content
 */
import Clipboard from './clipboard'

const install = function(Vue) {
  Vue.directive('clipboard', Clipboard)
}

if (window.Vue) {
  window.clipboard = Clipboard
  Vue.use(install); // eslint-disable-line
}

Clipboard.install = install
export default Clipboard
