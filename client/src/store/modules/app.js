/*
 * @copyright: Huang Ding
 * @Author: ding-cx
 * @Date: 2020-11-10 12:10:01
 * @LastEditors: ding-cx
 * @LastEditTime: 2021-04-23 14:48:22
 * @Description: file content
 */
import Cookies from 'js-cookie'
import {
  getLanguage
} from '@/lang/index'

const state = {
  sidebar: {
    opened: Cookies.get('sidebarStatus') ? !!+Cookies.get('sidebarStatus') : true,
    display: Cookies.get('sidebarDisplayStatus') ? +Cookies.get('sidebarDisplayStatus') : true,
    withoutAnimation: false
  },
  device: 'desktop',
  language: getLanguage(),
  size: Cookies.get('size') || 'small'
}

const mutations = {
  TOGGLE_SIDEBAR: state => {
    state.sidebar.opened = !state.sidebar.opened
    state.sidebar.withoutAnimation = false
    if (state.sidebar.opened) {
      Cookies.set('sidebarStatus', 1)
    } else {
      Cookies.set('sidebarStatus', 0)
    }
  },
  CLOSE_SIDEBAR: (state, withoutAnimation) => {
    Cookies.set('sidebarStatus', 0)
    state.sidebar.opened = false
    state.sidebar.withoutAnimation = withoutAnimation
  },
  /** 切换侧栏是否隐藏不显示 */
  TOGGLE_DISPLAY_SIDBAR: (state, isDisplay) => {
    Cookies.set('sidebarDisplayStatus', isDisplay);
    state.sidebar.display = isDisplay;
  },
  TOGGLE_DEVICE: (state, device) => {
    state.device = device
  },
  SET_LANGUAGE: (state, language) => {
    state.language = language
    Cookies.set('language', language)
  },
  SET_SIZE: (state, size) => {
    state.size = size
    Cookies.set('size', size)
  }
}

const actions = {
  toggleSideBar({
    commit
  }) {
    commit('TOGGLE_SIDEBAR')
  },
  closeSideBar({
    commit
  }, {
    withoutAnimation
  }) {
    commit('CLOSE_SIDEBAR', withoutAnimation)
  },
  toggleSideBarDisplay({
    commit
  }, isDisplay) {
    commit('TOGGLE_DISPLAY_SIDBAR', isDisplay)
  },
  toggleDevice({
    commit
  }, device) {
    commit('TOGGLE_DEVICE', device)
  },
  setLanguage({
    commit
  }, language) {
    commit('SET_LANGUAGE', language)
  },
  setSize({
    commit
  }, size) {
    commit('SET_SIZE', size)
  }
}

export default {
  namespaced: true,
  state,
  mutations,
  actions
}
