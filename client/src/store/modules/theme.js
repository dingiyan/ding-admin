/*
 * @copyright: Huang Ding
 * @Author: ding-cx
 * @Date: 2021-03-17 13:45:24
 * @LastEditors: ding-cx
 * @LastEditTime: 2021-03-22 11:48:51
 * @Description: file content
 */
import Cookies from 'js-cookie'

import {
  defaultDingAdminTheme
} from "@/settings"

/** 设置body的class，用来改变主题。样式。 */
function toggleBodyThemeClass(name) {
  const theme_name = "theme-" + name;
  const tempExisted = [];
  document.body.classList.forEach(item => {
    if (item.indexOf('theme-') > -1) {
      tempExisted.push(item)
    }
  })
  tempExisted.forEach(item => document.body.classList.remove(item))
  document.body.classList.add(theme_name)

}

const state = {
  theme_name: Cookies.get('theme_name') || defaultDingAdminTheme,
  theme_list: [{
    label: "默认",
    prop: "default",
  }, {
    label: "纯白之雪",
    prop: "white"
  }]

}

toggleBodyThemeClass(state.theme_name);




const mutations = {
  SET_THEME: (state, theme_name) => {
    state.theme_name = theme_name;
    toggleBodyThemeClass(state.theme_name);
    Cookies.set('theme_name', theme_name)
  },

}

const actions = {

}

export default {
  namespaced: true,
  state,
  mutations,
  actions
}
