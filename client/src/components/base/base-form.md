<!--
 * @Author: your name
 * @Date: 2020-05-05 14:37:47
 * @LastEditTime: 2020-07-30 18:05:26
 * @LastEditors: Please set LastEditors
 * @Description: In User Settings Edit
 * @FilePath: \ipms\src\components\form.md
 -->

# form 数据驱动表单组件

> form表单组件在于传入一些配置即可生成表单，方便快速地生成表单，减少繁琐操作。

## 属性、事件

> 组件的属性或自定义事件

名称 | 说明 | 类型 | 可选值 | 默认值
|:---|:----|:-----|:----|:-----|
option | 配置对象，详细见下文 | object | 必填 | -
v-model | 双绑数据 | object | 必填 | -
@submit | 提交事件 ，提交按钮点击触发 | event callback func |  参数：formdata，loading() | -
@change | 表单change事件，某一个表单change都会触发 | event callback func | - | -


## option 属性
> option是驱动整个组件的一个对象，它采用对象表示法，每一个表单元素其实都是一个子对象，通过这些对象的配置就可以驱动表单的生成与展示。

名称 | 说明 | 类型 | 可选值 | 默认值
|:---|:----|:-----|:----|:-----|
labelWidth | 作用在el-form组件的全局label宽度 | string | 表示宽度的值 | 80px
labelPosition | 作用再el-form组件上的全局label位置 | string |left、center、right|right|
gutter | 作用在el-row上的gutter | number | - | 0
menu | 表单菜单的相关配置，详细见下文 | object | - | -
items | 表单项，每项就是一个表单的配置对象，详细见下文 | array | - | -
rules | 表单form全局验证规则 | object[array] | 表单验证配置对象| {}
hideErrorMsg | 是否隐藏表单的校验提示信息，某些时候不需要校验表单但行距太宽 | boolean | true、false | false


### option.menu 菜单配置
> 表单的菜单配置，如提交按钮，清空按钮，菜单的位置等

名称 | 说明 | 类型 | 可选值 | 默认值
|:---|:----|:-----|:----|:-----|
display | 菜单栏是否显示，若设置不显示，则内部按钮的display属性设置将不起作用 | boolean | true，false | true
span | 菜单栏的栅格宽度，同el-col的span | number | 1-24 | 24
align | 菜单栏el-col的align | string | left,center,right | center
submitBtn | 提交按钮的配置，如下 | object | - | -
submitBtn.type | 提交按钮type | string | el-button的可选值 | primary
submitBtn.size | 提交按钮size | string | el-button的可选值 | medium
submitBtn.icon | 提交按钮icon | string | el-button的可选值 | el-icon-check
submitBtn.text | 提交按钮文本 | string | - | 提交
resetBtn | 清空按钮的配置，如下 | object | - | -
resetBtn.type | 清空按钮type | string | el-button的可选值 | ""
resetBtn.size | 清空按钮size | string | el-button的可选值 | medium
resetBtn.icon | 清空按钮icon | string | el-button的可选值 | el-icon-close
resetBtn.text | 清空按钮文本 | string | - | 清空




### option.items[{}] 每一项表单组件配置
> 表单的主体配置，option.items是一个array，array的每一个值应该是一个表单组件配置对象。这个配置对象的配置项如下：

名称 | 说明 | 类型 | 可选值 | 默认值
|:---|:----|:-----|:----|:-----|
label | 表单label | string | - | ''
prop | 表单属性名（用于保存值和唯一识别id） | string | 必填 | 
component | 表单组件名，比如el-input；组件选项对象； | string,object | 已注册组件，或者组件选项，参考vue的component元组件，注意需要且能提供v-model响应 | -
componentProps | 表单组件的属性 | object | 该组件所有可用的属性，本组件是直接v-bind进去的（element查询详细配置） | -
componentSlots | 组件插槽名的数组，定义它，并在本form组件内以prop-slotname命名插槽，就可将插槽传递给component | array | component含有的插槽 | -
span | 表单栅格span，同el-col的span | number | 1-24 | 6
slot | 控制是否使用插槽 | true，false |  | false
display | 是否显示 | boolean | - | - |
labelWidth | 表单label宽度 | string | - | 80px
table | 开启表格批量组件，开启之后需配置columns等，详细见下文表格式批量表单 | boolean | true,false | false
newRow | 是否新起一行，默认的el-col是堆叠的 | boolean | true,false | false
rules | 绑定在form-item上的单个表单验证规则，使用参考elment | object[array] | - | -
options | select，checkbox-group,radio-group组件特殊需要的配置，可选列表，详细见下面 | object | - | component是el-select时必须
options.list | 选项列表 | array | 每项为对象或纯字符串;纯字符串时，value和label一致,单多选就不能设置label，valueKey，select不能设置valueField | -
options.label | 当选项item为对象时，规定select，单多选的label显示字段名， | string | - | -
options.valueKey | 当选项item绑定为对象时。规定valuekey （select组件要v-model匹配，必须接受一个一样的对象）| string | - | -
options.valueField | select组件特需，当选项item为对象，但绑定值为对象的某一个field时，规定某个字段作为普通值。value绑定为这个值。(此时当v-model传入一个值，select就能匹配到。) | string | - | -


### 表格式批量表单
> 有时候一些结构化的数据录入，需要一种表格式的批量表单。按行可增删排序。需开启table，并提供相关配置。相关的items的配置如下：

名称 | 说明 | 类型 | 可选值 | 默认值
|:---|:----|:-----|:----|:-----|
table | 开启表格式批量子表单 | boolean | true,false | false
columns | table模式必须的主体配置，传递每一列的表单组件及其配置，基本配置与items相近，只需label,prop，component，componentSlots,compomentProps等配置即可；另外可配置width控制col列宽 | array | - | -
tableOption | 传递一个table模式时，对table及其功能的相关配置对象，内部可配置内容如下 | object | - | -
tableOption.addRowBtn | 配置表格最下方的添加一行的按钮，所有项均可选：{display,align,size,type,disabled,text}  | object | - | -
tableOption.indexColumn | 配置序号列，{display,index} | object | - | -
tableOption.operationColumn | 操作列，{upMoveBtn,downMoveBtn,clearBtn,deleteBtn}每项均为布尔值，控制按钮显隐 | object | - | -
tableOption.operationColumn.width | 操作列的宽度，当其他列较宽，操作列可能需要固定时需要设置宽度 | string,number | - | -
tableOption.operationColumn.fixed | 操作列的固定方式，true,left,right | boolean，string  | - | -


### 插槽
组件提供了以下插槽：

名称 | 含义 | 参数 | 备注
|:---|:----|:-----|:------|
|menu | 菜单行的插槽 | 
|prop-slot name | - | 每一个表单的prop加上-再加上组件的原来的插槽名，就可以传递组件的插槽进去 | - |
| prop | 逃生舱，兜底自定义插槽 | {option:item option} |以`item.slot`规定使用插槽定义该表单。然后以prop命名插槽，作用域为option

### ref 

每个表单item都以自己的prop命名的ref。可通过如下获取：
```
this.$refs.baseForm.$refs.propName
```
