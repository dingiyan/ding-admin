# crud-table 数据驱动增删改查表格组件

> crud-table是数据配置驱动的增删改查表格组件，提供表格数据展示，快捷增加删除修改搜索功能的组件

## 属性、事件

> 组件的属性或自定义事件

名称 | 说明 | 类型 | 可选值 | 默认值
|:---|:----|:-----|:----|:-----|
v-model | 增改表单数据双绑 | object | - | 
option | 配置对象，详细见下文 | object | 必填 | -|
data | 表格数据 | array | - | -|
page | 分页参数，可带sync | object | 形如：{total,currentPage,size} | - |
loading | 加载动态 | 
before-open-dialog | 打开dialog前的钩子函数，实现自定义的操作时可用 | Function | - | args:(type,done[,row]) |
@page-change | 分页的size和currentPage发生变化时触发 | 参数为一个对象，{currentPage,size,offset} | - | - |
@refresh | 刷新 | 自定义事件 | - | -|
@add-submit | 添加时，提交表单 | formData,loading,done 数据、按钮loading态func、关闭dialog的func | - | -
@row-edit-submit | 修改时提交表单 | formData,loading,done 同上addSubmit | - | -
@search-submit | 搜索区域提交表单 |
@search-change | 搜索区域表单change | 
@search-reset | 搜索区域重置按钮 |
@search-btn-click | 搜索区域开关按钮事件 | 参数为开关bool |
@selection-change | 选中行change，参照element的同名事件 |
@row-del | 行删除事件 | 参数为行scope |
@row-menu-dropdown | 行下拉菜单命令事件 | 参数为行scope和下拉菜单command|
@cell-click | 单元格click事件 | 参照element事件， row, column, cell, event |
@row-click | 行click事件 | 参照element事件， row, column, event |
@row-dblclick | 行双击事件 | 参照element事件， row, column, event |
@dialog-open | dialog打开后触发的open事件 | 参数为dialog中的表单组件ref。通过它可进一步拿到item的ref。

## option

> 组件的option配置是最基础的。

名称 | 说明 | 类型 | 可选值 | 默认值 |
|:-----|:----|:-----|:------|:-----|
table | 表格的配置，也包含列的基础配置，详细见下表 | object | 必填 | - |
search | crud上方的搜索区域的表单定义option，[参考form组件](base-form) | object | | 
form | crud的增改表单的一些额外配置，见下文 | 
headerMenu | crud的头部配置，如新增按钮、导出excel按钮的配置等
footerMenu | 底部的配置 | 

## option.table

> table配置是基础配置，配置了表格的内容，和列的内容

名称 | 说明 | 类型 | 可选值 | 默认值 |
|:-----|:----|:-----|:------|:-----|
height |
fit |
border |
stripe | 
headerRowClassName | table的header-row-class-name | 可自定义表头行的class，然后自定义样式
selectionColumn | 多选列配置
indexColumn | index列配置
rowMenuColumn | 行菜单列配置
column | 列配置，详见下表 | array |


## option.table.column

> table.column一个数组，每个元素是表格列的配置，包含基础列信息和表单配置

名称 | 说明 | 类型 | 可选值 | 默认值 |
|:-----|:----|:-----|:------|:-----|
label | 列名称 | 
prop | 列字段名，作为列的关键唯一索引 |
hiden | 列是否隐藏 |
width | 表格列宽 |
headerAlign | 表头对齐 |
align | 内容对齐 | 
copy | 加上clipboard指令实现复制 |
slot | 使用自定义的插槽 | 参数：列的scope，列的配置col | boolean
form | 表单配置，参考form组件，另外新增了一些属性 |


## option.table.indexColumn

> table.indexColumn对index列的相关配置

名称 | 说明 | 类型 | 可选值 | 默认值 |
|:-----|:----|:-----|:------|:-----|
|display | 是否显示index列 |
|index | table组件的index，可提供数字和函数 | number/function | - | - |
|indexLabel | index列名称 | string | - | 序号 |
|pagination | 开启分页递增 | boolean |

## option.table.selectionColumn

> table.selectionColumn选择列的相关配置

名称 | 说明 | 类型 | 可选值 | 默认值 |
|:-----|:----|:-----|:------|:-----|
|display | 是否显示勾选列 | boolean | true/false | false |

## option.table.rowMenuColumn

> table.rowMenuColumn 行菜单功能的相关配置

名称 | 说明 | 类型 | 可选值 | 默认值 |
|:-----|:----|:-----|:------|:-----|
|display | 是否显示 | boolean | true/false | false |
|align | 水平位置 | string | center/left/right | left |
|width | 列宽度(单位px) | string | - | "" |
|label | 列头名 | string | - | "" |
|fixed | 固定 | boolean、string | true/left/right | false |
|slot | 插槽  rowMenu | boolean | true/false | false |



## option.table.column[index].form

> 每个column的form配置参照form组件，也有一些新增配置，新增配置参考下表

名称 | 说明 | 类型 | 可选值 | 默认值 |
|:-----|:----|:-----|:------|:-----|
index | 排序，与col的配置顺序解耦，通过它可以自己决定form的顺序。index越小越在前，默认为0 | number | - | 0
addDisplay | 添加时是否显示 |
editDisplay | 修改编辑时是否显示 |
viewDisplay | 查看时是否显示 |
addDisabled | 添加时是否可用 |
editDisabled | 编辑时是否可用 |
viewDisabled | 查看时是否可用 |
options.filter | options特殊配置里加入了filter函数，用于当数据是对象或数组，或数据需要转换时对原始数据进行过滤，参数：原始数据val，配置的list列表。注意必须要options.list存在才能生效。另外，如果没有配置filter，则将按照form的options配置的label和valueKey对list进行解析对应并显示结果。

options.asyncData | 一个异步获取数据的函数，参数为一个回调函数，其参数将设置list的值。

## option.form
form表单的全局配置，它是将每个column里的item放在自己的items里的。form最终将传递给给base form。
同时form也带一下表单dialog的配置。

名称 | 说明 | 类型 | 可选值 | 默认值 |
|:-----|:----|:-----|:------|:-----|
dialogWidth | 弹窗宽度 | string | - | 60% |
custom | 表单自定义方式，等于slot时，需传入插槽form | string | slot | - |
dialogCloseBtn | 弹窗内关闭按钮是否显示 | boolean | true,false | true |

## option.headerMenu
头部菜单栏配置

名称 | 说明 | 类型 | 可选值 | 默认值 |
|:-----|:----|:-----|:------|:-----|
isAddBtn | 是否显示添加按钮 | boolean | true、false | true
isExportBtn | 是否显示导出表格按钮 | boolean | true、false | true
isRefreshBtn | 是否显示右侧刷新按钮 | boolean | true、false | true
isSearchBtn | 是否显示右侧搜索控制按钮 | boolean | true、false | true

## option.footerMenu
底部菜单栏配置


名称 | 说明 | 类型 | 可选值 | 默认值 |
|:-----|:----|:-----|:------|:-----|
isAddBtn | 是否显示添加按钮 | boolean | true、false | true
isExportBtn | 是否显示导出表格按钮 | boolean | true、false | true
isRefreshBtn | 是否显示右侧刷新按钮 | boolean | true、false | true
isSearchBtn | 是否显示右侧搜索控制按钮 | boolean | true、false | true

## option.search

search顶部的搜索表单配置是单独的。配置参考base-form文档即可。

## 方法
> 组件以提供方法的形式提供一些操作，父组件通过`this.$refs.refName.methodName`调用

方法名 | 说明 | 参数 | 返回值 | 
|:---|:-----|:-----|:------|
setSearchDisplay | 设置搜索区是否显示 | boolean | void |
setLoading | 设置表格loading态 | boolean，true打开loading，false关闭loading | void |
getSelectedList | 获取表格选中列集合 | 无 | 一个数组：selectedList
openAdd | 打开添加的dialog | 无 | 无 |
openEditRow | 打开编辑一行的dialog | 行的scope，可选传入 | 无 |
openViewRow | 打开查看一行的dialog | 行的scope，可选传入 | 无 |
getSearchFormRef | 获取搜索区域表单的ref，传入prop name即可。
getFormRef | 获取dialog form中某个字段的ref
## slot
名称 | 说明 | scope | 备注 | 
|:---|:-----|:-----|:------|
headerMenuLeft | 左上角菜单按钮插槽 | - | -
headerMenuRight | 右上角菜单按钮插槽 | - | -
footerMenu | 左下角菜单按钮插槽 | - | -
rowMenu   | 行菜单插槽 | data:scope行scope | 通过option.table.rowMenuColumn.slot = true开启，此时不会显示内置菜单按钮
rowMenuBtn | 行菜单插槽 | data:el的行scope数据 | 当行菜单为下拉菜单时，需用el-dropdown-item包裹。
prop名-col | 列单元格插槽 | 列名-col | {scope,col},scope是单元格的scope，col是列的配置对象
form-prop | 表单元素插槽 | form-prop | {scope,col},scope是单元格的scope，col是列的配置对象



## example

### 从dialog打开事件里获取到dialog的from组件的某个item的组件ref。

```
 @dialog-open="onDialogOpen"

 
  let componentRef = ref(null)
  /** dialog打开事件，其中可以拿到form的ref了。 */
  const onDialogOpen = (formRef, dialogState,row) => {
    // console.log(formRef);
    componentRef.value = formRef.getRefs('form的prop名称')
    if (dialogState === 0) {//判断当前是打开的新增、修改、查看的dialog。
      componentRef.value.empty();//调用该ref的方法。
    }else if(dialogState === 1){
       
    }
  }
```
