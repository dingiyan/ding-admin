/*
 * @copyright: Huang Ding
 * @Author: ding-cx
 * @Date: 2021-01-08 12:44:25
 * @LastEditors: ding-cx
 * @LastEditTime: 2021-03-11 18:10:18
 * @Description: 挂载全局的window对象上的站点可配置选项。无关程序的小配置。不影响安全性。
 * 
 */

 window.dingAdmin = {
     config:{
         elsize:"mini"
     }
 }

