<?php


/**
 * 压缩一个目录的方法：
 * zip_dir($path,$have_dir='default',$zip_dir=true,$zip_file=true)
 * 
 * 解压一个zip文件方法：
 * extract($zipfile,$targetpath)
 */

 
class MY_Zip extends CI_Zip {

	protected $CI;

  public function __construct()
    {
        // Assign the CodeIgniter super-object
        $this->CI =& get_instance();
		
    }


/**
 * [zip_dir 提供压缩文件夹的方法]
 * @param  [string]  $path     [必须，要压缩的路径，应该是一个文件夹路径]
 * @param  string $have_dir [空字符串或none字符串时，此时将不包含这个文件夹来读取其内的内容压缩进压缩文档，若为default字符串（默认），则使用服务器的文件夹名称，使用其他字符串则以这个字符串创建文件夹,然后递归找到$path下的所有文件和文件夹压缩进这一个文件夹中。]
 * @param  boolean $zip_dir  [是否只压缩目录下的子目录]
 * @param  boolean $zip_file [是否只压缩目录下的文件]
 * @return [object]            [返回zip对象]
 */
  public function zip_dir($path,$have_dir='default',$zip_dir=true,$zip_file=true){
    
	$path = substr($path,-1) === '/' ? $path : $path.'/';
    $defa = explode('/',$path);
    $defa = $defa[count($defa)-2];//需压缩文件夹名称字符串
    
    if(is_dir($path)){
      $this->CI->load->library('zip');

      if($have_dir == 'default'){//如果要包含文件夹自身进行压缩
      $adddir= $defa;
      
      }elseif($have_dir == 'none' || $have_dir == ''){//或者不包含文件夹自身
       $adddir= '';
      }else{//或者传入一个字符串作为文件夹名称,所有文件将压缩进这个文件夹.
	  
	   $adddir = $have_dir;
      }

        $a = scandir($path);

        foreach($a as $val){
           if($val !="." && $val !=".."){
            $strp = $path.$val;
            // 当不需要外层dir时
            if($adddir == ''){
              if(is_dir($strp)){
                if($zip_dir){
                  $this->CI->zip->read_dir($strp,false);
                }
              }else{
                if($zip_file){
                  $this->CI->zip->read_file($strp);
                }
              }
            }else{

              if(is_dir($strp)){
                if($zip_dir){
                  $this->zip_dir($path.$val.'/',$adddir.'/'.$val);
                }
              }else{
                if($zip_file){
                  $this->CI->zip->read_file($strp,$adddir.'/'.$val);
                }
              }


            }
           }
        }

        // $this->zip->read_dir($path);
        return $this->CI->zip;

    }else{
      return 0;//error:路径不是目录，请修改。
    }
  }




  /**
   * @example: $this -> zip -> extract($zipfile,$targetpath);
   * @description: 从一个zip解压到服务器上的目标路径
   * @param {string} [zipfile] [zip文件路径]
   * @param {string} [targetpath] [解压目标目录，默认是null，如果不传，则以zip的path解压，也就是说会创建zip同名文件夹，目录可以是不存在的，会自动创建]
   * @return: {boolean} --- example: 解压成功返回true，失败返回false，文件不是zip等open失败返回-1
   * 
   * 注意压缩包内不能有中文名称的文件，否则解压后有问题。
   */
  public function extract($zipfile,$targetpath=NULL){

    // 如果未传入目标目录，则以zip的路径和文件名解压
    if($targetpath === NULL){
      
      $targetpath = pathinfo($zipfile)['dirname']."/".pathinfo($zipfile)['filename'];
    
    }
   

    $zip = new zipArchive();
    if($zip->open($zipfile)){
        $zip -> extractTo($targetpath);//提取zip文件到指定目录，这个目标目录路径可以是服务器不存在的，系统会自动创建。
        return $zip -> close();
        
    }else{
        return -1;//如果zip有问题无法open，则返回-1
    }
  }








}///class
?>
