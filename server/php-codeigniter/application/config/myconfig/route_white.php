<?php

/* 路由白名单
不经过token校验

注意：
必须以/开头
必须是全路径的，例如一个http://localhost:8000/index.ph/abc/dd/c
则从index.php后续开始全路径，包含控制器类名和函数名。
*/
$config['route_white'] = array(
    "/sys/User/login",
    "/sys/Captcha"
);