<?php
/*
 * @copyright: Huang Ding
 * @Author: ding-cx
 * @Date: 2020-11-26 18:10:22
 * @LastEditors: ding-cx
 * @LastEditTime: 2020-12-01 17:53:59
 * @Description: file content
 */
 
/* 整个应用的集中化功能配置。比如登录鉴权有效期配置 */


$config['login_exp'] = 120;//登录有效期。单位为秒。

// 服务器是否安装了redis。
$config['is_install_redis'] = false;
// $config['is_install_redis'] = true;

// 控制是否允许用户在其他地方并行登录同一个账户。
$config['login_only_one_address'] = true;