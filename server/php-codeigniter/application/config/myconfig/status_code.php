<?php

defined('BASEPATH') OR exit('No direct script access allowed');


/* 自定义的配置。配置整个应用的状态码。
自定义的状态码，相当于前端收到的都是200 http status。然后根据服务器给的信息，data，含有err为true的就是服务器返回错误。 */

// 正常状态
$config['status_code'][20000] = array("code"=>20000,"msg" => 'success',"data" => null);

// 通用出错
$config['status_code'][50000] = array("is_error" => true,"code"=>50000,"msg" => 'is error',"data" => null);

// 错误状态 token校验
$config['status_code'][50008] = array("is_error" => true,"code"=>50008,"msg" => 'Illegal token',"data" => null);
$config['status_code'][50012] = array("is_error" => true,"code"=>50012,"msg" => 'other login',"data" => null);
$config['status_code'][50014] = array("is_error" => true,"code"=>50014,"msg" => 'token expired',"data" => null);

// 表单验证错误
$config['status_code'][50100] = array("is_error" => true,"code"=>50100,"msg" => 'form validation error',"data" => null);