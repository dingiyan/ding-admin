<?php
defined('BASEPATH') OR exit('No direct script access allowed');

/*
| -------------------------------------------------------------------------
| Hooks
| -------------------------------------------------------------------------
| This file lets you define "hooks" to extend CI without hacking the core
| files.  Please see the user guide for info:
|
|	https://codeigniter.com/user_guide/general/hooks.html
|
|
|   pre_system 系统早期钩子，只有基准类和钩子类被加载了
|   pre_controller 所有基础类加载完成，url安全检查完成，但还未执行控制器
|   post_controller_constructor 控制器实例化之后调用，还未进入具体的执行方法
|   post_controller 控制器执行完之后马上执行。
|   display_override 覆盖向浏览器输出的内容
|   cache_override 自定义缓存方法有用
|   post_system 系统最后调用
|
|
*/


/* 设置跨域 */
$hook['pre_system'][] = array(
    'class'    => '',
    'function' => 'cross_domain',
    'filename' => 'CrossDomain.php',
    'filepath' => 'hooks',
    'params'   => array()
);


/* 检查token，看是否有效。否则直接退出程序。 就是登录权限控制*/
$hook['post_controller_constructor'][] = array(
    'class'    => 'TokenVerify',
    'function' => 'verify',
    'filename' => 'TokenVerify.php',
    'filepath' => 'hooks',
    'params'   => array()
);
