<?php
		defined('BASEPATH') OR exit('No direct script access allowed');
		
		$config['upload_path']      = './files/uploads/';
        $config['allowed_types']    = '*';
        $config['max_size']         = 10000;
        $config['max_width']        = 10240;
        $config['max_height']       = 7680;
        $config['file_name']        = "default";
