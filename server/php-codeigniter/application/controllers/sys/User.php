<?php
/*
 * @copyright: Huang Ding
 * @Author: ding-cx
 * @Date: 2020-11-26 18:10:22
 * @LastEditors: ding-cx
 * @LastEditTime: 2020-12-01 17:53:40
 * @Description: file content
 */
/*
user api
login
*/

class User extends MY_Controller
{
    public function index()
    {
        // $data = $this->input->delete();
        // echo json_encode($data);
    }

    /* 登入 */
    public function login()
    {
        $this->load->service('sys/ServiceUser', 'user');
        // $this->insert($this->input->post());
        $ret = $this->user->login();
    }

    /* 根据user_id获取用户详细信息 */
    public function info(){
        $this->load->service('sys/ServiceUser','user');
        $this->user->getInfos(config_item('userInfo')['uid']);
    }


    /* 登出 */
    public function logout(){
        if(config_item("is_install_redis")){
            $userName = config_item('userInfo')['name'];
            // $this->send($userName);
            // return;
            $this->load->driver('cache');
            $ret = $this->cache->redis->delete($userName);
            $this->send($ret);
        }else{
            $this->send(true);
        }
    }

    private function insert($d)
    {
        $this->load->helper("uuid");
        $uuid = create_uuid();
        $data = array(
            "user_name" => $d['user_name'],
            "user_uuid" => $uuid,
            "password" => $d['password']
        );
        echo $this->db->insert('sys_user', $data);
    }


    public function test()
    {
        // echo 7809;
        // 使用中。拿到token保存的载荷用户信息。
        $userInfo = config_item('userInfo');
        $this->send($userInfo);
        return;
        $this->load->driver('cache');
        $redis = $this->cache->redis->get('user_id');
        $this->cache->redis->save('user_id', $userInfo['sub']);
        echo $redis;
    }
}
