<?php
defined('BASEPATH') OR exit('No direct script access allowed');

class Dbo extends CI_Controller {
	   public function __construct()
    {
        parent::__construct();
        $this->load->helper('crossdomain');
		$this->load->model('odb');
		$this->load->helper('cookie');
		$this->load->library('session');
		cross_domain();//跨域设置header
    }

	public function getdata(){
		// 获取从前台传来的数据
		$posts = array();
		$posts['table'] = $this->input->post("table");
		$posts['select'] = $this->input->post("select");
		$posts['where'] = $this->input->post("where");
		$posts['limit'] = $this->input->post("limit");
		$posts['order'] = $this->input->post("order");
		$posts['like'] = $this->input->post("like");



		// echo json_encode($posts);

		// 如果没有提供table，则无法获取，返回-1
		if(!$posts['table']){
			echo -1;
			return -1;
		}

		$this->load->database();

		$this->db->start_cache();//开启查询缓存
		
		// 如果设置了select则执行
		if($posts['select']){
			$this->db->select($posts['select']);
		}
		
		// 如果设置了where则执行
		
		// $userwhere = array('uid'=>1);
		// $this->db->where('year','3');
		if($posts['where']){
			$this->db->where($posts['where']);
		}
		if($posts['like']){
			$this->db->like($posts['like']);
		}
		// 如果设置了排序则执行
		if($posts['order']){
			$this->db->order_by($posts['order']['field'], $posts['order']['method']);
		}
		$this->db->stop_cache();//停止缓存，缓存之后这些条件可复用。否则执行查询一次就清空了所有条件

		// 在分页前先获取数据总行数
		$firstquery = $this->db->get($posts['table']);
		$counts = $firstquery -> num_rows();
		$retdata = array();
		$retdata['count'] = $counts;

		// 如果设置了limit则执行
		if($posts['limit']){
			$nums = isset($posts['limit'][0]) ? $posts['limit'][0] : 10;
			$shifting = isset($posts['limit'][1]) ? $posts['limit'][1] : 0;
			$this->db->limit($nums,$shifting);
		}
		
		$retdata2 = $this->db->get($posts['table']);
		$retdata['rows'] = $retdata2->result_array();
		echo json_encode($retdata);
	}



// --------------------------------------------------------------------------------------



	/**
  * @example: 前端以ajax，post方式提交即可。
  * @description: 封装基本的更新数据库控制器接口，前端常用接口，如果有更复杂的需求，则自行写控制器。此接口只基本用于修改数据库字段。
  * @param {string} [table] [前端post提交的参数，要操作的数据表名]
  * @param {string | array} [where] [前端post提交的参数，sql语句的where，可直接传入字符串，也可关联数组形式]
  * @param {array} [data] [前端post提交的参数，要修改记录的字段及值的键值对关联数组。]
  * @return: {null} example: 
  */
	public function update(){
		$posts = array();
		$posts['table'] = $this->input->post('table');
		$posts['where'] = $this->input->post('where');
		$posts['data'] = $this->input->post('data');

		if(!$posts['table'] || !$posts['where'] || !$posts['data']){
			echo -1;
			return;
		}
		echo $this->odb->update($posts['table'],$posts['where'],$posts['data']);
	}








// --------------------------------------------------------------------------------------









/**
 * @example: 
 * @description: insert one row to database
 * @param {type} [name] [description]
 * @return: {type} example: 
 */
public function add(){
	$posts = array();
		$posts['table'] = $this->input->post('table');
		$posts['data'] = $this->input->post('data');
		// 参数没有将返回-1
		if(!$posts['table'] || !$posts['data']){
			echo -1;
			return;
		}
		echo $this->odb->add($posts['table'],$posts['data']);
}



// -----------------------------------------------------------------





public function addonly(){

	    $posts = array();
		$posts['table'] = $this->input->post('table');
		$posts['data'] = $this->input->post('data');
		// 参数没有将返回-1
		if(!$posts['table'] || !$posts['data']){
			echo -1;
			return;
		}
		
		$posts['onlywhere'] = !$this->input->post('onlywhere') ? array() : $this->input->post('onlywhere');
		$posts['isand'] = $this->input->post('isand') === NULL ? false : boolval($this->input->post('isand'));
		
		// 返回值可能是插入新记录的id，也可能是有重复项的重复项记录
		
		$ret = $this->odb->add_only($posts['table'],$posts['data'],$posts['onlywhere'],$posts['isand']);
		if(is_int($ret)){
			echo $ret;
		}else{
			echo json_encode($ret);
		}
}



// ------------------------------------------------------

public function delete(){
	
	$posts = array();
	$posts['table'] = $this->input->post('table');
	$posts['where'] = $this->input->post('where');
	// 参数没有将返回-1
	if(!$posts['table'] || !$posts['where']){
		echo -1;
		return;
	}

	echo $this->odb->delete_one($posts['table'],$posts['where']);

}







public function del($apid){
	echo $this->odb->delete_one('patent_info',array('pat_id' => $apid));
}



}///class