<?php
/**
 * PHP实现jwt
 */
class JwtTest extends CI_Controller
{
    public function index()
    {
        $this->test();
    }
    
    private function test()
    {
        $this->load->library('jwt');
        
    //测试和官网是否匹配begin
        $payload=array('sub'=>'1234567890','name'=>'John Doe','iat'=>1516239022);
        $jwt=new Jwt();
        $token=$jwt->getToken($payload);
        echo "<pre>";
        echo $token;
    
        //对token进行验证签名
        $getPayload=$jwt->verifyToken($token);
        echo "<br><br>";
        var_dump($getPayload);
        echo "<br><br>";
        //测试和官网是否匹配end
    
    
        //自己使用测试begin
        $payload_test=array('iss'=>'admin','iat'=>time(),'exp'=>time()+7200,'nbf'=>time(),'sub'=>'www.admin.com','jti'=>md5(uniqid('JWT').time()));
        ;
        $token_test=Jwt::getToken($payload_test);
        echo "<pre>";
        echo $token_test;
    
        //对token进行验证签名
        $getPayload_test=Jwt::verifyToken($token_test);
        echo "<br><br>";
        var_dump($getPayload_test);
        echo "<br><br>";
        //自己使用时候end
    }
}
