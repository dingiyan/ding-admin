<?php

/* 
 get_lan_ip_list();
 get_custmer_ip();
 */


defined('BASEPATH') OR exit('No direct script access allowed');


// ------------------------------------------------------------------------

/* 
	get_lan_ip_list();
	return 本机的ip ， hostname
			局域网内所有主机的ip和主机名
 */

if ( ! function_exists('get_lan_ip_list'))
{
	
	function get_lan_ip_list()
	{
		$retarr = array();	
		$retarr['lan_list'] = array();
		$host_name = exec("hostname");
		// $host_ip = gethostbyname($host_name); 
		$bIp = gethostbyname($host_name); //获取本机的局域网IP
		$retarr['local_ip'] = $bIp;
		$retarr['local_hostname'] = gethostbyaddr($bIp);
		// echo "本机IP：",$bIp,"\n";
		// echo "本机主机名：",gethostbyaddr($bIp),"\n\n\n"; //gethostbyaddr 函数可以根据局域网IP获取主机名
		//默认网关IP
		list($ipd1,$ipd2,$ipd3) = explode('.',$bIp);
		$mask = $ipd1 . "." . $ipd2 . "." . $ipd3 ;
		exec('arp -a',$aIp); //获取局域网中的其他IP
		
		$bool = true;
		foreach( $aIp as $ipv) {
		 if(strpos($ipv,'接口') !== false) {//一下显示的IP是否是当前局域网中的 而不是其他的类型 可以在cmd下试一下命令
		 $bool = false;
		 preg_match('/(?:(?:25[0-5]|2[0-4]\d|((1\d{2})|([1-9]?\d)))\.){3}(?:25[0-5]|2[0-4]\d|((1\d{2})|([1-9]?\d)))/',$ipv,$arr);
		 if(strcmp($arr[0],$bIp) == 0) {
		  $bool = true;
		 }
		 } else {
		 if($bool) {
		  $str = preg_replace('/\s+/', '|', $ipv);
		  $sArr = explode('|',$str);
		 
		  if(count($sArr)<2){
			  continue;
		  }
		  if($sArr[1] == 'Internet' || empty($sArr[1])) {
		  continue;
		  }
		  //去除默认网关
		  if(strcmp($mask . ".1", $sArr[1]) == 0) {
		  continue;
		  }
		  //去除同网关下255的IP
		  if(strcmp($mask . ".255", $sArr[1]) == 0) {
		  continue;
		  }
		  //去除组播IP
		  list($cIp) = explode('.', $sArr[1]);
		  if($cIp >= 224 && $cIp <= 239) {
		  continue;
		  }
		  $arr2 = array();
		  $arr2['ip'] = $sArr[1];
		  $arr2['hostname'] = gethostbyaddr($sArr[1]);
		 
		  array_push($retarr['lan_list'],$arr2);
		  // echo "<br/>";
		  // echo "IP地址：",$sArr[1],"\n---";
		  // echo "MAC地址：",$sArr[2],"n---";
		  // echo "主机名：",gethostbyaddr($sArr[1]),"\n";
		  // echo "<br/>";
		 }
		 }
		}//for end
		return $retarr;
	}
}



/* 
get_custmer_ip();
 return 客户端的ip地址
 */
if(!function_exists('get_custmer_ip')){
	function get_custmer_ip(){
		if (getenv("HTTP_CLIENT_IP") && strcasecmp(getenv("HTTP_CLIENT_IP"), "unknown")) 
		        $ip = getenv("HTTP_CLIENT_IP");
		else if (getenv("HTTP_X_FORWARDED_FOR") && strcasecmp(getenv("HTTP_X_FORWARDED_FOR"), "unknown")) 
		        $ip = getenv("HTTP_X_FORWARDED_FOR");
		else if (getenv("REMOTE_ADDR") && strcasecmp(getenv("REMOTE_ADDR"), "unknown")) 
		        $ip = getenv("REMOTE_ADDR");
		else if (isset($_SERVER['REMOTE_ADDR']) && $_SERVER['REMOTE_ADDR'] && strcasecmp($_SERVER['REMOTE_ADDR'], "unknown")) 
		        $ip = $_SERVER['REMOTE_ADDR'];
		else 
		        $ip = "unknown";
		return($ip);
	}
}


function get_custmer_hostname(){
	
	$custip = get_custmer_ip();
	
	// 如果客户端就是服务器本机
	if($custip === "::1"){
		$host_name = exec("hostname");
		$bIp = gethostbyname($host_name); //获取本机的局域网Ip
		return gethostbyaddr($bIp);
	}else{
		// 客户端是局域网其他电脑
		return gethostbyaddr($custip);
	}
}
