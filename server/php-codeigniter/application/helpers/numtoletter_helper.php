<?php


defined('BASEPATH') OR exit('No direct script access allowed');


// ------------------------------------------------------------------------


/**
 * @example: num_to_letter(123);
 * @description: 将数字对应转换为字母，对应excel的列号来的
 * @param {number} [number] [传入一个整数数字]
 * @return: {string} example: 0 --》 'A'
 */
if ( ! function_exists('num_to_letter'))
{	
    function num_to_letter($index, $start = 65) {
        $str = '';
        if (floor($index / 26) > 0) {
            $str .= num_to_letter(floor($index / 26)-1);
        }
        return $str . chr($index % 26 + $start);
    }
}




