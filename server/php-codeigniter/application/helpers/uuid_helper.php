<?php


defined('BASEPATH') OR exit('No direct script access allowed');

if(! function_exists('create_uuid')){
    function create_uuid($isformat=false)
    {
        $chars = md5(uniqid(mt_rand(), true));
        $uuid = substr($chars, 0, 8) . '-'
        . substr($chars, 8, 4) . '-'
        . substr($chars, 12, 4) . '-'
        . substr($chars, 16, 4) . '-'
        . substr($chars, 20, 12);
        if($isformat){
            return $uuid;
        }
        return $chars ;
    }
    
}
