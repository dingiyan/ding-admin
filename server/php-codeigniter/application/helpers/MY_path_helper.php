<?php

/*
 * @Description: 扩展path辅助函数，实现解决set_realpath方法的缺陷，当传入的相对路径不存在时也可以返回绝对路径。
 * @Author: your name
 * @Date: 2019-09-10 09:52:56
 * @LastEditTime: 2019-09-11 09:12:56
 * @LastEditors: Please set LastEditors
 */




 


/**
 * @example: get_full_path('./files/123.tif)
 * @description: 实现根据传入的相对路径返回绝对路径，不管这个路径是否存在。
 * @param {string} [strPath] [相对路径的地址]
 * @return: {string} example: D:\wamp64\www\ipms\server\files
 */
function get_full_path($strPath){
    //    因为文件可能不存在，所以才封装这个函数进行path拼接，如果目录和文件都存在，则直接使用set_realpath。
    $setrealstr = set_realpath($strPath);
    if( $setrealstr === $strPath){
        //如果没有设置出绝对路径，手动实现。根据提供的相对路径返回绝对路径，当然这个路径是不存在的，或者文件是不存在的。
         
         $path = explode('/',$strPath);
       
         $startone = array_shift($path);//第一层级的路径，这个目录是程序入口，是必然存在的
        $houpath = implode('\\',$path);//后续的路径，手动拼接
        $dd = set_realpath($startone).$houpath;
        return $dd;

    }else{
        return $setrealstr;
    }
        
       
}


/**
 * @example: get_filename("./files/a/b.jpg",false)
 * @description: 从一个path路径中获取其中的文件名（也就是最后一层如果是文件，就返回文件名）
 * @param {string} [path] [一个文件路径]
 * @param {boolean} [is_need_extensionname] [文件名是否需要后缀，默认为true需要后缀]
 * @return: {string | boolean}} example: "abc.jpg" | "abc" | false
 */
function get_filename($path,$is_need_extensionname=true){
    if(is_file($path)){
        $filename = array_pop(explode("/",$path));
        if($is_need_extensionname){
            return $filename;
        }else{//如果不需要后缀
            return explode(".",$filename)[0];
        }
    }
    return false;
}