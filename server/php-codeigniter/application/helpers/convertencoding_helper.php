<?php


defined('BASEPATH') OR exit('No direct script access allowed');


// ------------------------------------------------------------------------

function convert_encoding($string){
	 //根据系统进行配置
	// echo mb_detect_encoding($string);
	if(mb_detect_encoding($string)=='GBK'){
		return $string;
	}
	
	$encode = stristr(PHP_OS, 'WIN') ? 'GBK' : 'UTF-8';
	$string = iconv('UTF-8', $encode, $string);
	//$string = mb_convert_encoding($string, $encode, 'UTF-8');
	return $string;
}
