<?php

class EntityUser extends Entity
{
    public function __construct()
    {
        parent::__construct();
    }
    private $table = "sys_user";
    
    /* 最好使属性名和field保持一样。 */
    public $user_name =  array(
        'field' => 'user_name',
        'label' => 'user_name',
        'rules' => 'required|min_length[5]|max_length[20]|alpha_numeric',
        'errors' => array(
            'required' => '此项必填： %s.',
            'min_length' => '至少需 {param} 个字符： {field} .',
            'max_length' => '不能超过 {param} 个字符： {field} .',
            'alpha_numeric' => '只能包含字母和数字： {field} .',
        )
        );

    public $password = array(
            'field' => 'password',
            'label' => 'password',
            'rules' => 'required|exact_length[32]',
        );

   
}
