<?php

class EntityCaptcha extends Entity
{
    public function __construct()
    {
        parent::__construct();
    }

    private $table = 'sys_captcha';

    
    
    /* 最好使属性名和field保持一样。 */
    public $cap_uuid = array(
            'field' => 'cap_uuid',
            'label' => 'cap_uuid',
            'rules' => 'required|exact_length[36]',
        );

    public $ip_address = array(
            'field' => 'ip_address',
            'label' => 'ip_address',
            'rules' => 'required|valid_ip',
        );
    public $word = array(
            'field' => 'word',
            'label' => 'word',
            'rules' => 'required|exact_length[4]|alpha_numeric',
        );

    // private $abc = 123;
}
