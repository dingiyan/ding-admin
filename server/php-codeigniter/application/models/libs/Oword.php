<?php

class Oword extends CI_Model {
	public function __construct(){
		parent::__construct();

	}

  /**
   * $values 形如：[['abc','你好'],['bds','天啊']]
   * type：普通数组，每个值也是普通数组代表一个setValue设置的键值对，并且每个值固定又包含两个值，其中前者为word模板中的标签，后者为替换标签的内容。
   
   
   * $loops 形如：[['biaoqianming',[{'va1':'内容','va2':'那地方'},{'va1':'内容','va2':'那地方'}]]]
   * type:普通数组，每个值为普通数组，代表一个setLoop设置的键值对。每个值又是固定包含两个值的普通数组。前者为标签，后者为一个普通数组，后者普通数组包含一个loop所需列表，这个列表内每个值，则是关联数组。以标签：内容的键值对方式存放。
   */

    protected $values = array();
	protected $loops = array();
	
	// 向values内自行添加
	public function values_push($key,$value){
		array_push($this->values,array($key,$$value));
	}
	
	public function loops_push($key,$value){
		array_push($this->loops,array($key,$$value));
	}
	
  public function setvalues($values){
   $this->values = $values;
    // array_push( $this->$values, $values);
  }

  public function setloops($loops){
    $this->loops = $loops;
    // array_push($this->$loops, $loops);
  }

  public function save($temp_path,$save_path){
    return $this->do_it($temp_path,$save_path);
  }
  public function download($temp_path,$save_path){
    return $this->do_it($temp_path,$save_path,true);
  }

  protected function do_it($temp_path,$save_path,$is_download=false){
    $this->load->library('PHPWord');
   
    $temp_path = $this->convert_encoding($temp_path);
    $save_path = $this->convert_encoding($save_path);
    $obj_doc = new PHPWord();
    $temp = $obj_doc -> loadTemplate($temp_path);

    if(isset($this->values) && is_array($this->values)){
       $this->setv($temp);
    }
    if(isset($this->loops) && is_array($this->loops)){
       $this->setl($temp);
    }
    
    if($is_download){
      $temp->save($save_path);
       $this->load->helper('download');
       force_download($save_path,NULL);
    }else{
      return $temp->save($save_path);
    }
  }


  protected function setv($temp){
    foreach ($this->values as $key => $value) {
      $temp->setValue($value[0],$value[1]);
    }
  }


  protected function setl($temp){
    foreach ($this->loops as $key => $value) {
      $temp->setLoop($value[0],$value[1]);
    }
  }

/**
 * 转换字符编码，本模型内各方法均内置了这个编码转换方法，但使用php自带的操作函数需要文件路径时还需要使用此方法，不然中文仍旧乱码或导致错误。
 * @param $string
 * @return string
 */
public function convert_encoding($string){
    //根据系统进行配置
    // echo mb_detect_encoding($string);
    if(mb_detect_encoding($string)=='GBK'){
      return $string;
    }
    $encode = stristr(PHP_OS, 'WIN') ? 'GBK' : 'UTF-8';
    $string = iconv('UTF-8', $encode, $string);
    //$string = mb_convert_encoding($string, $encode, 'UTF-8');
    return $string;
}







}

?>
