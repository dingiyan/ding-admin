<?php

class Oimage extends CI_Model
{
	public function __construct()
	{
        parent::__construct();
        
        // 导入路径辅助函数
        $this->load->helper('path');
    }



    /**
     * @example: $this->oimage->transform(path,pathnew,null,2000);
     * @description: 利用php imagick扩展实现图片格式转换，默认是转换为jpg格式
     * @param {string} [originPath] [原始图片文件的路径]
     * @param {string} [savePath] [输出文件的位置，必须带上文件名和后缀，后缀扩展名应该与format参数保持一致。]
     * @param {string} [format] [要转换的目标图片格式，支持imagick支持的所有格式]
     * @param {number} [width] [设置图片的宽度，高度将随比例自动设置，也就是新旧图片宽高比例一样。默认为1200像素]
     * @return: {boolean} --- example: 1 | 0
     */
    public function transform($originPath_,$savePath_,$format='jpg',$width=1200){
      
        // 设置路径为绝对路径，传入相对路径即可
        $originPath = get_full_path($originPath_);//get_full_path是扩展path的辅助函数。
        $savePath   = get_full_path($savePath_);
       
        // 如果要转换的文件不存在，则返回false
        if(!file_exists($originPath)){
            echo 'file is not exists';
            return false;
        }

        $format = ($format === NULL || $format === "") ? 'jpg' : $format;

    //    创建image对象
        $im = new Imagick($originPath);
        // 缩略图
        $im->thumbnailImage($width, 0);
        // 获取宽度和高度
        $width_ = $im->getImageWidth();
        $height = $im->getImageHeight();
       
        // 创建新image对象，并将上面的缩略图数据叠上去

        $canvas = new Imagick();

        $canvas->newImage($width_, $height, '');//创建图片对象
        $canvas->setImageFormat($format);//设置图片格式
        $canvas->compositeImage($im, imagick::COMPOSITE_OVER, 20, 10);//将缩略图叠上去
        return $canvas->writeImage($savePath);//将数据写入图片文件

    }





    
}///class