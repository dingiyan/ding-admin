<?php

class Ofile extends CI_Model {
	public function __construct(){
		parent::__construct();
	}


  
/**
 * 转换字符编码，本模型内各方法均内置了这个编码转换方法，但使用php自带的操作函数需要文件路径时还需要使用此方法，不然中文仍旧乱码或导致错误。
 * @param $string
 * @return string
 */
public function convert_encoding($string){
    //根据系统进行配置
    // echo mb_detect_encoding($string);
    if(mb_detect_encoding($string)=='GBK'){
    	return $string;
    }
    $encode = stristr(PHP_OS, 'WIN') ? 'GBK' : 'UTF-8';
    $string = iconv('UTF-8', $encode, $string);
    //$string = mb_convert_encoding($string, $encode, 'UTF-8');
    return $string;
}


/**
 * [dir_copy 复制一个目录下的所有文件及子目录，第三、四个参数分别控制是否复制文件和子目录，默认均为true，全部复制]
 * @param  string $src [目录地址]
 * @param  string $dst [目标路径]
 * @param  bool $only_file [是否复制第一层目录下文件]
 * @param  bool $only_dir [是否复制子目录]
 * @param  bool $only_file_son [是否复制所有子目录下的文件，当only_dir为false，这个参数没有作用。]
 * @return [bool]      [是否成功]
 */
public function dir_copy($src = '', $dst = '', $only_file=true, $only_dir=true, $only_file_son=true)
{
    if (empty($src) || empty($dst))
    {
        return false;
    }
 
 // 内置解决中文编码乱码问题，递归调用存在错误提示，但功能可以执行。暂时不使用
 // $src = $this->convert_encoding($src);
 // $dst = $this->convert_encoding($dst);

    $dir = opendir($src);
    $this->dir_mkdir($dst);
    while (false !== ($file = readdir($dir)))
    {
        if (($file != '.') && ($file != '..'))
        {
            if (is_dir($src . '/' . $file))
            {
            	if($only_dir){
            		$this -> dir_copy($src . '/' . $file, $dst . '/' . $file, $only_file_son,true,$only_file_son);
            	}
            }
            else
            {
            	if($only_file){
                	copy($src . '/' . $file, $dst . '/' . $file);
            	}
            }
        }
    }
    closedir($dir);
 
    return true;
}


/**
 * 创建文件夹
 *
 * @param string $path 文件夹路径
 * @param int $mode 访问权限
 * @param bool $recursive 是否递归创建
 * @return bool
 */
function dir_mkdir($path = '', $mode = 0777, $recursive = true)
{
    clearstatcache();
    // $path = $this->convert_encoding($path);
    if (!is_dir($path))
    {
        mkdir($path, $mode, $recursive);
        return chmod($path, $mode);
    }
 
    return true;
}


/**
 * [dir_del 删除文件夹下文件和文件夹，第二、三参数默认为true，将删除目录及目录之下所有内容]
 * @param  [string] $path [文件夹路径]
 * @param  [bool] $only_file [控制是否删除文件]
 * @param  [bool] $only_dir [控制是否删除文件夹]
 * @return [bool]       [布尔值]
 */
  function dir_del($path,$only_file=true,$only_dir=true){
        // $path = substr($path,-1) === '/' ? $path : $path.'/';

        // 编码设置，递归调用有错误提示，暂不适用
        // $path = $this->convert_encoding($path);
    //如果是目录则继续
    if(is_dir($path)){
        //扫描一个文件夹内的所有文件夹和文件并返回数组
    $p = scandir($path);
    foreach($p as $val){
        //排除目录中的.和..
        if($val !="." && $val !=".."){
        //如果是目录则递归子目录，继续操作
        if(is_dir($path.$val)){
            if($only_dir){
                //子目录中操作删除文件夹和文件
                $this->dir_del($path.$val.'/');
                //目录清空后删除空文件夹
                @rmdir($path.$val.'/');
            }
        }else{
        //如果是文件直接删除
        if($only_file){
            unlink($path.$val);
        }
        }
        }
    }
    //    连带路径中的最后的文件夹也删出
    //    if($only_dir && $only_file){
    //   	 return rmdir($path);
    //    }
    return true;
    
    }else{
        return 'Error:your argment is not a path';
    }

  }


  /**
   * @example: every_file("./files/cache/123/")
   * @description: 遍历一个文件夹路径path下的每个文件，接收回调函数callback对文件进行处理，遇到文件夹时可通过参数isdir控制是否深入遍历
   * @param {string} [path] [文件夹路径]
   * @param {function} [callback] [回调函数句柄，参数为被遍历的文件路径]
   * @param {boolean} [isdir] [布尔控制，是否深度递归遍历path下的目录，默认为false，不深度递归。]
   * @return: {0 | null} --- example: 0表示path不是文件夹路径，正常情况是null，什么都不返回
   */
  public function every_file($path,$callback,$isdir=false){
    if(is_dir($path)){

        $p = scandir($path);
       
        foreach($p as $val){
            //排除目录中的.和..
            if($val !="." && $val !=".."){
                //如果是目录且参数isdir为真则递归子目录，继续操作
                if(is_dir($path.$val) && $isdir){
                    $this -> every_file($path.$val.'/',$callback,true);
                }else{
                    //如果是文件，执行回调函数，回调接收一个参数：文件路径
                    call_user_func($callback,$path.$val);                    
                }
            }
        }


    }else{
        return 0;//path不是目录，返回0
    }
  }







}

?>
