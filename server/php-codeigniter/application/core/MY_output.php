<?php
/*
 * @copyright: Huang Ding
 * @Author: ding-cx
 * @Date: 2020-11-21 10:01:02
 * @LastEditors: ding-cx
 * @LastEditTime: 2020-12-04 09:55:49
 * @Description: file content
 */

/* 扩展ci的核心系统类 output */
class MY_output extends CI_output
{
    public function __construct()
    {
        parent::__construct();
    }

    /*  
    *扩展输出类，定义send方法，可结合config的status状态码输出。统一
    输出标准。在控制器中代理了send方法，更方便规范输出
    * @params $data 要输出的数据 ，array，string
    * @params $status 在自定义的config中的状态码。如果没有对应，将报错。
     */
    public function send($data=array(), $status=20000)
    {
        // 全局公共函数，拿到配置。
        $statusList = config_item('status_code');
        $ret = array();
        if (array_key_exists($status,$statusList)) {
            $ret = $statusList[$status];
            $ret['data'] = $data;
        }else{
            throw "没有定义".$status."状态码";
            exit();
        }
        if (is_array($ret)) {
            $str = json_encode($ret);
            // log_message("error",$str);
            echo $str;
        } elseif (is_string($ret)) {
            echo $ret;
        } else {
            echo "your echo data is not array or string.";
        }
    }
}
