<?php

/* 扩展ci的核心系统类 input */
class MY_Input extends CI_Input {
    public function __construct(){
        parent::__construct();
    }

    
    public function put(){
        return $this->common($this->method() === 'put');
    }
    public function delete(){
        return $this->common($this->method() === 'delete');
    }
    public function patch(){
        return $this->common($this->method() === 'patch');
    }
    
    
    private function common($method){
        if($method){
            $data = json_decode($this->raw_input_stream);
            if($data){
                return $data;
            }
            /* 如果是这些方法， 但未携带数据，则返回空数组。携带了数据则返回数据。 */
            return  array();
        }
        return null;
    }
}