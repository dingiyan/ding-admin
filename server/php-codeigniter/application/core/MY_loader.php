<?php

/* 扩展ci的核心系统类 控制器类，携带一些简便的方法提供调用 */
class MY_loader extends CI_loader
{
    public function __construct()
    {
        parent::__construct();
    }
    
    /* 加载实体类的扩展loader，可更方便快捷加载 */
    public function entity($model, $name='')
    {
        require_once("system/core/Model.php");
        require_once("application/core/ModelEntity.php");
        return $this->model('entity/'.$model, $name);
    }
    /* 加载服务类的扩展loader，可更方便快捷加载 */
    public function service($model, $name='')
    {
        require_once("system/core/Model.php");
        require_once("application/core/ModelService.php");
        return $this->model('service/'.$model, $name);
    }
}
