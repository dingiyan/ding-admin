<?php

/* 扩展一个service类，专门用于逻辑组织管理的service*/
class Service extends CI_Model
{
    public function __construct()
    {
        parent::__construct();
    }
    // 
    
    /* service辅助函数，用；来合并多个验证结果 */
    public function merge_valid($results = array()){
        $ret = array('result' => true,'error' => array());
        if(is_array($results)){
            foreach($results as $item){
                if(!$item['result']){
                    $ret['result'] = false;
                     foreach($item['error'] as $key => $val){
                        $ret['error'][$key] = $val;
                     }
                }
            }
        }
        return $ret;
    }
  
     // by ding =========----------------------------------------------
     
    /**
     * 将output的send方法放置在控制器上，更方便输出数据
     *
     * @return	object
     */
    public function send($data=array(), $status=20000)
    { 
        $ci =& get_instance();
        return $ci->output->send($data, $status);
    }
   
}
