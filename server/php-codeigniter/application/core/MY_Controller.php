<?php

/* 扩展ci的核心系统类 控制器类，携带一些简便的方法提供调用 */
class MY_Controller extends CI_Controller
{
    public function __construct()
    {
        parent::__construct();
    }
    
    
    // by ding =========----------------------------------------------
     
    /**
     * 将output的send方法放置在控制器上，更方便输出数据
     *
     * @return	object
     */
    public function send($data=array(), $status=20000)
    {
        return $this->output->send($data, $status);
    }
}
