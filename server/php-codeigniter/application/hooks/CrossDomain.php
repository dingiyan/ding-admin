<?php
if(!function_exists("cross_domain")){

	function cross_domain(){
		header('Access-Control-Allow-Origin:*');
		return;
		$origin = isset($_SERVER['HTTP_ORIGIN'])? $_SERVER['HTTP_ORIGIN'] : '';  
		
		$allow_origin = array(  
			'http://localhost:8088',
			'http://dingiyan:8088'
		);  
		
		if(in_array($origin, $allow_origin)){  
			header('Access-Control-Allow-Origin:'.$origin);       
		} 
	}
	
}