-- 创建数据库
-- CREATE DATABASE `ding_admin` DEFAULT CHARSET utf8 COLLATE utf8_general_ci;
-- 删除一个数据库
-- DROP DATABASE `ding_admin`
-- 系统 用户
CREATE TABLE `ding_admin`.`sys_user` (
    `user_id` INT(10) NOT NULL AUTO_INCREMENT COMMENT 'id',
    `user_uuid` VARCHAR(36) NOT NULL COMMENT 'uuid',
    `user_name` VARCHAR(50) CHARACTER SET utf8 COLLATE utf8_general_ci NOT NULL COMMENT '用户名，登录名',
    `real_name` VARCHAR(30) CHARACTER SET utf8 COLLATE utf8_general_ci NOT NULL COMMENT '真实姓名',
    `phone_num` VARCHAR(30) CHARACTER SET utf8 COLLATE utf8_general_ci NOT NULL COMMENT '手机号码',
    `email` VARCHAR(50) CHARACTER SET utf8 COLLATE utf8_general_ci NOT NULL COMMENT '邮箱',
    `password` VARCHAR(64) CHARACTER SET utf8 COLLATE utf8_general_ci NOT NULL COMMENT '密码',
    `role_ids` VARCHAR(50) CHARACTER SET utf8 COLLATE utf8_general_ci NOT NULL COMMENT '角色ID列表',
    `is_use` TINYINT(1) NOT NULL DEFAULT 1 COMMENT '是否正常使用',
    `create_user_id` INT(8) NOT NULL COMMENT '创建者id',
    `edit_user_id` INT(8) NOT NULL COMMENT '修改者id',
    `create_time` DATETIME NOT NULL DEFAULT CURRENT_TIMESTAMP COMMENT '创建时间',
    `edit_time` DATETIME NOT NULL DEFAULT CURRENT_TIMESTAMP ON UPDATE CURRENT_TIMESTAMP COMMENT '修改时间',
    PRIMARY KEY (`user_id`, `user_uuid`)
) ENGINE = MyISAM;

-- 用户数据。插入。密码是111111
INSERT INTO
    `sys_user` (
        `user_id`,
        `user_uuid`,
        `user_name`,
        `real_name`,
        `phone_num`,
        `email`,
        `password`,
        `role_ids`,
        `is_use`,
        `create_user_id`,
        `edit_user_id`
    )
VALUES
    (
        NULL,
        '6713fabb96b3b6e5f0b35b49573d61f6',
        'admin',
        '',
        '',
        '',
        '96e79218965eb72c92a549dd5a330112',
        '',
        '1',
        '0',
        '0'
    );

-- 系统 角色
CREATE TABLE `ding_admin`.`sys_role`(
    `role_id` INT(6) NOT NULL AUTO_INCREMENT COMMENT 'id',
    `role_name` VARCHAR(20) CHARACTER SET utf8 COLLATE utf8_general_ci NOT NULL COMMENT '角色名称',
    `desc` VARCHAR(100) CHARACTER SET utf8 COLLATE utf8_general_ci NOT NULL COMMENT '备注',
    `create_user_id` INT(8) NOT NULL COMMENT '创建者id',
    `edit_user_id` INT(8) NOT NULL COMMENT '修改者id',
    `create_time` DATETIME NOT NULL DEFAULT CURRENT_TIMESTAMP COMMENT '创建时间',
    `edit_time` DATETIME NOT NULL DEFAULT CURRENT_TIMESTAMP ON UPDATE CURRENT_TIMESTAMP COMMENT '修改时间',
    PRIMARY KEY (`role_id`)
) ENGINE = MyISAM;

-- 系统 角色对应的权限保存表，一个角色的一个权限保存一条
CREATE TABLE `ding_admin`.`sys_role_perm`(
    `auto_id` INT(6) NOT NULL AUTO_INCREMENT COMMENT '自增id',
    `role_id` INT(6) NOT NULL COMMENT '角色id',
    `perm_id` INT(6) NOT NULL COMMENT '权限id',
    `create_user_id` INT(8) NOT NULL COMMENT '创建者id',
    `edit_user_id` INT(8) NOT NULL COMMENT '修改者id',
    `create_time` DATETIME NOT NULL DEFAULT CURRENT_TIMESTAMP COMMENT '创建时间',
    `edit_time` DATETIME NOT NULL DEFAULT CURRENT_TIMESTAMP ON UPDATE CURRENT_TIMESTAMP COMMENT '修改时间',
    PRIMARY KEY (`auto_id`, `role_id`)
) ENGINE = MyISAM;

-- 系统 操作日志记录
-- 日志类型
--      登录、新增数据、修改数据、删除数据 、多个操作
CREATE TABLE `ding_admin`.`sys_log` (
    `log_id` BIGINT(14) NOT NULL AUTO_INCREMENT COMMENT 'id',
    `type` INT(2) NOT NULL COMMENT '日志类别',
    `user_id` INT(10) NOT NULL COMMENT '操作用户',
    `req_url` VARCHAR(300) CHARACTER SET utf8 COLLATE utf8_general_ci NOT NULL COMMENT '请求url',
    `method` INT(1) NOT NULL COMMENT '请求方法',
    `params` VARCHAR(300) CHARACTER SET utf8 COLLATE utf8_general_ci NOT NULL COMMENT '携带参数',
    `req_ip` VARCHAR(15) CHARACTER SET utf8 COLLATE utf8_general_ci NOT NULL COMMENT '请求ip地址',
    `res_status` VARCHAR(4) CHARACTER SET utf8 COLLATE utf8_general_ci NOT NULL COMMENT '响应状态码',
    PRIMARY KEY (`log_id`)
) ENGINE = MyISAM;

-- 系统 组织结构表
CREATE TABLE `ding_admin`.`sys_org`(
    `org_id` INT(8) NOT NULL AUTO_INCREMENT COMMENT '组织结构id',
    `org_uuid` VARCHAR(36) CHARACTER SET utf8 COLLATE utf8_general_ci NOT NULL COMMENT '组织结构uuid',
    `org_name` VARCHAR(80) CHARACTER SET utf8 COLLATE utf8_general_ci NOT NULL COMMENT '组织名称',
    `org_code` VARCHAR(16) CHARACTER SET utf8 COLLATE utf8_general_ci NOT NULL COMMENT '组织机构代码，自写',
    `parent_org_id` INT(8) NOT NULL COMMENT '上级部门id',
    `state` TINYINT(1) NOT NULL COMMENT '状态1启用0禁用',
    `orders` INT(4) NOT NULL COMMENT '排序',
    `create_user_id` INT(8) NOT NULL COMMENT '创建者id',
    `edit_user_id` INT(8) NOT NULL COMMENT '修改者id',
    `create_time` DATETIME NOT NULL DEFAULT CURRENT_TIMESTAMP COMMENT '创建时间',
    `edit_time` DATETIME NOT NULL DEFAULT CURRENT_TIMESTAMP ON UPDATE CURRENT_TIMESTAMP COMMENT '修改时间',
    PRIMARY KEY (`org_id`)
) ENGINE = MyISAM;

-- 验证码保存表
CREATE TABLE `ding_admin`.`sys_captcha` (
    captcha_id bigint(13) unsigned NOT NULL auto_increment COMMENT 'id',
    cap_uuid varchar(36) not NULL COMMENT '前端传来的uuid',
    captcha_time DATETIME NOT NULL DEFAULT CURRENT_TIMESTAMP COMMENT '生成时间',
    ip_address varchar(45) NOT NULL COMMENT 'client ip',
    word varchar(20) NOT NULL COMMENT '生成的字符',
    PRIMARY KEY (`captcha_id`),
    KEY `word` (`word`)
) ENGINE = MyISAM;

-- 菜单表
CREATE TABLE `ding_admin`.`sys_menus`(
    `menu_id` int(4) NOT NULL AUTO_INCREMENT COMMENT '菜单id',
    `parent_menu_id` int(4) NOT NULL COMMENT '父菜单id',
    `menu_order` int(4) NOT NULL COMMENT '菜单排序',
    `path` varchar(60) NOT NULL COMMENT '菜单的route name同时也是path',
    `title` varchar(30) NOT NULL COMMENT '菜单的侧栏显示名称',
    `component` varchar(200) NOT NULL COMMENT '组件路径，基于views的',
    `redirect` varchar(200) NOT NULL COMMENT 'path的重定向路径',
    `is_hidden` TINYINT(1) NOT NULL DEFAULT 0 COMMENT '是否不在侧栏显示',
    `alwaysShow` TINYINT(1) NOT NULL DEFAULT 1 COMMENT '始终显示在侧栏',
    `icon` varchar(50) NOT NULL COMMENT '菜单icon名称',
    `noCache` TINYINT(1) NOT NULL DEFAULT 0 COMMENT '不缓存',
    `affix` TINYINT(1) NOT NULL DEFAULT 1 COMMENT '固定在tagview',
    `breadcrumb` TINYINT(1) NOT NULL DEFAULT 1 COMMENT '是否展示在面包屑',
    `activeMenu` varchar(200) NOT NULL COMMENT '当前页面激活左侧菜单的路径',
    `create_user_id` INT(8) NOT NULL COMMENT '创建者id',
    `edit_user_id` INT(8) NOT NULL COMMENT '修改者id',
    `create_time` DATETIME NOT NULL DEFAULT CURRENT_TIMESTAMP COMMENT '创建时间',
    `edit_time` DATETIME NOT NULL DEFAULT CURRENT_TIMESTAMP ON UPDATE CURRENT_TIMESTAMP COMMENT '修改时间',
    PRIMARY KEY (`menu_id`)
) ENGINE = MyISAM;

-- 菜单，按钮的权限赋予表，结合菜单表，这里有菜单表里某个菜单的权限就表示该菜单需权限。没有就通用可看。
-- 系统 权限列表
CREATE TABLE `ding_admin`.`sys_perm`(
    `perm_id` INT(8) NOT NULL AUTO_INCREMENT COMMENT '权限id',
    `menu_id` INT(4) NOT NULL COMMENT '所属菜单',
    `type` INT(3) NOT NULL COMMENT '权限类型菜单、按钮',
    `cname` VARCHAR(20) CHARACTER SET utf8 COLLATE utf8_general_ci NOT NULL COMMENT '权限名称',
    `ename` VARCHAR(50) CHARACTER SET utf8 COLLATE utf8_general_ci NOT NULL COMMENT '授权标识',
    `create_user_id` INT(8) NOT NULL COMMENT '创建者id',
    `edit_user_id` INT(8) NOT NULL COMMENT '修改者id',
    `create_time` DATETIME NOT NULL DEFAULT CURRENT_TIMESTAMP COMMENT '创建时间',
    `edit_time` DATETIME NOT NULL DEFAULT CURRENT_TIMESTAMP ON UPDATE CURRENT_TIMESTAMP COMMENT '修改时间',
    PRIMARY KEY (`perm_id`),
    UNIQUE (`ename`)
) ENGINE = MyISAM;