import egg from "egg"
// import WsOnMessage from "@diyaner/egg-ws"

/** 此处写业务逻辑接口文档。ws的on事件，定义已知的event，对应的回调的参数类型。业务代码中使用此
 * 事件就能提示。这是使用了ts的类型合并功能，egg-ws内部的WsWorker类合并。
 */
declare module 'egg'{
    interface WsWorker {
        on(event:'abc',callback:(ws:WsOnMessage,data:{a:22}) => void) : void;
    }
}
export = egg