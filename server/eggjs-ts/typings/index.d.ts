/*
 * @copyright: Huang Ding
 * @Author: ding-cx
 * @Date: 2021-02-23 10:37:56
 * @LastEditors: ding-cx
 * @LastEditTime: 2021-04-12 15:14:32
 * @Description: file content
 */
import egg from "egg";
// 引入框架的声明
import "dinegg";

import "egg-socket.io";

import "@cxkj/egg-nodemailer"

import "@diyaner/egg-ws"
import "./wsInterface"

declare module "egg" {
  /** api返回值的接口 */
  interface apiReturn {
    /** 返回统一编码 */
    code: 20000 | 50000 | 50008;
    /** 返回说明信息 */
    msg: string;
    /** 返回数据主体 */
    data: any;
  }

  interface EggAppConfig {
    /** 模型表的前缀 */
    dbModel_prefix: string;
    /** 路由前缀，挂载在config上 */
    route_prefix: {
      api: "/api";
      api2: "/api/v2";
    };
  }

  interface Context {
    //  dinegg内不能定义
    /** jwtLogin中间件解析出来的token 用户信息存放 */
    loginUserInfo: {
      /** 用户id */
      uid: number | string;
      /** 用户名 */
      name: string;
    };
  }

 
}
export = egg;
