// This file is created by egg-ts-helper@1.25.9
// Do not modify this file!!!!!!!!!

import 'egg';
type AnyClass = new (...args: any[]) => any;
type AnyFunc<T = any> = (...args: any[]) => T;
type CanExportFunc = AnyFunc<Promise<any>> | AnyFunc<IterableIterator<any>>;
type AutoInstanceType<T, U = T extends CanExportFunc ? T : T extends AnyFunc ? ReturnType<T> : T> = U extends AnyClass ? InstanceType<U> : U;
import ExportInstaller from '../../../app/service/installer';
import ExportQuerySql from '../../../app/service/query-sql';
import ExportRedis from '../../../app/service/redis';
import ExportSysConfig from '../../../app/service/sys/config';
import ExportSysDropdown from '../../../app/service/sys/dropdown';
import ExportSysMenus from '../../../app/service/sys/menus';
import ExportSysOrg from '../../../app/service/sys/org';
import ExportSysPermission from '../../../app/service/sys/permission';
import ExportSysUser from '../../../app/service/sys/user';

declare module 'egg' {
  interface IService {
    installer: AutoInstanceType<typeof ExportInstaller>;
    querySql: AutoInstanceType<typeof ExportQuerySql>;
    redis: AutoInstanceType<typeof ExportRedis>;
    sys: {
      config: AutoInstanceType<typeof ExportSysConfig>;
      dropdown: AutoInstanceType<typeof ExportSysDropdown>;
      menus: AutoInstanceType<typeof ExportSysMenus>;
      org: AutoInstanceType<typeof ExportSysOrg>;
      permission: AutoInstanceType<typeof ExportSysPermission>;
      user: AutoInstanceType<typeof ExportSysUser>;
    }
  }
}
