// This file is created by egg-ts-helper@1.25.9
// Do not modify this file!!!!!!!!!

import 'egg';
import ExportAddUpdateDelete from '../../../app/middleware/addUpdateDelete';
import ExportLoginLog from '../../../app/middleware/loginLog';
import ExportTestCtx from '../../../app/middleware/testCtx';
import ExportSysConfigObject from '../../../app/middleware/sys/configObject';

declare module 'egg' {
  interface IMiddleware {
    addUpdateDelete: typeof ExportAddUpdateDelete;
    loginLog: typeof ExportLoginLog;
    testCtx: typeof ExportTestCtx;
    sys: {
      configObject: typeof ExportSysConfigObject;
    }
  }
}
