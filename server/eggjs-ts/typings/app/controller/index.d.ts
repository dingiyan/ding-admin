// This file is created by egg-ts-helper@1.25.9
// Do not modify this file!!!!!!!!!

import 'egg';
import ExportTest from '../../../app/controller/test';
import ExportSysCaptcha from '../../../app/controller/sys/captcha';
import ExportSysConfig from '../../../app/controller/sys/config';
import ExportSysDropdownMenu from '../../../app/controller/sys/dropdownMenu';
import ExportSysEmail from '../../../app/controller/sys/email';
import ExportSysInstall from '../../../app/controller/sys/install';
import ExportSysLog from '../../../app/controller/sys/log';
import ExportSysMenus from '../../../app/controller/sys/menus';
import ExportSysOrg from '../../../app/controller/sys/org';
import ExportSysOss from '../../../app/controller/sys/oss';
import ExportSysPermission from '../../../app/controller/sys/permission';
import ExportSysQuerySql from '../../../app/controller/sys/querySql';
import ExportSysRole from '../../../app/controller/sys/role';
import ExportSysUser from '../../../app/controller/sys/user';

declare module 'egg' {
  interface IController {
    test: ExportTest;
    sys: {
      captcha: ExportSysCaptcha;
      config: ExportSysConfig;
      dropdownMenu: ExportSysDropdownMenu;
      email: ExportSysEmail;
      install: ExportSysInstall;
      log: ExportSysLog;
      menus: ExportSysMenus;
      org: ExportSysOrg;
      oss: ExportSysOss;
      permission: ExportSysPermission;
      querySql: ExportSysQuerySql;
      role: ExportSysRole;
      user: ExportSysUser;
    }
  }
}
