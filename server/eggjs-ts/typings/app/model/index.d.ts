// This file is created by egg-ts-helper@1.25.9
// Do not modify this file!!!!!!!!!

import 'egg';
import ExportTemplate from '../../../app/model/template';
import ExportSysCaptcha from '../../../app/model/sys/captcha';
import ExportSysConfig from '../../../app/model/sys/config';
import ExportSysDropdownMenu from '../../../app/model/sys/dropdown_menu';
import ExportSysDropdownMenuItem from '../../../app/model/sys/dropdown_menu_item';
import ExportSysEmail from '../../../app/model/sys/email';
import ExportSysLog from '../../../app/model/sys/log';
import ExportSysMenus from '../../../app/model/sys/menus';
import ExportSysOrg from '../../../app/model/sys/org';
import ExportSysOss from '../../../app/model/sys/oss';
import ExportSysPermissionList from '../../../app/model/sys/permission_list';
import ExportSysRole from '../../../app/model/sys/role';
import ExportSysRolePermission from '../../../app/model/sys/role_permission';
import ExportSysUser from '../../../app/model/sys/user';

declare module 'egg' {
  interface IModel {
    Template: ReturnType<typeof ExportTemplate>;
    Sys: {
      Captcha: ReturnType<typeof ExportSysCaptcha>;
      Config: ReturnType<typeof ExportSysConfig>;
      DropdownMenu: ReturnType<typeof ExportSysDropdownMenu>;
      DropdownMenuItem: ReturnType<typeof ExportSysDropdownMenuItem>;
      Email: ReturnType<typeof ExportSysEmail>;
      Log: ReturnType<typeof ExportSysLog>;
      Menus: ReturnType<typeof ExportSysMenus>;
      Org: ReturnType<typeof ExportSysOrg>;
      Oss: ReturnType<typeof ExportSysOss>;
      PermissionList: ReturnType<typeof ExportSysPermissionList>;
      Role: ReturnType<typeof ExportSysRole>;
      RolePermission: ReturnType<typeof ExportSysRolePermission>;
      User: ReturnType<typeof ExportSysUser>;
    }
  }
}
