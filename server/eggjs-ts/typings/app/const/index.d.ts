// This file is created by egg-ts-helper@1.25.9
// Do not modify this file!!!!!!!!!

import 'egg';
import ExportRedis from '../../../app/const/redis';

declare module 'egg' {
  interface Application {
    const: T_const;
  }

  interface T_const {
    redis: typeof ExportRedis;
  }
}
