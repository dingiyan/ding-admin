// This file is created by egg-ts-helper@1.25.9
// Do not modify this file!!!!!!!!!

import 'egg';
import ExportDefault from '../../../../app/io/controller/default';
import ExportNsp from '../../../../app/io/controller/nsp';

declare module 'egg' {
  interface CustomController {
    default: ExportDefault;
    nsp: ExportNsp;
  }
}
