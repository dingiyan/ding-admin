import * as assert from 'assert';
// import { apiReturn } from 'egg';
import { app } from 'egg-mock/bootstrap';

describe("test/app/controller/captcha.test.ts", () => {
    before(async () => {
        await app.model.Sys.Captcha.sync({ force: true });
    })

    // 测试生成验证码。
    it("生成png验证码：",async () => {
        const uuid = "hglsdakgasldf";//测试随意写的，前端生成时应该使用规范的uuid 36位字符串
        const png = await app.httpRequest().get("/sys/Captcha/generate/" + uuid);
        // console.log(png.body);
        assert(png.body instanceof Buffer)
    })
})