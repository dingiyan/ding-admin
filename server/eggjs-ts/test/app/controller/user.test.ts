import * as assert from 'assert';
// import { Context } from 'egg';
// import { apiReturn } from 'egg';
import { app } from 'egg-mock/bootstrap';
import { sys_user } from '../../../app/model/sys/user';

// 测试 sys_user 系统用户表通用api。
describe('test/app/controller/user.test.ts', () => {
  before(async () => {
    await app.model.Sys.User.sync({ force: true });
  })

  it('create -> update -> delete', async () => {
    app.mockCsrf();
    const result = await app.httpRequest().post(app.config.routerPrefix + '/sys_user').send({
      user_name: "testname",
      password: 'helo passworld'
    } as sys_user);
    // console.log(result.body);
    // const result = await app.httpRequest().get('/').expect(200);
    assert(result.body.code === 20000);
    assert(result.body.data.id > 0);
    
    const res1 = await app.httpRequest().put(app.config.routerPrefix + '/sys_user/' + result.body.data.id).send({
      user_name: 'updatename'
    })
    assert(res1.body.data.affectedRows === 1)
    const res2 = await app.httpRequest().del(app.config.routerPrefix + '/sys_user/' + result.body.data.id)
    // console.log(res2.body);
    assert(res2.body.data.affectedRows === 1)
  });

  // 登录api
  it('user login', async () => {
    // const ctx:Context = app.mockContext();
    //获取图片验证码。
    const uuid: string = (Math.random() * 10000).toString(36);
    const png = await app.httpRequest().get("/sys/Captcha/generate/" + uuid);
    // console.log(png.body);
    assert(png.body instanceof Buffer)
    const word = await app.model.Sys.Captcha.findOne({
      where: {
        cap_uuid: uuid
      }
    })
    //添加模拟用户
    app.mockCsrf();
    const result = await app.httpRequest().post(app.config.routerPrefix + '/sys_user').send({
      user_name: "admin",
      password: '123'
    } as sys_user);
    // console.log(result.body.data.id);
    assert(result.body.code === 20000);

    //登录请求 success
    const res = await app.httpRequest().post("/sys/user/userLogin").send({
      cap_uuid: uuid,
      word: word?.word,
      user_name: "admin",
      password: "123"
    })
    // console.log(res.body.data.token);
    assert(typeof res.body.data.token === 'string')
    assert(res.body.code === 20000)


    //登录请求 验证码错误
    const res2 = await app.httpRequest().post("/sys/user/userLogin").send({
      cap_uuid: uuid,
      word: 'hels',
      user_name: "admin",
      password: "123"
    })

    // console.log(res);
    assert(res2.body.data === '验证码错误！')
    assert(res2.body.code === 50000)

    //登录请求 用户名或密码错误
    const res3 = await app.httpRequest().post("/sys/user/userLogin").send({
      cap_uuid: uuid,
      word: word?.word,
      user_name: "admin2",
      password: "123"
    })

    // console.log(res);
    assert(res3.body.data === '用户名或密码错误！')
    assert(res3.body.code === 50000)


  })

});
