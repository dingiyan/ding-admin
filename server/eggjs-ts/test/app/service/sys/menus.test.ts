/*
 * @copyright: Huang Ding
 * @Author: ding-cx
 * @Date: 2021-03-01 09:55:54
 * @LastEditors: ding-cx
 * @LastEditTime: 2021-03-01 09:56:27
 * @Description: file content
 */

import * as assert from "assert";
import { Context } from "egg";
import { app } from "egg-mock/bootstrap";

describe("test/app/service/sys/menus.test.js", () => {
  let ctx: Context;

  before(async () => {
    ctx = app.mockContext();
  });

  it("sayHi", async () => {
    // const result = await ctx.service.test.sayHi('egg');
    console.log(ctx.service.restful.create);
    const result = "hi, egg";
    assert(result === "hi, egg");
  });
});
