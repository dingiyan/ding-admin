/*
 * @copyright: Huang Ding
 * @Author: ding-cx
 * @Date: 2021-03-01 09:55:54
 * @LastEditors: ding-cx
 * @LastEditTime: 2021-03-20 17:17:36
 * @Description: file content
 */

import * as assert from "assert";
import { Context } from "egg";
import { app } from "egg-mock/bootstrap";

describe("test/app/service/sys/dropdown.test.ts", () => {
  let ctx: Context;

  before(async () => {
    ctx = app.mockContext();
  });

  it("getDicItemByMenuEname", async () => {
    let data = await ctx.service.sys.dropdown.getDicItemByMenuEname('sex'); //editor user id
    assert(Array.isArray(data));
  });
});
