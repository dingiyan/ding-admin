/*
 * @copyright: Huang Ding
 * @Author: ding-cx
 * @Date: 2021-03-01 09:55:54
 * @LastEditors: ding-cx
 * @LastEditTime: 2021-03-24 10:54:03
 * @Description: file content
 */

import * as assert from "assert";
import { Context } from "egg";
import { app } from "egg-mock/bootstrap";

describe("test/app/service/sys/config.test.ts", () => {
  let ctx: Context;

  before(async () => {
    ctx = app.mockContext();
  });

  it("getConfigObject", async () => {
    let data = await ctx.service.sys.config.getConfigObject(); //editor user id
    // console.log(data);
    assert(Object.keys(data).length >= 0);
  });
});
