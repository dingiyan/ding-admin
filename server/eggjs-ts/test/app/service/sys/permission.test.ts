/*
 * @copyright: Huang Ding
 * @Author: ding-cx
 * @Date: 2021-03-01 09:55:54
 * @LastEditors: ding-cx
 * @LastEditTime: 2021-03-19 17:04:31
 * @Description: file content
 */

import * as assert from "assert";
import { Context } from "egg";
import { app } from "egg-mock/bootstrap";

describe("test/app/service/sys/permission.test.ts", () => {
  let ctx: Context;

  before(async () => {
    ctx = app.mockContext();
  });

  it("getAllPermByUser", async () => {
    let data = await ctx.service.sys.permission.getAllPermByUser(1); //editor user id
    console.log(data);
    assert(Array.isArray(data.custom_perm));
    // data = await ctx.service.sys.permission.getAllPermByUser(1); //editor user id
    // assert(Array.isArray(data));
  });
});
