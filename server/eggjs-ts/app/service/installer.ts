/*
 * @copyright: Huang Ding
 * @Author: ding-cx
 * @Date: 2021-04-23 16:00:30
 * @LastEditors: ding-cx
 * @LastEditTime: 2021-04-23 16:12:56
 * @Description: file content
 */
import { Service } from "egg";

export default class SysInstallerService extends Service {
  /** 插入数据 */
  async insertRows() {
    await this.sysConfig();
    await this.sysMenus();
    await this.sysPermission();
    await this.sysUser();
  }

  private async sysConfig() {
    const model = this.app.model.Sys.Config;
    await model.findOrCreate({
      defaults: {
        id: 1,
        key: "oss",
        name: "oss",
        value: JSON.stringify({
          local: {
            url: "http://localhost:7001/oss/local/file/",
            dir: "D:/uploads",
          },
        }),
      },
      where: {
        key: "oss",
      },
    });

    await model.findOrCreate({
      defaults: {
        id: 2,
        key: "email",
        name: "系统邮件配置",
        value: JSON.stringify({
          host: "smtp.163.com",
          port: 25,
          secure: false,
          auth: {
            user: "ding25199@163.com",
            pass: "ding940908",
          },
        }),
        memo: "系统邮件配置",
      },
      where: {
        key: "email",
      },
    });
  }

  private async sysMenus() {
    const model = this.app.model.Sys.Menus;

    // 固定默认插入数据库的基本菜单数据
    const baseMenus = [
      {
        id: 1,
        parent_menu_id: 0,
        title: "系统管理",
        path: "sys",
        component: "/",
        icon: "dashboard",
      },
      {
        id: 2,
        parent_menu_id: 1,
        title: "菜单管理",
        path: "menus",
        component: "sys/menus",
        icon: "nested",
      },
      {
        id: 3,
        parent_menu_id: 1,
        title: "用户管理",
        path: "user",
        component: "sys/user",
        icon: "user",
      },
      {
        id: 4,
        parent_menu_id: 1,
        title: "权限管理",
        path: "permission",
        component: "layout",
        icon: "user",
      },
      {
        id: 5,
        parent_menu_id: 4,
        title: "角色管理",
        path: "role",
        component: "sys/role",
        icon: "el-icon-user",
      },
      {
        id: 6,
        parent_menu_id: 4,
        title: "自定义权限",
        path: "custom-perm",
        component: "sys/permission",
        icon: "el-icon-user",
      },
      {
        id: 7,
        parent_menu_id: 1,
        title: "系统日志",
        path: "log",
        component: "sys/log",
        icon: "el-icon-document",
      },
      {
        id: 8,
        parent_menu_id: 1,
        title: "字典管理",
        path: "dic",
        component: "sys/dic",
        icon: "el-icon-document",
      },
      {
        id: 9,
        parent_menu_id: 1,
        title: "组织机构",
        path: "org",
        component: "sys/org",
        icon: "el-icon-s-opportunity",
      },
      {
        id: 10,
        parent_menu_id: 1,
        title: "系统配置",
        path: "config",
        component: "sys/config",
        icon: "el-icon-setting",
      },
      {
        id: 11,
        parent_menu_id: 1,
        title: "文件上传",
        path: "oss",
        component: "sys/oss",
        icon: "el-icon-document",
      },
      {
        id: 12,
        parent_menu_id: 1,
        title: "邮件管理",
        path: "email",
        component: "sys/email",
        icon: "el-icon-document",
      },
    ];
    baseMenus.forEach(async (item) => {
      await model.findOrCreate({
        defaults: item,
        where: {
          parent_menu_id: item.parent_menu_id,
          path: item.path,
        },
      });
    });
  }

  private async sysPermission() {
    const model = this.app.model.Sys.PermissionList;
    await model.findOrCreate({
      defaults: {
        menu_id: 5,
        cname: "角色批量授权",
        ename: "sys:role:batchPerm",
      },
      where: {
        ename: "sys:role:batchPerm",
      },
    });
  }

  private async sysUser() {
    const User = this.app.model.Sys.User;

    await User.findOrCreate({
      defaults: {
        user_name: "editor",
        password: "96e79218965eb72c92a549dd5a330112",
        real_name: "editor",
        is_use: true,
        role_ids: 1,
      },
      where: {
        user_name: "editor",
      },
    });
    await User.findOrCreate({
      defaults: {
        user_name: "admin",
        password: "96e79218965eb72c92a549dd5a330112",
        real_name: "admin",
        is_use: true,
        role_ids: 1,
      },
      where: {
        user_name: "admin",
      },
    });
  }

  
}
