/*
 * @copyright: Huang Ding
 * @Author: ding-cx
 * @Date: 2021-03-20 17:08:10
 * @LastEditors: ding-cx
 * @LastEditTime: 2021-03-20 17:15:05
 * @Description: file content
 */
import {Service} from "egg"

export default class DropdownService extends Service {

    /** 根据字典英文名称获取一个字典的项目列表 */
    async getDicItemByMenuEname(ename:string=''){
        const data = await this.app.model.Sys.DropdownMenuItem.findAll({
            raw:true,
            attributes:['label','value'],
            include:{
                model:this.app.model.Sys.DropdownMenu,
                as:"dropdown_item",
                where:{
                    ename,
                },
                attributes:[]
            }
        })

        console.log(data);
        return data;

    }
}