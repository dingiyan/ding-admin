/*
 * @copyright: Huang Ding
 * @Author: ding-cx
 * @Date: 2021-03-24 14:32:41
 * @LastEditors: ding-cx
 * @LastEditTime: 2021-03-26 11:23:56
 * @Description: file content
 */

import { Service } from "egg";

import { Singleton } from "egg";
import { Redis } from "ioredis";

// const prefix = "dingAdmin";

export default class RedisService extends Service {
  constructor(ctx) {
    super(ctx);
  }

  /**
   *获取redis实例，不选择db，由调用者自行选择db。如果不选，则使用config的默认连接db号。
   *
   * @param {string} [instanceName="default"] 实例名称，见config定义的
   * @return {*}
   * @memberof RedisService
   */
  getRedis(instanceName: string = "default"): Redis {
    return (this.app.redis as Singleton<Redis>).get(instanceName);
  }

  /**
   *获取redis实例同时设置选择db
   *
   * @param {string} [instanceName="default"] 数据库实例，在config中的clients匹配。
   * @param {number} dbNumber 要使用的数据库号码。目前默认为0-15个。
   * @return {*}
   * @memberof RedisService
   */
  async getRedisDb(
    instanceName: string = "default",
    dbNumber: number
  ): Promise<Redis> {
    let redis = (this.app.redis as Singleton<Redis>).get(instanceName);
    await redis.select(dbNumber);
    return redis;
  }
}
