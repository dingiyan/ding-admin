/*
 * @copyright: Huang Ding
 * @Author: ding-cx
 * @Date: 2021-02-23 10:37:56
 * @LastEditors: ding-cx
 * @LastEditTime: 2021-08-13 11:56:45
 * @Description: file content
 */
import { Application } from "egg";
import wsDecorator from "../libs/wsDecorator";

export default (app: Application) => {
  const { controller, router } = app;
  const route_prefix = app.config.route_prefix.api;

  wsDecorator.bind(app)
  // 系统、用户 注册api
  app.routerRegister(
    router,
    controller.sys.user,
    "SysUser",
    route_prefix + "/sys/user",
    []
  );

  // 系统安装接口
  router.get('/sys-install',controller.sys.install.index)

  // 系统用户登录
  router.post(
    "/sys/user/login",
    app.middleware.loginLog({}),
    controller.sys.user.userLogin
  );
  router.post(
    "/sys/user/logout",
    app.middleware.loginLog({ type: "logout" }),
    controller.sys.user.userLogout
  );
  // 用户信息获取
  router.get("/sys/user/info", controller.sys.user.info);

  // 验证码图片获取 api
  router.get("/sys/captcha/generate/:uuid", controller.sys.captcha.generate);

  /** ------------------------------------------------- */
  // 获取后端做好的菜单路由树结构
  router.get(
    route_prefix + "/sys/menus/getRouteTree",
    controller.sys.menus.getRouteTree
  );
  // 动态生成的具有权限的菜单路由树
  router.get(
    route_prefix + "/sys/menus/getRouteMenu",
    controller.sys.menus.getRouteMenu
  );
  router.get(
    route_prefix + "/sys/menus/getRouteByParentId/:parentId",
    controller.sys.menus.getRouteListByParentId
  );
  router.post(
    route_prefix + "/sys/menus/getRouteListByTreeNode",
    controller.sys.menus.getRouteListByTreeNode
  );
  // 菜单管理
  app.routerRegister(
    router,
    controller.sys.menus,
    "SysMenus",
    route_prefix + "/sys/menus"
  );
  /** ------------------------------------------------- */
  /** ------------------------------------------------- */
  /** 组织部门机构管理 */
  router.get(
    "SysOrg",
    route_prefix + "/sys/org/tree/getTreeList",
    controller.sys.org.getTreeList
  );
  router.post(
    "SysOrg",
    route_prefix + "/sys/org/tree/getOrgListByIdList",
    controller.sys.org.getOrgListByIdList
  );
  app.routerRegister(
    router,
    controller.sys.org,
    "SysOrg",
    route_prefix + "/sys/org"
  );
  /** ------------------------------------------------- */

  /** 用户角色 */
  app.routerRegister(
    router,
    controller.sys.role,
    "SysUserRole",
    route_prefix + "/sys/user/role"
  );

  /** 权限 */
  router.post(
    route_prefix + "/sys/permission/batchSetRolePermission",
    controller.sys.permission.batchSetRolePermission
  );
  router.get(
    route_prefix + "/sys/permission/getListByMenuId/:menu_id",
    controller.sys.permission.getListByMenuId
  );
  router.get(
    route_prefix + "/sys/permission/getAllPermByRoleId/:role_id",
    controller.sys.permission.getAllPermByRoleId
  );
  /** 特殊 获取一个用户的权限表，前端用来判断权限。 */
  router.get(
    route_prefix + "/sys_user/permission/getAllPermByUser",
    controller.sys.permission.getAllPermByUser
  );
  app.routerRegister(
    router,
    controller.sys.permission,
    "SysMenuPermission",
    route_prefix + "/sys/permission"
  );

  /** 系统日志表 */
  app.routerRegister(
    router,
    controller.sys.log,
    "SysLog",
    route_prefix + "/sys/log"
  );

  /** 系统 - 下拉菜单字典管理 */
  app.routerRegister(
    router,
    controller.sys.dropdownMenu,
    "SysDropdownMenu",
    route_prefix + "/sys/dic"
  );
  router.get(
    route_prefix + "/sys/dic-list/getDicList",
    controller.sys.dropdownMenu.getDicList
  );
  router.post(
    route_prefix + "/sys/dic-list/",
    controller.sys.dropdownMenu.dicAdd
  );
  // 根据菜单英文名，获取一个菜单的项目列表。
  router.get(
    route_prefix + "sys/dic/getDicItem/:menu_ename",
    controller.sys.dropdownMenu.getDicItemByMenuEname
  );

  /** ------------系统配置管理------------------- */
  // 获取配置对象。整个配置list object
  router.get(
    route_prefix + "/sys/config/get/object",
    controller.sys.config.getConfigObject
  );
  app.routerRegister(
    router,
    controller.sys.config,
    "SysConfig",
    route_prefix + "/sys/config",
    [app.middleware.sys.configObject()]
  );
  /** ------------系统配置管理------------------- / */
  /** ------------云存储，文件上传管理-------------------   */
  // 文件上传接口
  router.post(route_prefix + "/sys/oss/upload", controller.sys.oss.upload);
  // 获取文件
  router.get(
    "/oss/local/file/:month/:fileName",
    controller.sys.oss.getLocalOssFile
  );
  app.routerRegister(
    router,
    controller.sys.oss,
    "SysOss",
    route_prefix + "/sys/oss"
  );
  /** ------------云存储，文件上传管理------------------- /  */

  /** ------------系统 邮件发送log-------------------   */
  router.post(
    route_prefix + "/sys/email/sendEmail",
    controller.sys.email.sendEmail
  );
  app.routerRegister(
    router,
    controller.sys.email,
    "SysEmail",
    route_prefix + "/sys/email"
  );

  // router.post("/query-sql/getSearchData", controller.sys.querySql.test);

  // socket.io
  // app.io.of("/").route("chat", app.io.controller.nsp.chat);
};
