/*
 * @copyright: Huang Ding
 * @Author: ding-cx
 * @Date: 2021-02-26 17:15:12
 * @LastEditors: ding-cx
 * @LastEditTime: 2021-04-12 15:55:35
 * @Description: file content
 */

import { DataTypes } from "sequelize";
import * as moment from "moment";

export const modelCommonField = {
  create_user_id: {
    field: "create_user_id",
    comment: "创建者id",
    type: DataTypes.INTEGER,
  },
  edit_user_id: {
    field: "edit_user_id",
    comment: "修改者id",
    type: DataTypes.INTEGER,
  },
  createdAt: {
    type: DataTypes.DATE,
    get() {
      const val = (this as any).getDataValue("createdAt");
      if (val) {
        return moment(val).format("YYYY-MM-DD HH:mm:ss");
      }
      return val;
    },
  },
  updatedAt: {
    type: DataTypes.DATE,
    get() {
      const val = (this as any).getDataValue("updatedAt");
      if (val) {
        return moment(val).format("YYYY-MM-DD HH:mm:ss");
      }
      return val;
    },
  },
  deletedAt: {
    type: DataTypes.DATE,
    get() {
      const val = (this as any).getDataValue("deletedAt");
      if (val) {
        return moment(val).format("YYYY-MM-DD HH:mm:ss");
      }
      return val;
    },
  },
};

export interface commonFieldInterface {
  /** 创建者id */
  create_user_id: number;

  /** 修改者id */
  edit_user_id: number;
  /** 创建时间 */
  createdAt: string;
  /** 修改时间 */
  updatedAt: string;
  /** 删除时间 */
  deletedAt: string;
}
