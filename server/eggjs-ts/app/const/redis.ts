/*
 * @copyright: Huang Ding
 * @Author: ding-cx
 * @Date: 2021-03-26 10:44:53
 * @LastEditors: ding-cx
 * @LastEditTime: 2021-03-26 11:03:26
 * @Description: file content
 */

const prefix = "ding_admin_redis_cache";

/** redis的key */
export default {
  /** app 系统配置 */
  dingAdminSysConfig: prefix + "SysConfig",
  
};
