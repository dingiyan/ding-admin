/*
 * @copyright: Huang Ding
 * @Author: ding-cx
 * @Date: 2021-03-06 09:55:06
 * @LastEditors: ding-cx
 * @LastEditTime: 2021-03-06 09:55:20
 * @Description: file content
 */
export default {
  parseMsg(action, payload = {}, metadata = {}) {
    const meta = Object.assign(
      {},
      {
        timestamp: Date.now(),
      },
      metadata
    );

    return {
      meta,
      data: {
        action,
        payload,
      },
    };
  },
};
