/*
 * @copyright: Huang Ding
 * @Author: ding-cx
 * @Date: 2021-02-26 16:57:25
 * @LastEditors: ding-cx
 * @LastEditTime: 2021-04-09 13:46:34
 * @Description: file content
 */
import { Application } from "egg";
import { DataTypes, Model } from "sequelize";
import { modelCommonField } from "../utils";

export interface roleInterface extends Model {
  fieldName: string;
}

export default (app: Application) => {
  const model = app.model.define<roleInterface>(
    app.config.dbModel_prefix + "table_name",
    {
      fieldName: {
        comment: "备注信息",
        type: DataTypes.STRING(30),
      },
      ...modelCommonField
    }
    ,
    {
      comment: "表注释",
    }
  );
  return model;
};
