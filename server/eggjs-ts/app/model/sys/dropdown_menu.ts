/*
 * @copyright: Huang Ding
 * @Author: ding-cx
 * @Date: 2021-02-26 16:57:25
 * @LastEditors: ding-cx
 * @LastEditTime: 2021-04-09 12:45:00
 * @Description: file content
 */
import { Application } from "egg";
import { DataTypes, Model } from "sequelize";
import { modelCommonField } from "../../utils";

export interface dropdownMenuInterface extends Model {
  id: number;
  /** 英文名 */
  ename: string;
  /** 中文名 */
  cname: string;
  /** 备注 */
  memo: string;
}

export default (app: Application) => {
  const model = app.model.define<dropdownMenuInterface>(
    app.config.dbModel_prefix + "sys_dropdown_menu",
    {
      ename: {
        field: "ename",
        comment: "菜单名",
        type: DataTypes.STRING(30),
      },
      cname: {
        field: "cname",
        comment: "中文名",
        type: DataTypes.STRING(30),
      },
      memo: {
        field: "memo",
        comment: "备注",
        type: DataTypes.STRING(300),
      },
      ...modelCommonField,
    },{
      comment:"字典表"
    }
  );
  (model as any).associate = () => {
    model.hasMany(app.model.Sys.DropdownMenuItem, {
      foreignKey: "menu_id",
      as: "dropdown_item",
      constraints: false,
    });
  };
  return model;
};
