/*
 * @copyright: Huang Ding
 * @Author: ding-cx
 * @Date: 2021-02-26 16:57:25
 * @LastEditors: ding-cx
 * @LastEditTime: 2021-04-23 16:07:59
 * @Description: file content
 */
import { Application } from "egg";
import { DataTypes, Model } from "sequelize";
import { modelCommonField, commonFieldInterface } from "../../utils";

export interface sysMenusInterface extends Model, commonFieldInterface {
  /** 主键id */
  id: number;
  /** 父菜单id */
  parent_menu_id: number;
  /** 菜单排序号 */
  order: number;
  /** 菜单的route name，同时也是path */
  path: string;
  /** 菜单名称，侧边栏显示名称 */
  title: string;
  /** 组件名 基于前端views目录*/
  component: string;
  /** 重定向路径 */
  redirect: string;
  /** 是否隐藏，不在侧边栏显示 */
  is_hidden: boolean;
  /** 是否总是在侧栏显示根路由 */
  always_show: boolean;
  /** 菜单图标 */
  icon: string;
  /** 不缓存 */
  no_cache: boolean;
  /** 固定在tagview */
  affix: boolean;
  /** 是否展示在面包屑 */
  breadcrumb: boolean;
  /** 当前页面激活左侧菜单的路径 */
  active_menu: boolean;
  /** 路由组件name选项 */
  component_name: string;
}

export default (app: Application) => {
  const model = app.model.define<sysMenusInterface>(
    app.config.dbModel_prefix + "sys_menus",
    {
      parent_menu_id: {
        field: "parent_menu_id",
        comment: "父菜单id", //没有父级则为0
        type: DataTypes.INTEGER,
      },
      order: {
        field: "order",
        comment: "菜单排序号", //没有父级则为0
        type: DataTypes.INTEGER,
      },
      path: {
        field: "path",
        comment: "菜单的routename,同时也是path",
        type: DataTypes.STRING(60),
      },
      title: {
        field: "title",
        comment: "侧边栏显示名称",
        type: DataTypes.STRING(30),
      },
      component: {
        field: "component",
        comment: "组件名，基于views",
        type: DataTypes.STRING(200),
      },
      redirect: {
        field: "redirect",
        comment: "重定向路径",
        type: DataTypes.STRING(200),
      },
      is_hidden: {
        field: "is_hidden",
        comment: "是否隐藏，不在侧边栏的显示",
        type: DataTypes.BOOLEAN,
      },
      always_show: {
        field: "always_show",
        comment: "是否总是在侧栏显示根路由",
        type: DataTypes.BOOLEAN,
      },
      icon: {
        field: "icon",
        comment: "菜单图标",
        type: DataTypes.STRING(50),
      },
      no_cache: {
        field: "no_cache",
        comment: "不缓存",
        type: DataTypes.BOOLEAN,
      },
      affix: {
        field: "affix",
        comment: "固定在tagview",
        type: DataTypes.BOOLEAN,
      },
      breadcrumb: {
        field: "breadcrumb",
        comment: "是否展示在面包屑",
        type: DataTypes.BOOLEAN,
      },
      active_menu: {
        field: "active_menu",
        comment: "当前页面激活左侧菜单的路径",
        type: DataTypes.STRING(200),
      },
      component_name: {
        field: "component_name",
        comment: "对应路由组件的name选项",
        type: DataTypes.STRING(120),
      },
      ...modelCommonField,
    },
    {
      comment: "系统菜单，页面",
    }
  );

  /* 创建关联关系 hasOne是我的id对应as的parent_menu_id*/
  // model.hasOne(model, {
  //   foreignKey: "parent_menu_id",
  //   as: "parentMenu",
  // });
  (model as any).associate = () => {
    model.belongsTo(model, {
      foreignKey: "parent_menu_id",
      as: "parent_menu",
      constraints: false,
    });
  };


  return model;
};
