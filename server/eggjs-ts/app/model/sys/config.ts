/*
 * @copyright: Huang Ding
 * @Author: ding-cx
 * @Date: 2021-02-26 16:57:25
 * @LastEditors: ding-cx
 * @LastEditTime: 2021-04-23 16:02:59
 * @Description: 系统配置表
 */
import { Application } from "egg";
import { DataTypes, Model } from "sequelize";
import { modelCommonField, commonFieldInterface } from "../../utils";

export interface sysConfigInterface extends Model, commonFieldInterface {
  /** 主键id */
  id: number;
  /** 名称 */
  name: string;
  /** 英文键名 */
  key: string;
  /** 内容，可json parse的字符串 */
  value: string;
  /** 备注 */
  memo: string;
}

export default (app: Application) => {
  const model = app.model.define<sysConfigInterface>(
    app.config.dbModel_prefix + "sys_config",
    {
      name: {
        field: "name",
        comment: "名称",
        type: DataTypes.STRING(40),
      },
      key: {
        field: "key",
        comment: "英文键名",
        type: DataTypes.STRING(80),
        unique: true,
      },
      value: {
        field: "value",
        comment: "英文键名",
        type: DataTypes.STRING(5000),
      },
      memo: {
        field: "memo",
        comment: "备注",
        type: DataTypes.STRING(300),
      },

      ...modelCommonField,
    },
    {
      comment: "系统自定义配置表",
    }
  );


  return model;
};
