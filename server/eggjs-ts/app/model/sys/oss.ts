/*
 * @copyright: Huang Ding
 * @Author: ding-cx
 * @Date: 2021-02-26 16:57:25
 * @LastEditors: ding-cx
 * @LastEditTime: 2021-04-09 13:43:36
 * @Description: 系统oss,云存储文件表。上传文件均通过此处保存，然后url可拿去使用。
 */
import { Application } from "egg";
import { DataTypes, Model } from "sequelize";
import { modelCommonField } from "../../utils";

export interface sysOssInterface extends Model {
  /** 主键id */
  id: number;
  /** 文件存储的 url */
  url: string;

  /** 创建者id */
  create_user_id: number;

  /** 修改者id */
  edit_user_id: number;
  /** 创建时间 */
  createdAt: string;
  /** 修改时间 */
  updatedAt: string;
  /** 删除时间 */
  deletedAt: string;
}

export default (app: Application) => {
  const model = app.model.define<sysOssInterface>(
    app.config.dbModel_prefix + "sys_oss",
    {
      url: {
        field: "url",
        comment: "文件url",
        type: DataTypes.STRING(1200),
      },

      ...modelCommonField,
    },
    {
      comment: "文件上传",
    }
  );

  // egg-sequelize绑定关联关系的方法。
  (model as any).associate = () => {
    model.belongsTo(app.model.Sys.User, {
      foreignKey: "create_user_id",
      as: "create_user",
      constraints: false,
    });
  };

  return model;
};
