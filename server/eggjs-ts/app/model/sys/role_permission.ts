/*
 * @copyright: Huang Ding
 * @Author: ding-cx
 * @Date: 2021-02-26 16:57:25
 * @LastEditors: ding-cx
 * @LastEditTime: 2021-04-09 13:45:51
 * @Description: file content
 */
import { Application } from "egg";
import { DataTypes, Model } from "sequelize";
import { modelCommonField } from "../../utils";

export interface rolePermission extends Model {
  /** 角色id */
  role_id: number;
  /** 权限id */
  perm_id: number;
  /** 对应菜单id */
  menu_id: number;
  /** 权限类型 */
  type: number;
  /** 创建者id */
  create_user_id: number;
  /** 修改者id */
  edit_user_id: number;
}

export default (app: Application) => {
  const model = app.model.define<rolePermission>(
    app.config.dbModel_prefix + "role_permission",
    {
      role_id: {
        field: "role_id",
        comment: "角色id",
        type: DataTypes.INTEGER,
      },
      menu_id: {
        field: "menu_id",
        comment: "对应菜单id",
        type: DataTypes.INTEGER,
      },
      perm_id: {
        field: "perm_id",
        comment: "授权标识id", //如果是0类型，则记录菜单的menu_id,1,则记录通用功能权限的id，2则是自定义权限的perm_id字段
        type: DataTypes.INTEGER,
      },
      type: {
        field: "type",
        comment: "权限类型", //控制 0 菜单权限，1通用功能权限，2自定义功能权限
        type: DataTypes.INTEGER,
      },
      ...modelCommonField,
    },
    {
      paranoid: false,
      comment:"角色拥有权限列表"
    }
  );

  (model as any).associate = () => {
    model.belongsTo(app.model.Sys.PermissionList, {
      foreignKey: "perm_id",
      as: "role_perm_perm",
      constraints: false,
    });
  };

  return model;
};
