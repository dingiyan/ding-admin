/*
 * @copyright: Huang Ding
 * @Author: ding-cx
 * @Date: 2021-02-26 16:57:25
 * @LastEditors: ding-cx
 * @LastEditTime: 2021-04-12 15:34:39
 * @Description: file content
 */
import { Application } from "egg";
import { DataTypes, Model } from "sequelize";
import { modelCommonField, commonFieldInterface } from "../../utils";

export interface emailInterface extends Model, commonFieldInterface {
  /** 发送人 */
  sender: string;
  /** 接收人 以,隔开的多个*/
  receiver: string;
  /** 邮件主题 */
  subject: string;
  /** 邮件内容 */
  content: string;
  /** 发送时间 */
  send_time: string;
  /** 结果 0失败1成功*/
  state: 0 | 1;
  /** 邮件类型 0系统邮件，1用户邮件*/
  type: 0 | 1;
}

export default (app: Application) => {
  const model = app.model.define<emailInterface>(
    app.config.dbModel_prefix + "sys_email",
    {
      sender: {
        field: "sender",
        comment: "发送人",
        type: DataTypes.STRING(120),
      },
      receiver: {
        field: "receiver",
        comment: "接收人",
        type: DataTypes.STRING(3000),
      },
      subject: {
        field: "subject",
        comment: "主题",
        type: DataTypes.STRING(500),
      },
      content: {
        field: "content",
        comment: "内容",
        type: DataTypes.TEXT,
      },

      send_time: {
        field: "send_time",
        comment: "发送时间",
        type: DataTypes.STRING(22),
      },

      type: {
        field: "type",
        comment: "0系统邮件，1用户发送",
        type: DataTypes.TINYINT,
      },
      state: {
        field: "state",
        comment: "0失败1成功",
        type: DataTypes.TINYINT,
      },

      ...modelCommonField,
    },
    {
      comment: "系统，邮件发送记录表",
    }
  );
  return model;
};
