/*
 * @copyright: Huang Ding
 * @Author: ding-cx
 * @Date: 2021-02-26 16:57:25
 * @LastEditors: ding-cx
 * @LastEditTime: 2021-04-09 12:44:41
 * @Description: file content
 */
import { Application } from "egg";
import { DataTypes, Model } from "sequelize";
import { modelCommonField } from "../../utils";

export interface dropdownMenuItemInterface extends Model {
  id: number;
}

export default (app: Application) => {
  const model = app.model.define<dropdownMenuItemInterface>(
    app.config.dbModel_prefix + "sys_dropdown_menu_item",
    {
      menu_id: {
        field: "menu_id",
        comment: "菜单id",
        type: DataTypes.INTEGER,
      },
      label: {
        field: "label",
        comment: "显示名称",
        type: DataTypes.STRING(30),
      },
      value: {
        field: "value",
        comment: "菜单值",
        type: DataTypes.STRING(30),
      },
      orders: {
        field: "orders",
        comment: "排序号",
        type: DataTypes.INTEGER,
      },
      is_edit: {
        field: "is_edit",
        comment: "是否可修改",
        type: DataTypes.BOOLEAN,
      },

      ...modelCommonField,
    },{
      comment:"字典，项目表"
    }
  );
  (model as any).associate = () => {
    model.belongsTo(app.model.Sys.DropdownMenu, {
      foreignKey: "menu_id",
      as: "dropdown_item",
      constraints: false,
    });
  };
  return model;
};
