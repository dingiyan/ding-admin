/*
 * @copyright: Huang Ding
 * @Author: ding-cx
 * @Date: 2020-12-29 08:44:29
 * @LastEditors: ding-cx
 * @LastEditTime: 2021-04-09 12:42:58
 * @Description: 系统 验证码 模型
 */

import { Application } from "egg";
import { Model } from "sequelize";

export interface sys_captcha extends Model {
  cap_uuid: string;
  captcha_time: string;
  ip_address: string;
  /** 生成的验证码 */
  word: string;
}

/**
 *sys_captcha 系统验证码模型
 *
 * @param {*} app
 * @return {*}
 */
export default (app: Application) => {
  const { STRING } = app.Sequelize;

  const Captcha = app.model.define<sys_captcha>(
    app.config.dbModel_prefix + "sys_captcha",
    {
      // 注意所有模型都有自动添加的id字段。因此无需手动添加主键。

      cap_uuid: {
        type: STRING(36),
        unique: true,
        comment: "前端传来的验证码UUID",
      },
      captcha_time: {
        type: STRING(22),
        comment: "生成时间",
      },
      ip_address: {
        type: STRING(45),
        comment: "ip地址",
        allowNull: false,
      },
      word: {
        type: STRING(10),
        comment: "生成字符",
      },
    },
    {
      freezeTableName: true, //冻结表名，因sequelize会自动复数 define定义的表名。冻结之后既直接使用define作为表名。
      timestamps: true, //使用自动的时间戳字段。
      comment:"验证码"
    }
  );

  return Captcha;
};
