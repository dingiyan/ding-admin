/*
 * @copyright: Huang Ding
 * @Author: ding-cx
 * @Date: 2021-03-06 12:08:16
 * @LastEditors: ding-cx
 * @LastEditTime: 2021-03-06 12:08:54
 * @Description: file content
 */
export default () => {
    return async (ctx, next) => {
      console.log(ctx.packet);
    //   const say = await ctx.service.user.say();
      ctx.socket.emit('res', 'packet!');
      await next();
      console.log('packet response!');
    };
  };