/*
 * @copyright: Huang Ding
 * @Author: ding-cx
 * @Date: 2021-03-06 11:24:29
 * @LastEditors: ding-cx
 * @LastEditTime: 2021-03-06 16:24:21
 * @Description: file content
 */
export default () => {
  return async (ctx, next) => {
    const { socket } = ctx;
    const query = socket.handshake.query;
    // const nsp = ctx.app.io.of("/");
    console.log(query.userId);
    // socket.emit("res", "auth!");
    socket.emit("res", "----connect res.");
    await next();
    console.log("disconnect!");
  };
};
