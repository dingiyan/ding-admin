/*
 * @copyright: Huang Ding
 * @Author: ding-cx
 * @Date: 2021-03-06 09:57:36
 * @LastEditors: ding-cx
 * @LastEditTime: 2021-03-06 16:05:41
 * @Description: file content
 */
// const Controller = require('egg').Controller;
import { Controller } from "egg";

class NspController extends Controller {
  async chat() {
    const msg = this.ctx.args[0] + " : " + process.pid;
    const nsp = this.app.io.of("/");
    console.log(msg);
    nsp.emit("res", msg);
  }
}

export default NspController;
