/*
 * @copyright: Huang Ding
 * @Author: ding-cx
 * @Date: 2021-03-06 09:14:29
 * @LastEditors: ding-cx
 * @LastEditTime: 2021-03-06 09:34:39
 * @Description: file content
 */
// const Controller = require('egg').Controller;
import { Controller } from "egg";

class DefaultController extends Controller {
    /** ping hello.io */
  async ping() {
    const { ctx } = this;
    // const message = ctx.args[0];
    const message = "hello.";
    await ctx.socket.emit("res", `Hi! I've got your message: ${message}`);
  }
}

export default DefaultController;
