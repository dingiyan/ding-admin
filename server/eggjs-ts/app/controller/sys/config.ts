/*
 * @copyright: Huang Ding
 * @Author: ding-cx
 * @Date: 2021-03-19 11:41:39
 * @LastEditors: ding-cx
 * @LastEditTime: 2021-04-08 18:10:41
 * @Description: file content
 */

import { FuncController as Controller } from "../../../libs/controller/Common";
import { Op } from "sequelize";

export default class SysConfigController extends Controller {
  model = this.app.model.Sys.Config;

  async getSearchData() {
    const page: [number, number] = this.ctx.request.body.page || [20, 0];
    const searchs = this.ctx.request.body.searchs || null;
    console.log(searchs);
    let conditions = {};
    if (searchs) {
      // 按name模糊查询
      searchs.name && (conditions["name"] = { [Op.substring]: searchs.name });
    }

    const data = await this.app.model.Sys.Config.findAndCountAll({
      raw: false,
      limit: page[0],
      offset: page[1],
      order: [["createdAt", "desc"]],
      where: {
        ...conditions,
      },
    });
    this.ctx.success(data);
    // const conf = await this.ctx.service.sys.config.getConfigObject();
    // console.log(conf);
  }

  /** 获取系统配置对象 */
  async getConfigObject() {
    const ret = await this.ctx.service.sys.config.getConfigObject();
    this.ctx.success(ret);
  }
}
