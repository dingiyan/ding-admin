/*
 * @copyright: Huang Ding
 * @Author: ding-cx
 * @Date: 2021-04-23 15:19:14
 * @LastEditors: ding-cx
 * @LastEditTime: 2021-04-23 16:25:27
 * @Description: file content
 */

import { Controller } from "egg";

export default class InstallController extends Controller {
  async index() {
    try {
      await this.ctx.model.sync({ force: true });
      await this.ctx.service.installer.insertRows();
      this.ctx.success({state:true,msg:"初始化安装完成！"});
    } catch (error) {
      this.ctx.logger.error(
        "first use system. install database occur error.初次使用系统安装db出现错误！ %j",
        error
      );
      this.ctx.error("安装发生错误！");
    }
  }
}
