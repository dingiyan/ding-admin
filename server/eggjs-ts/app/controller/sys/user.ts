/*
 * @copyright: Huang Ding
 * @Author: ding-cx
 * @Date: 2021-02-23 10:37:56
 * @LastEditors: ding-cx
 * @LastEditTime: 2021-08-30 14:54:39
 * @Description: file content
 */
import { Context } from "egg";
import { FuncController as Controller } from "../../../libs/controller/Common";
import { Op } from "sequelize";

/**
 *系统 -> 用户 控制器
 *
 * @export
 * @class SysUserController
 * @extends {Controller}
 */
export default class SysUserController extends Controller {
  model = this.app.model.Sys.User;

  /**
   *用户登录
   *
   * @return {*}
   * @memberof SysUserController
   */
  async userLogin() {
    const { app, ctx } = this;
    const { body: data } = ctx.request;

    const dbCap = await app.model.Sys.Captcha.findOne({
      where: {
        cap_uuid: data.cap_uuid,
      },
    });

    // console.log(dbCap?.toJSON());
    // TODO 暂时跳过验证码 不方便开发
    if (false == data) {
      if (dbCap) {
        if (dbCap.word.toLowerCase() !== data.word.toLowerCase()) {
          ctx.error("验证码错误！");
          return;
        }
      } else {
        // db not exist captcha.
        // ctx.send("验证码错误！", 50000);
        ctx.error("验证码错误！");
        return;
      }
    }

    // console.log(data);

    // 匹配用户输入和数据库的账户密码是否匹配。
    const dbUser = await app.model.Sys.User.findOne({
      raw: true,
      where: {
        user_name: data.user_name,
        password: data.password,
      },
    });
    // console.log(dbUser);

    // 如果用户密码未匹配到数据库。
    if (!dbUser) {
      // ctx.send(null, 50000, "用户名或密码错误！");
      ctx.error("用户名或密码错误！");
      return;
    }

    // 如果用户未启用。禁止登录。
    if (!dbUser.is_use) {
      ctx.error("该用户未启用，暂不能登录！请联系管理员！");
      return;
    }

    // 验证通过。
    // 用用户信息 生成jwt token 保存在jwt中。后续作为登录凭证出来。
    const jwtPayload: Context["loginUserInfo"] = {
      uid: dbUser.id as number,
      name: dbUser.user_name as string,
    };
    const token = app.jwt.sign(jwtPayload, this.config.jwt.secret, {
      expiresIn: this.config.jwtLogin.expires,
    });

    // 同时存一份session
    dbUser.password = "";
    ctx.session = {};
    ctx.session.real_name = dbUser.real_name;
    ctx.session.avatar = dbUser.avatar;
    ctx.session.role_ids = dbUser.role_ids ? dbUser.role_ids.split(",") : [];

    ctx.send(
      {
        token,
        msg: "登录成功！",
      },
      20000,
      "登录成功"
    );
  }

  /**
   *用户登出。
   *
   * @memberof SysUserController
   */
  async userLogout() {
    // TODO 用户登出，需处理缓存清除等。
    this.ctx.session = {};
    this.ctx.success(true);
  }

  /**
   * 获取用户信息接口。
   * 获取用户的基本信息和该用户的除菜单0权限之外的通用和自定义权限表。
   */
  async info() {
    const { ctx } = this;

    // TODO 暂时从session存取拿用户信息。后续可改造成redis缓存。因调用会稍微频繁一点。
    // let userInfo = {};

    // 获取权限列表
    const ret = await this.ctx.service.sys.permission.getAllPermByUser(ctx.loginUserInfo.uid as number);

    ctx.send({
      real_name: ctx.session.real_name,
      roles: ctx.session.role_ids,
      introduction: "",
      avatar: ctx.session.avatar || "https://wpimg.wallstcn.com/f778738c-e4f8-4870-b634-56703b4acafe.gif",
      name: ctx.loginUserInfo.name,
      // 权限表
      perm: ret,
      // uid: ctx.loginUserInfo.uid,
    });
  }

  /** 搜索获取列表 */
  async getSearchData() {
    const page: [number, number] = this.ctx.request.body.page || [20, 0];

    const { searchs } = this.ctx.request.body;

    const conditions = {} as {
      user_name: any;
    };

    if (searchs) {
      // 按名称模糊查询搜索
      searchs.user_name && (conditions.user_name = { [Op.substring]: searchs.user_name });
    }

    const allUser = await this.app.model.Sys.User.findAndCountAll({
      raw: false,
      limit: page[0],
      offset: page[1],
      where: {
        ...conditions,
      },
    });
    this.ctx.success(allUser);
  }
}
