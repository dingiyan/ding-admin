/*
 * @copyright: Huang Ding
 * @Author: ding-cx
 * @Date: 2021-02-27 10:23:08
 * @LastEditors: ding-cx
 * @LastEditTime: 2021-03-16 17:08:17
 * @Description: file content
 */
import { FuncController as Controller } from "../../../libs/controller/Common";

export default class MenusController extends Controller {
  model = this.app.model.Sys.Menus;

  /** 修改通用api的数据。添加默认的创建，修改人uid */
  util_formatter(args, method) {
    if (method === "create") {
      args.body.create_user_id = this.ctx.loginUserInfo.uid;
    } else if (method === "update") {
      args.body.edit_user_id = this.ctx.loginUserInfo.uid;
    }
    return args;
  }

  /**
   *提供前端路由树结构
   *
   * @memberof MenusController
   */
  async getRouteTree() {
    const route = await this.ctx.service.sys.menus.createFrontEndRoute(false);
    // this.ctx.success(route);
    this.ctx.send(route);



  }

  /** 权限菜单 */
  async getRouteMenu() {
    const route = await this.ctx.service.sys.menus.createFrontEndRoute(true);
    // this.ctx.success(route);
    this.ctx.send(route);
  }

  /**
   *根据上级菜单id获取菜单列表
   *
   * @memberof MenusController
   */
  async getRouteListByParentId() {
    const parentId = this.ctx.params.parentId;
    if (parentId) {
      const route = await this.ctx.service.sys.menus.getRouteListByParentId(
        parentId
      );
      this.ctx.success(route);
    } else {
      this.ctx.error("请传入parentId参数。");
    }
  }

  /**
   *根据树节点点击获取其及所有后代id的菜单列表
   *
   * @memberof MenusController
   */
  async getRouteListByTreeNode() {
    const idList: number[] = this.ctx.request.body.idList;
    const page = this.ctx.request.body.page || [20, 0]; //分页参数
    // console.log(this.ctx.request.body);
    if (idList && idList.length > 0) {
      const route = await this.ctx.service.sys.menus.getRouteListByIdList(
        idList,
        page
      );
      this.ctx.success(route);
    } else {
      this.ctx.error("请传入idList参数。");
    }
  }
}
