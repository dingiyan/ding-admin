/*
 * @copyright: Huang Ding
 * @Author: ding-cx
 * @Date: 2021-03-19 11:41:39
 * @LastEditors: ding-cx
 * @LastEditTime: 2021-04-12 16:49:11
 * @Description: file content
 */

import { FuncController as Controller } from "../../../libs/controller/Common";
import DateFormat from "@diyaner/dy-utils/libs/DateFormat";

export default class SysEmailController extends Controller {
  model = this.app.model.Sys.Email;

  async getSearchData() {
    const page: [number, number] = this.ctx.request.body.page || [20, 0];
    const searchs: {
      method: string;
    } = this.ctx.request.body.searchs;

    let wheres = {} as any;
    if (searchs) {
      wheres = searchs;
    }
    console.log(searchs);
    const alldata = await this.app.model.Sys.Email.findAndCountAll({
      raw: false,
      limit: page[0],
      offset: page[1],
      order: [["createdAt", "desc"]],
      where: {
        ...wheres,
      },
    });
    this.ctx.success(alldata);
  }

  /** 发送邮件 封装了发邮件的插件 */

  async sendEmail() {
    const data = this.ctx.request.body;

    const sender = await this.ctx.service.sys.config.getConfigObject();
    console.log(sender.email);
    const config = sender.email || {
      host: "smtp.163.com",
      port: 25,
      secure: false,
      auth: {
        user: "ding25199@163.com",
        pass: "ding940908",
      },
    };
    this.ctx.service.nodemailer.createTransporter(config);
    // 发送邮件
    const mailData = {
      user_name: "dingding",
      to: data.to,
      subject: "node egg notification",
      html: "<h1>hello I'm node send email.发送邮件。</h1>",
    };
    const ret = await this.ctx.service.nodemailer.sendEmail(mailData);
    // console.log(ret);
    const emailRow = await this.model.create(
      {
        sender: ret.from,
        receiver: ret.to,
        subject: mailData.subject,
        content: mailData.html,
        state: ret.state,
        type: 0,
        send_time: new DateFormat().format("yyyy-MM-dd hh:mm:ss"),
      },
      { raw: true }
    );
    this.ctx.success(emailRow);
  }
}
