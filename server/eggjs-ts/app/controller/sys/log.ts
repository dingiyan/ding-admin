/*
 * @copyright: Huang Ding
 * @Author: ding-cx
 * @Date: 2021-03-19 11:41:39
 * @LastEditors: ding-cx
 * @LastEditTime: 2021-03-26 12:00:46
 * @Description: file content
 */

import { FuncController as Controller } from "../../../libs/controller/Common";

export default class SysLogController extends Controller {
  model = this.app.model.Sys.Log;

  async getSearchData() {
    const page: [number, number] = this.ctx.request.body.page || [20, 0];
    const searchs: {
      method: string;
    } = this.ctx.request.body.searchs;

    let wheres = {} as any;
    if (searchs) {
      wheres = searchs;
    }
    console.log(searchs);
    const allLog = await this.app.model.Sys.Log.findAndCountAll({
      raw: false,
      limit: page[0],
      offset: page[1],
      order: [["createdAt", "desc"]],
      where: {
        ...wheres,
      },
    });
    this.ctx.success(allLog);

    // try {
    //   const ret = await this.ctx.curl(
    //     "http://cpquery.sipo.gov.cn/txnQueryFeeData.do?select-key:shenqingh=2020300530894&select-key:zhuanlilx=3&select-key:gonggaobj=&select-key:backPage=http%3A%2F%2Fcpquery.sipo.gov.cn%2FtxnQueryOrdinaryPatents.do%3Fselect-key%3Asortcol%3D%26select-key%3Asort%3D%26select-key%3Ashenqingh%3D2020300530894%26select-key%3Azhuanlimc%3D%26select-key%3Ashenqingrxm%3D%26select-key%3Azhuanlilx%3D%26select-key%3Ashenqingr_from%3D%26select-key%3Ashenqingr_to%3D%26verycode%3D6%26inner-flag%3Aopen-type%3Dwindow%26inner-flag%3Aflowno%3D1616137428994&token=D3362B726B4040CDB4106244D22D542B&inner-flag:open-type=window&inner-flag:flowno=1616137434816"
    //   );

    //   console.log(ret);
    // } catch (error) {
    //   console.log(error);
    // }
  }
}
