/*
 * @copyright: Huang Ding
 * @Author: ding-cx
 * @Date: 2021-03-20 14:05:29
 * @LastEditors: ding-cx
 * @LastEditTime: 2021-03-20 17:23:28
 * @Description: file content
 */

import { FuncController as Controller } from "../../../libs/controller/Common";

/**
 *字典管理。此控制器模型内置接口是每一个项的。针对每个字典需手动写接口。
 *
 * @export
 * @class DropDownMenu
 * @extends {Controller}
 */
export default class DropDownMenu extends Controller {
  model = this.app.model.Sys.DropdownMenuItem;

  /** 修改通用api的数据。添加默认的创建，修改人uid */
  util_formatter(args, method) {
    // console.log(args);
    if (method === "create") {
      args.body.create_user_id = this.ctx.loginUserInfo.uid;
    } else if (method === "update") {
      args.body.edit_user_id = this.ctx.loginUserInfo.uid;
    }
    return args;
  }

  /** 字典列表获取 */
  async getDicList() {
    const dicList = await this.app.model.Sys.DropdownMenu.findAll({
      raw: true,
    });
    this.ctx.success(dicList);
  }

  /** 字典增加 */
  async dicAdd() {
    const data = this.ctx.request.body;
    data.create_user_id = this.ctx.loginUserInfo.uid;
    // data.edit_user_id = this.ctx.loginUserInfo.uid;
    const ret = await this.app.model.Sys.DropdownMenu.create(data);
    if (ret.id > 0) {
      this.ctx.success({ insertRows: 1 });
    } else {
      this.ctx.error("保存失败");
    }
  }

  /** 右侧列表获取 */
  async getSearchData() {
    const page = this.ctx.request.body.page || [20, 0];
    const menu_id = this.ctx.request.body.menu_id || [];

    const data = await this.app.model.Sys.DropdownMenuItem.findAndCountAll({
      raw: false,
      limit: page[0],
      offset: page[1],
      where: {
        menu_id,
      },
    });
    this.ctx.success(data);
  }

  /** 覆盖默认的新增，需判断重复。 */
  async create1() {
    //   TODO 覆盖默认的新增
    // super.create();
  }

  /** get 通过字典菜单的英文名称查找一个菜单的所有项。返回值为[{label:"",value:''}] */
  async getDicItemByMenuEname() {
    const menu_ename = this.ctx.params.menu_ename || "";
    const data = await this.ctx.service.sys.dropdown.getDicItemByMenuEname(
      menu_ename
    );
    this.ctx.success(data);
  }
}
