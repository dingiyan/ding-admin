/*
 * @copyright: Huang Ding
 * @Author: ding-cx
 * @Date: 2021-03-29 08:42:23
 * @LastEditors: ding-cx
 * @LastEditTime: 2021-03-31 12:00:49
 * @Description: file content
 */

import { Controller } from "egg";

export default class QuerySql extends Controller {
  async test() {
    this.ctx.service.querySql.select("config.name AS config_name");
    this.ctx.service.querySql.select([["user.real_name", "user_real_name"]]);
    this.ctx.service.querySql.select("config.key");
    this.ctx.service.querySql.select("config.id AS config_id");
    // this.ctx.service.querySql.select("*");
    this.ctx.service.querySql.from(
      this.config.dbModel_prefix + "sys_config config"
    );
    this.ctx.service.querySql.from(
      this.config.dbModel_prefix + "sys_user user",
      "config.create_user_id = user.id"
    );
    // this.ctx.service.querySql.join(this.config.dbModel_prefix + "sys_user user","user.id=config.create_user_id",'left');
    this.ctx.service.querySql.where_and("config.name", "like", "%23%");
    this.ctx.service.querySql.where_or("user.real_name", "=", "editor");

    // console.log(123);
    const data = await this.ctx.service.querySql.get();
    console.log(data);
    this.ctx.success(data);
  }
}
