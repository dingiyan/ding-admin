/*
 * @copyright: Huang Ding
 * @Author: ding-cx
 * @Date: 2021-02-27 10:23:08
 * @LastEditors: ding-cx
 * @LastEditTime: 2021-03-23 15:16:55
 * @Description: file content
 */
import { FuncController as Controller } from "../../../libs/controller/Common";

export default class OrgPartController extends Controller {
  model = this.app.model.Sys.Org;


  /** 获取组织机构树结构数据。 */
  async getTreeList() {
    const orgTree = await this.ctx.service.sys.org.createOrgTree();
    // console.log(orgTree);
    this.ctx.success(orgTree);
  }

   /**
   *根据树节点点击获取其及所有后代id的组织列表
   *
   * @memberof MenusController
   */
   async getOrgListByIdList() {
    const idList: number[] = this.ctx.request.body.idList;
    const page = this.ctx.request.body.page || [20, 0]; //分页参数
    // console.log(this.ctx.request.body);
    if (idList && idList.length > 0) {
      const route = await this.ctx.service.sys.org.getOrgListByIdList(
        idList,
        page
      );
      this.ctx.success(route);
    } else {
      this.ctx.error("请传入idList参数。");
    }
  }
}
