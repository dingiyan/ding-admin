/*
 * @copyright: Huang Ding
 * @Author: ding-cx
 * @Date: 2021-02-23 10:37:56
 * @LastEditors: ding-cx
 * @LastEditTime: 2021-04-09 11:33:13
 * @Description: file content
 */
// import { Context } from "egg";
import { FuncController as Controller } from "../../../libs/controller/Common";

import { Op } from "sequelize";

/**
 *系统 -> 角色 控制器
 *
 * @export
 * @class SysUserController
 * @extends {Controller}
 */
export default class SysUserRoleController extends Controller {
  model = this.app.model.Sys.Role;

  util_formatter(args, method) {
    if (method === "create") {
      args.body.create_user_id = this.ctx.loginUserInfo.uid;
    } else if (method === "update") {
      args.body.edit_user_id = this.ctx.loginUserInfo.uid;
    }

    return args;
  }

  /** 搜索获取列表 */
  async getSearchData() {
    const page: [number, number] = this.ctx.request.body.page || [20, 0];
    const searchs = this.ctx.request.body.searchs;
    let condition = {};
    if (searchs) {
      searchs.role_name &&
        (condition["role_name"] = { [Op.substring]: searchs.role_name });
    }
    const allUser = await this.app.model.Sys.Role.findAndCountAll({
      raw: false,
      limit: page[0],
      offset: page[1],
      where: {
        ...condition,
      },
      attributes: {
        // 加上用户表的真实姓名字段。
        include: [
          [
            this.app.model.Sequelize.col("create_user.real_name"),
            "create_user_real_name",
          ],
          [
            this.app.model.Sequelize.col("edit_user.real_name"),
            "edit_user_real_name",
          ],
        ],
      } as any,
      include: [
        {
          model: this.app.model.Sys.User,
          as: "create_user",
          attributes: [],
        },
        {
          model: this.app.model.Sys.User,
          as: "edit_user",
          attributes: [],
        },
      ],
    });
    this.ctx.success(allUser);
  }

  /** 覆盖默认的create新增一条数据的接口 */
  async create() {
    const data = this.ctx.request.body;
    data.create_user_id = this.ctx.loginUserInfo.uid;
    // console.log(data);
    // 先添加角色
    const roleRow = await this.app.model.Sys.Role.create(data);
    // console.log(roleRow.id);
    // 拿到角色id再添加权限
    data.permission.roleIdList = [roleRow.id];
    const permInsert = await this.ctx.service.sys.permission.batchPostRolesPermission(
      data.permission
    );
    if (roleRow.id > 0 && permInsert.insertRows >= 0) {
      this.ctx.success({
        // role_id: roleRow.id,
        // perm_insert_rows: permInsert.insertRows,
        id: roleRow.id,
      });
    } else {
      this.ctx.success({});
    }
  }

  /** 覆盖内置的修改接口 */
  async update() {
    const data = this.ctx.request.body;
    const role_id = this.ctx.params.id;
    // console.log(data);
    // 先修改角色表
    const ret = await this.ctx.model.Sys.Role.update(data, {
      where: {
        id: role_id,
      },
    });
    if (ret[0] >= 0) {
      // 再修改角色权限表
      data.permission.roleIdList = [role_id];
      await this.ctx.service.sys.permission.batchPostRolesPermission(
        data.permission
      );
    }

    // if(ret2.insertRows)

    this.ctx.success({ affectedRows: ret });
  }

  /** 覆盖内置的删除角色的接口
   *
   */
  async destroy() {
    // TODO 升级改造使用sequelize事务处理同时删除的问题。确保数据一致性。
    // const t = await this.app.model.transaction();

    await super.destroy(); //调用父类删除该角色。
    // 删除成功的情况
    if (this.ctx.body.data.affectedRows > 0) {
      const role_id = this.ctx.params.id;
      // console.log(role_id);
      const role_perm_num = await this.ctx.service.sys.permission.emptyRoleListPermisson(
        [role_id]
      ); //调用删除角色的权限列表
      this.ctx.echo({ perm_delete: role_perm_num });
    }
  }
}
