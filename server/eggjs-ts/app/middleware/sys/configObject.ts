/*
 * @copyright: Huang Ding
 * @Author: ding-cx
 * @Date: 2021-02-21 08:39:50
 * @LastEditors: ding-cx
 * @LastEditTime: 2021-08-13 11:56:41
 * @Description: file content
 */
import { Context } from "egg";

export default (options?:{[key:string]:any}) => {
  return async (ctx: Context, next) => {
    // console.log(options);

    await next();
    // console.log(ctx.request.path);
    // 增、删、改时，强制刷新redis
    const refreshRedisMethods = ["PUT", "DELETE"];
    if (refreshRedisMethods.includes(ctx.method) || ctx.body?.data?.id > 0) {
      await ctx.service.sys.config.getConfigObject(true);
    }
  };
};
