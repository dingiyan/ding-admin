/*
 * @copyright: Huang Ding
 * @Author: ding-cx
 * @Date: 2021-02-21 08:39:50
 * @LastEditors: ding-cx
 * @LastEditTime: 2021-02-26 15:03:17
 * @Description: file content
 */
import { Context } from "egg";

export default (options) => {
  return async (ctx: Context, next) => {
    // console.log(options);
    if (options) {
    }

   
    // 可在ctx上挂载当前请求实例的某些数据。只在当前请求有效。
    ctx.dingAdminUserInfo = {} as Context["dingAdminUserInfo"];
    ctx.dingAdminUserInfo.user_name = ctx.query && ctx.query.name;
    // 测试verify返回的是对象。
    //  const token = ctx.app.jwt.sign({name:1},'123',{
    //     expiresIn:2000
    // })
    // const decode = ctx.app.jwt.verify(token,'123')
    // console.log(decode);
    await next();
    console.log(ctx.dingAdminUserInfo);
  };
};
