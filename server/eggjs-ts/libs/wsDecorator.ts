import { Application } from 'egg'

class ws {

    map: {event:string;cb:() => void}[] = []

    on(event ?: string) {
        return (target, name, descriptor) => {
            this.map.push({ event: event || name, cb: (...args) => {
                descriptor.value.apply(this._app,[...args])
            } })
            
        }
    }

    _app:Application;

    bind(app: Application) {
        this._app = app;
        this.map.forEach(item => {
            app.ws.on(item.event, item.cb)
        })
    }
}

export default new ws();