/*
 * @copyright: Huang Ding
 * @Author: ding-cx
 * @Date: 2021-02-23 10:37:56
 * @LastEditors: ding-cx
 * @LastEditTime: 2021-03-23 15:16:44
 * @Description: file content
 */
export { RestController } from "dinegg";
import { FuncController as Controller } from "dinegg";
export class FuncController extends Controller {
  /** 修改通用api的数据。添加默认的创建，修改人uid */
  util_formatter(args, method) {
    if (method === "create") {
      args.body.create_user_id = this.ctx.loginUserInfo.uid;
    } else if (method === "update") {
      args.body.edit_user_id = this.ctx.loginUserInfo.uid;
    }
    return args;
  }
}
