import { EggAppConfig, PowerPartial } from 'egg';

export default () => {
  const config: PowerPartial<EggAppConfig> = {};
  // config.middleware = ['testCtx']

  config.sequelize = {
    port: 3309,
    database: 'ding_admin_test',
    username: 'root',
    password: 'derlo123',
    timezone: '+08:00',
    define: {
      freezeTableName: true,
      timestamps: true,
    }
  }
  return config;
};
