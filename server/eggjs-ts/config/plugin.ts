/*
 * @copyright: Huang Ding
 * @Author: ding-cx
 * @Date: 2021-02-21 08:39:50
 * @LastEditors: ding-cx
 * @LastEditTime: 2021-06-18 18:20:14
 * @Description: file content
 */
import { EggPlugin } from "egg";
import * as path from "path";

const plugin: EggPlugin = {
  // static: true,
  // nunjucks: {
  //   enable: true,
  //   package: 'egg-view-nunjucks',
  // },
  io: {
    enable: false,
    package: "egg-socket.io",
  },
  ws: {
    enable: true,
    // path: path.join(__dirname, "../libs/plugin/egg-ws"),
    package: "@diyaner/egg-ws",
  },
  redis: {
    enable: true,
    package: "egg-redis",
  },
  nodemailer: {
    enable: true,
    package: "@cxkj/egg-nodemailer",
  },
};

export default plugin;
