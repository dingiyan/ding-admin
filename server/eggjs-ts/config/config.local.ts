/*
 * @copyright: Huang Ding
 * @Author: ding-cx
 * @Date: 2021-02-21 08:39:50
 * @LastEditors: ding-cx
 * @LastEditTime: 2021-03-24 15:12:23
 * @Description: file content
 */
import { EggAppConfig, PowerPartial } from "egg";

export default () => {
	const config: PowerPartial<EggAppConfig> = {};
	config.sequelize = {
		port: 3309,
		database: "ding_admin_dev",
		username: "root",
		password: "derlo123",
		timezone: "+08:00",
		define: {
			freezeTableName: true,
			timestamps: true,
			paranoid: true, //使用偏执表，即删除一行数据时，只标记某个字段，而不是物理删除。
		},
	};

	config.redis = {
		Redis: require("ioredis"),
		clients: {
			default: {
				port: 6379, // Redis port
				host: "127.0.0.1", // Redis host
				password: "dingAdmin888888",
				db: 0,
			},
			d2: {
				port: 6379, // Redis port
				host: "127.0.0.1", // Redis host
				password: "dingAdmin888888",
				db: 0,
			},

			// "6389": {
			//   port: 6389, // Redis port
			//   host: "127.0.0.1", // Redis host
			//   password: "",
			//   db: 0,
			// },
		},
	};

	return config;
};
