/*
 * @copyright: Huang Ding
 * @Author: ding-cx
 * @Date: 2021-02-21 08:39:50
 * @LastEditors: ding-cx
 * @LastEditTime: 2021-04-07 16:14:52
 * @Description: file content
 */
import { EggAppConfig, PowerPartial } from "egg";

export default () => {
  const config: PowerPartial<EggAppConfig> = {};
  config.sequelize = {
    database: "ding_admin_dev",
    port: 3309,
    username: "root",
    password: "derlo123",
    timezone: "+08:00",
    define: {
      timestamps: true,
      freezeTableName: true,
    },
  };
  config.redis = {
    clients: {
      default: {
        port: 6379, // Redis port
        host: "127.0.0.1", // Redis host
        password: "dingAdmin888888",
        db: 0,
      },
      // "6389": {
      //   port: 6389, // Redis port
      //   host: "127.0.0.1", // Redis host
      //   password: "",
      //   db: 0,
      // },
    },
  };
  return config;
};
