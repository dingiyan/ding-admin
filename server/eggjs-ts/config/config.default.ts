/*
 * @copyright: Huang Ding
 * @Author: ding-cx
 * @Date: 2021-02-23 10:37:56
 * @LastEditors: ding-cx
 * @LastEditTime: 2021-04-23 15:21:34
 * @Description: file content
 */
import { EggAppConfig, EggAppInfo, PowerPartial } from "egg";

export default (appInfo: EggAppInfo) => {
  const config = {} as PowerPartial<EggAppConfig>;

  // override config from framework / plugin
  // use for cookie sign key, should change to your own and keep security
  config.keys = appInfo.name + "_1612961177702_6847";

  // add your egg config in here
  config.middleware = ["addUpdateDelete"];

  /* jwt plugin 官方插件。*/
  config.jwt = {
    secret: "fhsldghwoeihgtiowehrlgsdhfsljl24234hldkfsdfhls",
  };

  /** jwt login中间件，登录校验。 */
  config.jwtLogin = {
    enable: true,
    expires: 3000, //jwt token过期时间.用户登录有效期。固定的。
    isGlobal: true,
    // 登录控制中间件的 路由白名单
    route_white_list: [
      "/sys/captcha/generate/",
      "/sys/user/login",
      "/oss/local/file", //oss访问文件
      // "/sys/user/logout",
      "/query-sql",
      "/sys-install",
    ],
  };

  /** 设置session配置 */
  config.session = {
    key: "EGG_SESS",
    maxAge: ((config.jwtLogin.expires as number) + 10) * 1000, //
    httpOnly: true,
    encrypt: true,
  };

  config.security = {
    csrf: false,
  };

  config.route_prefix = {
    api: "/api",
    api2: "/api/v2",
  };

  // 设置表前缀。在模型中使用。
  config.dbModel_prefix = "ding_";

  // socket.io plugin config
  config.io = {
    init: {}, // passed to engine.io
    namespace: {
      "/": {
        // connectionMiddleware: ["auth2"],
        connectionMiddleware: [],
        packetMiddleware: [],
      },
      // "/example": {
      //   connectionMiddleware: [],
      //   packetMiddleware: [],
      // },
    },
  };

  config.customLoader = {
    // 挂载常量目录
    const: {
      directory: "app/const",
    },
  };

  // 上传文件的配置.使用流上传。
  config.multipart = {
    mode: "stream",
    fileExtensions: ["xls", "xlsx", "doc", "docx", "pdf"],
    fileSize: "50mb", //限制上传文件大小
  };
  // the return config will combines to EggAppConfig
  return {
    ...config,
  };
};
