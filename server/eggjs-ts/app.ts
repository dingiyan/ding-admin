/*
 * @copyright: Huang Ding
 * @Author: ding-cx
 * @Date: 2021-02-21 08:39:50
 * @LastEditors: ding-cx
 * @LastEditTime: 2021-04-12 14:37:33
 * @Description: file content
 */
import { Application, IBoot } from "egg";

// 导入WebSocket模块:
// import * as WebSocket from "ws";

export default class AppInitBoot implements IBoot {
	private readonly app: Application;

	constructor(app: Application) {
		this.app = app;
		// console.log(process.pid);
		this.app.once("server", () => {
			// console.log(server);
			// console.log(process.pid);
		});
	}

	testRedis() {
		const ctx = this.app.createAnonymousContext();
		const d1 = ctx.service.redis.getRedis("default");
		const d2 = ctx.service.redis.getRedis("d2");
		d1.blpop("qqq", 0).then((res) => {
			console.log("blpop", res);
			if (res) {
				console.log(new Date().getTime() - Number(res[1]));
			}
		});

		setTimeout(async () => {
			const i = new Date().getTime();
			console.log("settimeout");
			const res = await d2.rpush("qqq", i);
			console.log("rpush", res);
		}, 3000);
	}

	async willReady() {
	 
		// console.log(this.app.config.env);
		//local本地开发环境执行一次sync，将模型入数据库。
		if (this.app.config.env !== "prod") {
			// await this.app.model.Sys.Email.sync({ force: true });
			// await this.app.model.sync({ force: true });
		}
	}
}
